package com.sattar.cars.shift.ui.main.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.network.UserApis;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;
import com.sattar.cars.shift.utils.AppConstants;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

public class MainInteractor extends BaseInteractor implements IMainInteractor {

    private Context mContext;
    private UserApis userApis;

    @Inject
    public MainInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper, UserApis userApis) {
        super(preferencesHelper);
        mContext=context;
        this.userApis=userApis;
    }


    @Override
    public ArrayList<String> getHeaderTitle() {
            return new ArrayList<>(Arrays.asList(AppConstants.getMainMenuTitle(mContext)));
    }



}
