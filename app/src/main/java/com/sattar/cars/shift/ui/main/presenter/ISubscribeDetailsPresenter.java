package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.ISubscribeDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.SubscribeDetailsView;

@PerActivity
public interface ISubscribeDetailsPresenter<V extends SubscribeDetailsView,I extends ISubscribeDetailsInteractor> extends IBasePresenter<V,I> {
    void getAvaliableTime(String lang, String departmentId);

    void getMakeOrder(String lang, String selectedDate, String selectedTime, String orderId, String clintId);

    boolean validateUserData(String date, String time);
}

