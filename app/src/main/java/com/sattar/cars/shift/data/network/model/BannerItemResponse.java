package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerItemResponse {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("department_id")
    @Expose
    private String departmentId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("descrbtion")
    @Expose
    private String descrbtion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescrbtion() {
        return descrbtion;
    }

    public void setDescrbtion(String descrbtion) {
        this.descrbtion = descrbtion;
    }
}
