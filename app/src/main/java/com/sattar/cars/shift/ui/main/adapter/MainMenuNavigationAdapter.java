package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.CategoryInfo;
import com.sattar.cars.shift.ui.main.holder.MainNavigationMenuViewHolder;


import java.util.ArrayList;

import javax.inject.Inject;


public class MainMenuNavigationAdapter extends RecyclerView.Adapter<MainNavigationMenuViewHolder> {


    private ArrayList<CategoryInfo> mainCategoryInfoArrayList=new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public MainMenuNavigationAdapter(Context context) {
        this.context = context;
    }

    public interface Callback {
        void onMainMenuItemClick(int position, CategoryInfo mainCategoryInfo, int lastIndex);
    }

    public void setMainMenuTitleListTitle(ArrayList<CategoryInfo> mainCategoryInfoArrayList) {
        this.mainCategoryInfoArrayList = mainCategoryInfoArrayList;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public MainNavigationMenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MainNavigationMenuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_main_nav_menu, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull MainNavigationMenuViewHolder holder, int position) {
        holder.setMainMenuTitle(mainCategoryInfoArrayList.get(position),position,mainCategoryInfoArrayList.size()-1);
    }


    @Override
    public int getItemCount() {
        return mainCategoryInfoArrayList.size();
    }
}
