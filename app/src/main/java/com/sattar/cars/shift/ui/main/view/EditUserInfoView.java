package com.sattar.cars.shift.ui.main.view;

import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.ui.base.view.IBaseView;

public interface EditUserInfoView extends IBaseView {


    void onFirstNameError(int firstNameError);

    void onEmailError(int emailRequiredError);

    void onMobileError(int governmentError);

    void onAgeError(int hospitalError);

    void exitTheApplication();

    void changeHeaderData(UserInfo userInfo);
}
