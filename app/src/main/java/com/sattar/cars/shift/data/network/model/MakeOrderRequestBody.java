package com.sattar.cars.shift.data.network.model;

import java.util.ArrayList;

public class MakeOrderRequestBody {
    private String date;
    private String time;
    private String finalPrice;
    private String duration;
    private String departmentId;
    private String companyName;
    private ArrayList<CarDetailsRequestBody> carDetailsRequestBodyArrayList;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public ArrayList<CarDetailsRequestBody> getCarDetailsRequestBodyArrayList() {
        return carDetailsRequestBodyArrayList;
    }

    public void setCarDetailsRequestBodyArrayList(ArrayList<CarDetailsRequestBody> carDetailsRequestBodyArrayList) {
        this.carDetailsRequestBodyArrayList = carDetailsRequestBodyArrayList;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
