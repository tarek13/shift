package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.EditUserPasswordInteractor;
import com.sattar.cars.shift.ui.main.interactor.IEditUserPasswordInteractor;
import com.sattar.cars.shift.ui.main.presenter.EditUserPasswordPresenter;
import com.sattar.cars.shift.ui.main.presenter.IEditUserPasswordPresenter;
import com.sattar.cars.shift.ui.main.view.EditUserPasswordView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class EditUserPasswordModule {

    @Provides
    @PerActivity
    IEditUserPasswordPresenter<EditUserPasswordView,IEditUserPasswordInteractor> provideEditProfilePresenter(EditUserPasswordPresenter<EditUserPasswordView,IEditUserPasswordInteractor> editProfilePresenter){
        return  editProfilePresenter;
    }

    @Provides
    @PerActivity
    IEditUserPasswordInteractor provideEditProfileInteractor (EditUserPasswordInteractor editProfileInteractor){
        return editProfileInteractor;
    }


}
