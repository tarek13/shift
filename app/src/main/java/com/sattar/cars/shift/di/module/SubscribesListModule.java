package com.sattar.cars.shift.di.module;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.SubscribesListAdapter;
import com.sattar.cars.shift.ui.main.interactor.SubscribesListInteractor;
import com.sattar.cars.shift.ui.main.interactor.ISubscribesListInteractor;
import com.sattar.cars.shift.ui.main.presenter.SubscribesListPresenter;
import com.sattar.cars.shift.ui.main.presenter.ISubscribesListPresenter;
import com.sattar.cars.shift.ui.main.view.SubscribesListView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class SubscribesListModule {

    @Provides
    @PerActivity
    ISubscribesListPresenter<SubscribesListView, ISubscribesListInteractor> provideSubscribesListPresenter(SubscribesListPresenter<SubscribesListView, ISubscribesListInteractor> companiesListPresenter){
        return companiesListPresenter;
    }

    @Provides
    @PerActivity
    ISubscribesListInteractor provideSubscribesListInteractor (SubscribesListInteractor eventsListInteractor) {
        return  eventsListInteractor;
    }

    @Provides
    @PerActivity
    SubscribesListAdapter provideSubscribesListAdapter(@ActivityContext Context context){
        return new SubscribesListAdapter(context);
    }

    @Provides
    @PerActivity
    LinearLayoutManager provideLinearManagerSubscribesList(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }
}
