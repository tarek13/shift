package com.sattar.cars.shift.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.LoginRequestBody;
import com.sattar.cars.shift.di.component.DaggerSplashComponent;
import com.sattar.cars.shift.di.component.SplashComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.SplashModule;
import com.sattar.cars.shift.ui.base.BaseActivity;
import com.sattar.cars.shift.ui.login.LoginActivity;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.splash.interactor.ISplashInteractor;
import com.sattar.cars.shift.ui.splash.presenter.ISplashPresenter;
import com.sattar.cars.shift.ui.splash.view.SplashView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashView {

    private static final String TAG = "SplashActivity";

    @Inject
    ISplashPresenter<SplashView, ISplashInteractor> mPresenter;

    SplashComponent splashComponent;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;

    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUnBinder(ButterKnife.bind(this));

        // define splash component of DI
        splashComponent = DaggerSplashComponent.builder()
                .splashModule(new SplashModule())
                .activityModule(new ActivityModule(this))
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .applicationComponent(((ShiftApp) getApplication()).getComponent())
                .build();

        //  to tell the Dagger to scan this class through the implementation of this interface.
        splashComponent.inject(this);
        mPresenter.onAttach(SplashActivity.this);
        userInfo = mPresenter.getCurrentUser();
        setUp();
    }

    @Override
    protected void setUp() {
        if (userInfo != null) {
            LoginRequestBody loginRequestBody = new LoginRequestBody();
            loginRequestBody.setLang(getString(R.string.language));
            loginRequestBody.setEmail(userInfo.getEmail());
            loginRequestBody.setPassword(userInfo.getPassword());
            mPresenter.onLoginButtonClick(loginRequestBody);
        } else {
            startDelayedThread();
        }
    }

    /*
     * open login activity
     * */
    @Override
    public void openLoginActivity() {
        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    /*
     * open main activity
     * */
    @Override
    public void openMainActivity() {
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    /*
     * start delayed thread to open login activty or main activity after 2000ms
     * */
    @Override
    public void startDelayedThread() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                openLoginActivity();
                // close this activity
                finish();
            }
        }, AppConstants.SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }


}
