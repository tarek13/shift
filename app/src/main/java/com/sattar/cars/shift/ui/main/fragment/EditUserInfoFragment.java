package com.sattar.cars.shift.ui.main.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.di.component.DaggerEditUserInfoComponent;
import com.sattar.cars.shift.di.component.EditUserInfoComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.EditUserInfoModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.login.LoginActivity;
import com.sattar.cars.shift.ui.main.interactor.IEditUserInfoInteractor;
import com.sattar.cars.shift.ui.main.presenter.IEditUserInfoPresenter;
import com.sattar.cars.shift.ui.main.view.EditUserInfoView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditUserInfoFragment extends BaseFragment implements EditUserInfoView {

    @BindView(R.id.edit_profile_first_name_edit_text)
    EditText editProfileFirstNameEditText;
    @BindView(R.id.edit_profile_error_first_name_text_view)
    CustomTextView editProfileErrorFirstNameTextView;
    @BindView(R.id.edit_profile_email_edit_text)
    EditText editProfileEmailEditText;
    @BindView(R.id.edit_profile_error_email_text_view)
    CustomTextView editProfileErrorEmailTextView;
    @BindView(R.id.edit_profile_mobile_edit_text)
    EditText editProfileMobileEditText;
    @BindView(R.id.edit_profile_error_mobile_text_view)
    CustomTextView editProfileErrorMobileTextView;
    @BindView(R.id.edit_profile_age_edit_text)
    EditText editProfileAgeEditText;
    @BindView(R.id.edit_profile_error_age_text_view)
    CustomTextView editProfileErrorAgeTextView;
    @BindView(R.id.male_radio_button)
    RadioButton maleRadioButton;
    @BindView(R.id.female_radio_button)
    RadioButton femaleRadioButton;
    @BindView(R.id.edit_profile_save_button_container)
    LinearLayout editProfileSaveButtonContainer;

    @Inject
    IEditUserInfoPresenter<EditUserInfoView, IEditUserInfoInteractor> editProfilePresenter;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;

    EditUserInfoComponent editUserInfoComponent;

    private String firstName;
    private String email;
    private String age;
    private UserInfo userInfo;
    private String mobile;
    private String gender;

    public static Fragment newInstance() {
        Bundle args = new Bundle();
        EditUserInfoFragment fragment = new EditUserInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_user_info, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        editUserInfoComponent = DaggerEditUserInfoComponent.builder()
                .editUserInfoModule(new EditUserInfoModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())

                .build();
        editUserInfoComponent.inject(this);

        editProfilePresenter.onAttach(this);
        setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {

        userInfo = editProfilePresenter.getInteractor().getPreferencesHelper().getCurrentUser();

        editProfileErrorFirstNameTextView.setVisibility(View.GONE);
        editProfileErrorEmailTextView.setVisibility(View.GONE);
        editProfileErrorMobileTextView.setVisibility(View.GONE);
        editProfileErrorAgeTextView.setVisibility(View.GONE);

        editProfileFirstNameEditText.setText(userInfo.getName());
        editProfileEmailEditText.setText(userInfo.getEmail());

        editProfileMobileEditText.setText(userInfo.getMobile());

        editProfileAgeEditText.setText(userInfo.getAge());
        if (userInfo.getGender().equals("2")) {
            femaleRadioButton.setChecked(true);
        } else {
            maleRadioButton.setChecked(true);
        }



    }

    @OnClick({R.id.edit_profile_save_button_container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_profile_save_button_container:
                if (!editProfilePresenter.checkIfEmailChange(editProfileEmailEditText.getText().toString())) {
                    showChangeEmailAlertDialog(getActivity(), getString(R.string.alert_email_title), getString(R.string.email_alert_edit));
                } else {
                    fillUserDataFromUI();
                    if (editProfilePresenter.validateUserData(firstName, email,mobile,age)) {
                        changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.WEBSITE_API_URL_1);
                        editProfilePresenter.onSaveButtonClick(buildRegisterRequestBody());
                    }
                }
                break;
        }
    }

    private void fillUserDataFromUI() {

        editProfileErrorFirstNameTextView.setVisibility(View.GONE);
        editProfileErrorEmailTextView.setVisibility(View.GONE);
        editProfileErrorMobileTextView.setVisibility(View.GONE);
        editProfileErrorAgeTextView.setVisibility(View.GONE);

        firstName = editProfileFirstNameEditText.getText().toString();
        email = editProfileEmailEditText.getText().toString();
        mobile = editProfileMobileEditText.getText().toString();
        age = editProfileAgeEditText.getText().toString();
        if (femaleRadioButton.isChecked()) {
            gender = AppConstants.GENDER_FEMALE_TYPE;
        }
        if (maleRadioButton.isChecked()) {
            gender = AppConstants.GENDER_MALE_TYPE;
        }


    }

    private RegisterRequestBody buildRegisterRequestBody() {
        RegisterRequestBody editProfileRequestBody = new RegisterRequestBody();

        editProfileRequestBody.setLang(getString(R.string.language));
        editProfileRequestBody.setFirstName(firstName);
        editProfileRequestBody.setMail(email);
        editProfileRequestBody.setMobile(mobile);
        editProfileRequestBody.setGender(gender);
        editProfileRequestBody.setAge(age);



        return editProfileRequestBody;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFirstNameError(int firstNameError) {
        editProfileErrorFirstNameTextView.setVisibility(View.VISIBLE);
        editProfileErrorFirstNameTextView.setText(firstNameError);
        editProfileFirstNameEditText.requestFocus();
    }



    @Override
    public void onEmailError(int emailRequiredError) {
        editProfileErrorEmailTextView.setVisibility(View.VISIBLE);
        editProfileErrorEmailTextView.setText(emailRequiredError);
        editProfileEmailEditText.requestFocus();
    }


    @Override
    public void onMobileError(int mobileError) {
        editProfileErrorMobileTextView.setVisibility(View.VISIBLE);
        editProfileErrorMobileTextView.setText(mobileError);
        editProfileMobileEditText.requestFocus();
    }



    @Override
    public void onAgeError(int hospitalError) {
        editProfileErrorAgeTextView.setVisibility(View.VISIBLE);
        editProfileErrorAgeTextView.setText(hospitalError);
        editProfileAgeEditText.requestFocus();
    }


    public void showChangeEmailAlertDialog(Context context, String title, String content) {
        new MaterialDialog.Builder(context)
                .title(title)
                .titleColorRes(R.color.yellow_gold_color)
                .content(content)
                .positiveText(R.string.agree)
                .positiveColorRes(R.color.purple)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        fillUserDataFromUI();
                        if (editProfilePresenter.validateUserData(firstName, email,
                                 mobile, age)) {
                            changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.WEBSITE_API_URL_1);
                            editProfilePresenter.onSaveButtonClick(buildRegisterRequestBody());
                        }
                    }
                })
                .show();
    }

    @Override
    public void exitTheApplication() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getBaseActivity().finish();
    }

    @Override
    public void changeHeaderData(UserInfo userInfo) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        editProfilePresenter.onDetach();
    }
}
