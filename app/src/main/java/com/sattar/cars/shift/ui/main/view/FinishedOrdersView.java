package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface FinishedOrdersView extends IBaseView {

    void showFinishedOrdersList(ArrayList<MyOrderItemResponse> eventItemResponseArrayList);


    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);
}
