package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CarDetailsRequestBody;
import com.sattar.cars.shift.ui.main.adapter.CarItemAdapter;
import com.sattar.cars.shift.utils.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.car_wash_type_text_view)
    CustomTextView carWashTypeTextView;
    @BindView(R.id.car_size_text_view)
    CustomTextView carSizeTextView;
    @BindView(R.id.car_delete_button)
    ImageView carDeleteButton;

    private CarItemAdapter.Callback callback;
    private Context context;
    private int position;
    private CarDetailsRequestBody data;

    public CarItemViewHolder(View itemView, Context context, CarItemAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;
        itemView.setOnClickListener(this);
    }
    public void setData(CarDetailsRequestBody data, int position) {
        this.data = data;
        this.position=position;
        setup();
    }

    private void setup() {
        carWashTypeTextView.setText(data.getPackageName());
        carSizeTextView.setText(data.getSizeName());

    }

    @Override
    public void onClick(View v) {
    }

    @OnClick(R.id.car_delete_button)
    public void onViewClicked() {
        callback.onDeleteClick(position, data);
    }
}
