package com.sattar.cars.shift.ui.main.interactor;

import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.RegisterErrorResponse;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;

public interface IEditUserPasswordInteractor extends IBaseInteractor{

    Observable<RegisterResponse> doUserEditProfile(Map<String, String> registerRequestBody);


    void saveUserToSharedPreferences(UserInfo userInfo);

    RegisterErrorResponse handleErrorResponse(Response<?> response);

}
