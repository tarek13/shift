package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.EditProfileFragmentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yahia on 05/10/15.
 */
public class EditUserFragment extends BaseFragment {
    public static final String TAG = "EditUserFragment";
    @BindView(R.id.edit_profile_tabs)
    TabLayout editUserTabs;

    @BindView(R.id.edit_profile_viewpager)
    ViewPager editUserViewpager;

    private ViewPager viewPager;
    private EditProfileFragmentAdapter adapter;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        EditUserFragment fragment = new EditUserFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.err.println(" in onCreateView ...");
        View rootView = inflater.inflate(R.layout.fragment_edit_user, container, false);
        setUnBinder(ButterKnife.bind(this, rootView));

        adapter = new EditProfileFragmentAdapter(getChildFragmentManager(),getBaseActivity());
        editUserViewpager.setAdapter(adapter);
        editUserTabs.setupWithViewPager(editUserViewpager);
      //  setupTabIcons();
        setShowMenu(false);
        return rootView;
    }
    @Override
    protected void setUp(View view) {
        // getBaseActivity().getSupportActionBar().setTitle(storeItemResponse.getTitle());
        ((MainActivity) getBaseActivity()).getSupportActionBar().setTitle(R.string.account_settings);
        ((MainActivity) getBaseActivity()).removeHamburgerIcon(true);

    }

   /* private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_account,
                R.drawable.navigation_menu_ic_devices,
                R.drawable.navigation_menu_ic_group,
                R.drawable.navigation_menu_ic_profiles,
                R.drawable.navigation_menu_ic_events
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
    }*/


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
