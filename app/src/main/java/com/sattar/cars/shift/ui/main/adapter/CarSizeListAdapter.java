package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse.Size;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.ui.main.holder.CarSizeItemViewHolder;

import java.util.ArrayList;

import javax.inject.Inject;

public class CarSizeListAdapter extends RecyclerView.Adapter<CarSizeItemViewHolder> {


    // View Types
    private ArrayList<ServiceDetailsResponse.Size> carSizesItemResponseArrayList = new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public CarSizeListAdapter(Context context) {
        this.context = context;
    }

    public interface Callback {
        void onCarSizeItemClick(int position, ServiceDetailsResponse.Size carSizesItemResponse);
    }

    public void setData(ArrayList<ServiceDetailsResponse.Size> carSizesItemResponses) {
        for(ServiceDetailsResponse.Size s:carSizesItemResponses){
            s.setSelected(false);
        }
        this.carSizesItemResponseArrayList = carSizesItemResponses;

        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public CarSizeItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CarSizeItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_car_size_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull CarSizeItemViewHolder holder, int position) {
        holder.setCarSize(carSizesItemResponseArrayList.get(position), position);
    }


    @Override
    public int getItemCount() {
        return carSizesItemResponseArrayList.size();
    }

    public ArrayList<ServiceDetailsResponse.Size> getSelectedItems() {

        ArrayList<ServiceDetailsResponse.Size> selectedItems = new ArrayList<>();
        for (ServiceDetailsResponse.Size item : carSizesItemResponseArrayList) {
            if (item.isSelected()) {
                selectedItems.add(item);
            }
        }
        return selectedItems;
    }

    public ArrayList<Size> getCarSizesItemResponseArrayList() {
        return carSizesItemResponseArrayList;
    }
}
