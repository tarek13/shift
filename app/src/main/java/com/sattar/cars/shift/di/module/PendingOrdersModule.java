package com.sattar.cars.shift.di.module;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.MyOrdersListAdapter;
import com.sattar.cars.shift.ui.main.interactor.PendingOrdersInteractor;
import com.sattar.cars.shift.ui.main.interactor.IPendingOrdersInteractor;
import com.sattar.cars.shift.ui.main.presenter.PendingOrdersPresenter;
import com.sattar.cars.shift.ui.main.presenter.IPendingOrdersPresenter;
import com.sattar.cars.shift.ui.main.view.PendingOrdersView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class PendingOrdersModule {

    @Provides
    @PerActivity
    IPendingOrdersPresenter<PendingOrdersView, IPendingOrdersInteractor> providePendingOrdersPresenter(PendingOrdersPresenter<PendingOrdersView, IPendingOrdersInteractor> companiesListPresenter){
        return companiesListPresenter;
    }

    @Provides
    @PerActivity
    IPendingOrdersInteractor providePendingOrdersInteractor (PendingOrdersInteractor eventsListInteractor) {
        return  eventsListInteractor;
    }

    @Provides
    @PerActivity
    MyOrdersListAdapter providePendingOrdersAdapter(@ActivityContext Context context){
        return new MyOrdersListAdapter(context);
    }

    @Provides
    @PerActivity
    LinearLayoutManager provideLinearManagerPendingOrders(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }
}
