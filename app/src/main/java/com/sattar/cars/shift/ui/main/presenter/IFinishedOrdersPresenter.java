package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IFinishedOrdersInteractor;
import com.sattar.cars.shift.ui.main.view.FinishedOrdersView;

@PerActivity
public interface IFinishedOrdersPresenter<V extends FinishedOrdersView,I extends IFinishedOrdersInteractor> extends IBasePresenter<V,I> {
    void loadFinishedOrders(String lang, String departmentId);
}

