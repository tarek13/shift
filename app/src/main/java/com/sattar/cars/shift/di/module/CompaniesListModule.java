package com.sattar.cars.shift.di.module;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;


import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.CompaniesListAdapter;
import com.sattar.cars.shift.ui.main.interactor.CompaniesListInteractor;
import com.sattar.cars.shift.ui.main.interactor.ICompaniesListInteractor;
import com.sattar.cars.shift.ui.main.presenter.CompaniesListPresenter;
import com.sattar.cars.shift.ui.main.presenter.ICompaniesListPresenter;
import com.sattar.cars.shift.ui.main.view.CompaniesListView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class CompaniesListModule {

    @Provides
    @PerActivity
    ICompaniesListPresenter<CompaniesListView, ICompaniesListInteractor> provideCompaniesListPresenter(CompaniesListPresenter<CompaniesListView, ICompaniesListInteractor> companiesListPresenter){
        return companiesListPresenter;
    }

    @Provides
    @PerActivity
    ICompaniesListInteractor provideCompaniesListInteractor (CompaniesListInteractor eventsListInteractor) {
        return  eventsListInteractor;
    }

    @Provides
    @PerActivity
    CompaniesListAdapter provideCompaniesListAdapter(@ActivityContext Context context){
        return new CompaniesListAdapter(context);
    }

    @Provides
    @PerActivity
    LinearLayoutManager provideLinearManagerCompaniesList(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }
}
