package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.ui.main.adapter.MyOrdersListAdapter;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyOrderItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.my_order_company_icon_image_view)
    ImageView myOrderCompanyIconImageView;
    @BindView(R.id.my_order_company_title_text_view)
    CustomTextView myOrderCompanyTitleTextView;
    @BindView(R.id.my_order_service_title_text_view)
    CustomTextView myOrderServiceTitleTextView;
    @BindView(R.id.my_order_date_text_view)
    CustomTextView myOrderDateTextView;
    @BindView(R.id.my_order_info_container)
    LinearLayout myOrderInfoContainer;


    private MyOrdersListAdapter.Callback callback;
    private Context context;
    private int position;
    private MyOrderItemResponse myOrderItemResponse;

    public MyOrderItemViewHolder(View itemView, Context context, MyOrdersListAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;
        itemView.setOnClickListener(this);
    }
    public void setData(MyOrderItemResponse myOrderItemResponse, int position) {
        this.myOrderItemResponse = myOrderItemResponse;
        this.position=position;
        setup();
    }

    private void setup() {
        myOrderCompanyTitleTextView.setText(myOrderItemResponse.getNameInner());
        if(context.getString(R.string.language).equals(AppConstants.ARABIC)) {
            myOrderServiceTitleTextView.setText(myOrderItemResponse.getNameAR());
        }else {
            myOrderServiceTitleTextView.setText(myOrderItemResponse.getNameEN());
        }

        if(myOrderItemResponse.getLogo()!=null   ) {
            GlideApp.with(context).load("http://ur-business.net/shift_new/"+myOrderItemResponse.getLogo()).error(R.drawable.care_care).into(myOrderCompanyIconImageView);
        }

        myOrderDateTextView.setText(myOrderItemResponse.getDate());


    }

    @Override
    public void onClick(View v) {
        callback.onDataItemClick(position, myOrderItemResponse);
    }

}
