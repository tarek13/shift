package com.sattar.cars.shift.data.network.model;

import java.util.List;
import java.util.Map;

public class SearchBodyRequest {
    private Map<String,String> options;
    private List<Integer> categoriesId;

    public Map<String, String> getOptions() {
        return options;
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    public List<Integer> getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(List<Integer> categoriesId) {
        this.categoriesId = categoriesId;
    }
}
