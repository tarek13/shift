package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.AboutUsResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IAboutUsInteractor;
import com.sattar.cars.shift.ui.main.view.AboutUsView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class AboutUsPresenter<V extends AboutUsView,I extends IAboutUsInteractor>
        extends BasePresenter<V,I> implements IAboutUsPresenter<V,I>{
    private static final String TAG = "Eventslist";

    @Inject
    public AboutUsPresenter(I mvpInteractor,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void loadAboutUs(String options) {
        getMvpView().showLoadingProgressbar();

        getCompositeDisposable().add(getInteractor().getAboutUs(options,getInteractor().getPreferencesHelper().getCurrentUser().getId())
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<AboutUsResponse>() {
                    @Override
                    public void accept(AboutUsResponse aboutUsResponse) {
                        getMvpView().hideLoadingProgressbar();

                        if(aboutUsResponse.getStatus()) {
                            getMvpView().showAboutUs(aboutUsResponse.getResult());
                        }else {
                            getMvpView().onError(aboutUsResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoadingProgressbar();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                            }
                        }else {
                            getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);

                        }
                    }
                }));
    }


}
