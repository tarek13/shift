package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface PendingOrdersView extends IBaseView {

    void showPendingOrdersList(ArrayList<MyOrderItemResponse> eventItemResponseArrayList);


    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);
}
