package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.data.db.model.CategoryInfo;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IMainInteractor;
import com.sattar.cars.shift.ui.main.view.MainView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class MainPresenter<V extends MainView, I extends IMainInteractor>
        extends BasePresenter<V, I> implements IMainPresenter<V, I> {

    @Inject
    public MainPresenter(I mvpInteractor,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onMainMenuItemClick(int position, CategoryInfo mainCategoryInfo, int lastIndex) {

        switch (position) {
            case 0:
                getMvpView().showHomeFragment();
                getMvpView().closeNavigationDrawer();
                break;
            case 1:
               getMvpView().showEditProfileFragment();
                getMvpView().closeNavigationDrawer();
                break;
            case 2:
                getMvpView().showMyOrdersFragment();
                getMvpView().closeNavigationDrawer();
                break;
            case 3:
               getMvpView().showAboutUsFragment();
                getMvpView().closeNavigationDrawer();
                break;
            case 4:
                doLogout();
                getMvpView().closeNavigationDrawer();
                break;
        }

    }

    @Override
    public void loadMainMenuItem() {
        ArrayList<CategoryInfo> categoryInfoArrayList = new ArrayList<>();
        int id = 0;
        for (String title : getInteractor().getHeaderTitle()) {
            CategoryInfo categoryInfo = new CategoryInfo();
            categoryInfo.setId(id);
            categoryInfo.setTitle(title);
            categoryInfo.setIcon(AppConstants.MAIN_MENU_ICON[id]);
            categoryInfoArrayList.add(categoryInfo);
            id++;
        }
        getMvpView().showMainMenuItem(categoryInfoArrayList);
    }


    @Override
    public void loadUserProfileDataFromPrefs() {
        UserInfo userInfo = getInteractor().getPreferencesHelper().getCurrentUser();
        if (userInfo != null) {
            getMvpView().fillUserDataFromPrefs(userInfo.getName(), userInfo.getEmail());
        }
    }


    @Override
    public void doLogout() {
        UserInfo userInfo = getInteractor().getPreferencesHelper().getCurrentUser();


        getMvpView().openLoginActivity();

        getInteractor().getPreferencesHelper().removeCurrentUser();
    }


}
