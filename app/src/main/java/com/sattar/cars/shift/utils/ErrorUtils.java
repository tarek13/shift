package com.sattar.cars.shift.utils;

import com.sattar.cars.shift.data.network.model.RegisterErrorResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ErrorUtils {

    public static RegisterErrorResponse parseRegisterError(Response<?> response, Retrofit retrofit) {
        Converter<ResponseBody, RegisterErrorResponse> converter =
                retrofit.responseBodyConverter(RegisterErrorResponse.class, new Annotation[0]);

        RegisterErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new RegisterErrorResponse();
        }

        return error;
    }
}
