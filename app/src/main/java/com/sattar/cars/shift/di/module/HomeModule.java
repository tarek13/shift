package com.sattar.cars.shift.di.module;


import android.content.Context;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.BannerViewPagerAdapter;
import com.sattar.cars.shift.ui.main.adapter.HomeAdapter;
import com.sattar.cars.shift.ui.main.interactor.HomeInteractor;
import com.sattar.cars.shift.ui.main.interactor.IHomeInteractor;
import com.sattar.cars.shift.ui.main.presenter.HomePresenter;
import com.sattar.cars.shift.ui.main.presenter.IHomePresenter;
import com.sattar.cars.shift.ui.main.view.HomeView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class HomeModule {

    @Provides
    @PerActivity
    IHomePresenter<HomeView,IHomeInteractor> provideHomePresenter(HomePresenter<HomeView,IHomeInteractor> homePresenter){
        return  homePresenter;
    }

    @Provides
    @PerActivity
    IHomeInteractor provideHomeInteractor (HomeInteractor homeInteractor) {
        return  homeInteractor;
    }

    @Provides
    @PerActivity
    HomeAdapter provideHomeAdapter(@ActivityContext Context context){
        return new HomeAdapter(context);
    }

    @Provides
    @PerActivity
    BannerViewPagerAdapter provideBannerViewPagerAdapter(@ActivityContext Context context){
        return new BannerViewPagerAdapter(context);
    }

   /* @Provides
    @PerActivity
    LinearLayoutManager provideLinearManagerHome(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }*/
}
