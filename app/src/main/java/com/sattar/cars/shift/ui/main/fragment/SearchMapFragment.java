package com.sattar.cars.shift.ui.main.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.di.component.DaggerSearchMapComponent;
import com.sattar.cars.shift.di.component.SearchMapComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.SearchMapModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.interactor.ISearchMapInteractor;
import com.sattar.cars.shift.ui.main.presenter.ISearchMapPresenter;
import com.sattar.cars.shift.ui.main.view.SearchMapView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by tarek on 11/02/17.
 */

public class SearchMapFragment extends BaseFragment implements SearchMapView, OnMapReadyCallback,
        PlaceSelectionListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMapClickListener, ResultCallback<LocationSettingsResult> {


    @BindView(R.id.myLocationButton)
    CustomTextView myLocationButton;

    @BindView(R.id.search_container)
    FrameLayout searchContainer;

    @BindView(R.id.search_map_error)
    CustomTextView searchMapError;

    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;

    @BindView(R.id.search_map_view_more_compaines_button)
    LinearLayout searchMapViewMoreCompainesButton;

    public static final String TAG = "SearchMapFragment";
   


    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private SupportPlaceAutocompleteFragment autocompleteFragment;
    private boolean mPermissionDenied = false;

    private GoogleApiClient mGoogleApiClient;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
    int REQUEST_CHECK_SETTINGS = 100;

    private MarkerOptions myMarkerOptions;

    @Inject
    ISearchMapPresenter<SearchMapView, ISearchMapInteractor> searchMapPresenter;

    SearchMapComponent searchMapComponent;
    private DepartmentResponse.Data data;


    public static SearchMapFragment newInstance() {
        Bundle args = new Bundle();
        SearchMapFragment fragment = new SearchMapFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_map, container, false);
        System.err.println("onCreateView LocationMapFragment");
        setUnBinder(ButterKnife.bind(this, view));
        searchMapComponent = DaggerSearchMapComponent.builder()
                .searchMapModule(new SearchMapModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .networkModule(new NetworkModule(""))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .build();

        searchMapComponent.inject(this);

        searchMapPresenter.onAttach(this);
        setShowMenu(false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        System.err.println("onViewCreated LocationMapFragment");
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        autocompleteFragment = (SupportPlaceAutocompleteFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setHint(getString(R.string.search_for_area));
        /*AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("EG")
                .build();
        autocompleteFragment.setFilter(typeFilter);*/
        // Register a listener to receive callbacks when a place has been selected or an error has
        // occurred.
        autocompleteFragment.setOnPlaceSelectedListener(this);

        myLocationButton.setEnabled(false);


    }

    @Override
    protected void setUp(View view) {
        ((MainActivity) getBaseActivity()).removeHamburgerIcon(true);
        ((MainActivity)getBaseActivity()).getSupportActionBar().setTitle(getString(R.string.choose_place));

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        checkForLocationPermission();

        mMap.setOnMyLocationButtonClickListener(this);

        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ///   Toast.makeText(getBaseActivity(), ((StoreBranchResponse) marker.getTag()).getTitle().getEn(), Toast.LENGTH_SHORT).show();
                if (!mCurrLocationMarker.equals(marker)) {
                    //  ((MainActivity) getBaseActivity()).showListOfSpecificCampaignsFragment(AppConstants.COME_FROM_BRANCH, null, null, ((StoreBranchResponse) marker.getTag()).getId(), null, null, null, null);
                }
                return false;
            }
        });

        // mMap.setBuildingsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);

    }


    @Override
    public void onPlaceSelected(Place place) {

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
            mMap.clear();
        }
        mCurrLocationMarker = mMap.addMarker(new MarkerOptions().title((String) place.getAddress()).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_green)).position(place.getLatLng()).title((String) place.getName()));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 14.0f));
        HashMap<String, String> options = new HashMap<>();
        options.put("latitude", String.valueOf(mCurrLocationMarker.getPosition().latitude));
        options.put("longitude", String.valueOf(mCurrLocationMarker.getPosition().longitude));
        options.put("department_id", data.getId());
        options.put("lang", getString(R.string.language));

        searchMapPresenter.loadSearchByLocationOffers(options);

        /*if (getViewType() == ApplicationConstant.NEARBY_CAR) {
            getNearbyLocationFromBeliaa(place.getLatLng().latitude, place.getLatLng().longitude);
        }*/
    }

    @Override
    public void onError(Status status) {
        System.err.println("status: " + status.getStatusMessage());
        Toast.makeText(getActivity(), "Place selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onResume() {
        super.onResume();
        System.err.println("onResume");
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            //showMissingPermissionError();
            mPermissionDenied = false;
        }
    }


    public void checkForLocationPermission() {
        if (Build.VERSION.SDK_INT < 23) {
            buildGoogleApiClient();


        } else {
            requestStoragePermission();
        }
    }

    private void requestStoragePermission() {

        int externalStorage = ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.ACCESS_FINE_LOCATION);

        if (externalStorage != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            buildGoogleApiClient();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.LOCATION_PERMISSION_REQUEST_CODE:
                // Check if the only required permission has been granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    buildGoogleApiClient();

                } else {
                    Toast.makeText(getActivity(), "You don't have permission to access storage", Toast.LENGTH_LONG).show();
                    mPermissionDenied = true;
                }
                break;
        }
    }


    @SuppressLint("MissingPermission")
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getBaseActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        mMap.setMyLocationEnabled(true);
        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(getBaseActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(
                            mGoogleApiClient,
                            builder.build()
                    );

            result.setResultCallback(this);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");


        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_green));

        myMarkerOptions = markerOptions;

        mCurrLocationMarker = mMap.addMarker(markerOptions);
        //move map camera
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myMarkerOptions.getPosition(), 14));

        HashMap<String, String> options = new HashMap<>();
        options.put("latitude", String.valueOf(mCurrLocationMarker.getPosition().latitude));
        options.put("longitude", String.valueOf(mCurrLocationMarker.getPosition().longitude));
        options.put("department_id", data.getId());
        options.put("lang", getString(R.string.language));

        searchMapPresenter.loadSearchByLocationOffers(options);

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        autocompleteFragment.setText("");
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker = mMap.addMarker(myMarkerOptions);
            //move map camera
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myMarkerOptions.getPosition(), 14));
            HashMap<String, String> options = new HashMap<>();
            options.put("latitude", String.valueOf(mCurrLocationMarker.getPosition().latitude));
            options.put("longitude", String.valueOf(mCurrLocationMarker.getPosition().longitude));
            options.put("department_id", data.getId());
            options.put("lang", getString(R.string.language));

            searchMapPresenter.loadSearchByLocationOffers(options);
        }
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {

        mMap.clear();
        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();


        intentBuilder.setLatLngBounds(mMap.getProjection().getVisibleRegion().latLngBounds);
        Intent intent = null;
        try {
            intent = intentBuilder.build(getBaseActivity());
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

        mCurrLocationMarker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_green)));
        HashMap<String, String> options = new HashMap<>();
        options.put("latitude", String.valueOf(mCurrLocationMarker.getPosition().latitude));
        options.put("longitude", String.valueOf(mCurrLocationMarker.getPosition().longitude));
        options.put("department_id", data.getId());
        options.put("lang", getString(R.string.language));


        searchMapPresenter.loadSearchByLocationOffers(options);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.REQUEST_PLACE_PICKER) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getBaseActivity(), data);
                mMap.clear();
                mCurrLocationMarker = mMap.addMarker(new MarkerOptions().title((String) place.getAddress()).position(place.getLatLng()).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_green)));
                // Clears the previously touched position
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 14));
                String toastMsg = String.format("Place: %s", place.getName());
            }
        } else if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {

                Toast.makeText(getActivity(), "GPS enabled", Toast.LENGTH_LONG).show();
            } else {

                Toast.makeText(getActivity(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }

        }
    }


    @OnClick(R.id.myLocationButton)
    public void onMyLocationButtonClickListner() {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        autocompleteFragment.setText("");
        mCurrLocationMarker = mMap.addMarker(myMarkerOptions);
        //move map camera
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myMarkerOptions.getPosition(), 14));
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;

                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the login_ic_user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }


    @Override
    public void showErrorMessage(int connectionError, int noWifi) {

    }

    @Override
    public void showStoresInMap(ArrayList<CompaniesItemResponse> data) {
        /*View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        CustomTextView cityNameTextView = marker.findViewById(R.id.cityNameTextView);
        for (CompaniesItemResponse storeBranchResponse : data) {
            if(getString(R.string.language).equals(AppConstants.ARABIC)) {
                cityNameTextView.setText(storeBranchResponse.getStore().getTitle().getAr());
            }else {
                cityNameTextView.setText(storeBranchResponse.getStore().getTitle().getEn());
            }

            Marker locationMarker = mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(AppUtils.createDrawableFromView(getActivity(), marker)))
                    .position(new LatLng(Double.parseDouble(storeBranchResponse.getLatitude()), Double.parseDouble(storeBranchResponse.getLongitude()))));
            locationMarker.setTag(storeBranchResponse);

        }*/

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        searchMapPresenter.onDetach();
    }

    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            searchContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            searchContainer.setVisibility(View.VISIBLE);
        }
    }

    public void setData(DepartmentResponse.Data data) {
        this.data = data;
    }

    @OnClick(R.id.search_map_view_more_compaines_button)
    public void onViewClicked() {
        ((MainActivity)getBaseActivity()).showCompaniesListFragment(0,data);
    }

    public DepartmentResponse.Data getData() {
        return data;
    }
}
