package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IServiceDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.ServiceDetailsView;

@PerActivity
public interface IServiceDetailsPresenter<V extends ServiceDetailsView,I extends IServiceDetailsInteractor> extends IBasePresenter<V,I> {
    void getAvaliableTime(String lang, String departmentId);

    void getMakeOrder(String lang, String selectedDate, String selectedTime, String orderId, String clintId);

    void getServiceDetails(String lang, String serviceId, String companyId);

    boolean validateUserData(String selectedWashType, String selectedCarSize, String date, String time, String selectedDuration);
}

