package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterErrorResponse {
    @SerializedName("errors")
    @Expose
    private RegisterErrors errors;

    public RegisterErrors getErrors() {
        return errors;
    }

    public void setErrors(RegisterErrors errors) {
        this.errors = errors;
    }

}
