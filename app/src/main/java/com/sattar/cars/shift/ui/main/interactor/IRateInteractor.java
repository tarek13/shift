package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;

import io.reactivex.Observable;

public interface IRateInteractor extends IBaseInteractor {

    Observable<GeneralResponse> rateOrder(HashMap<String, String> options);
}
