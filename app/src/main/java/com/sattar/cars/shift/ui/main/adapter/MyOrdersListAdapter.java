package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.ui.main.holder.MyOrderItemViewHolder;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

public class MyOrdersListAdapter extends RecyclerView.Adapter<MyOrderItemViewHolder> {



    // View Types
    private ArrayList<MyOrderItemResponse> myOrderItemResponseArrayList = new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public MyOrdersListAdapter(Context context) {
        this.context = context;
    }

    public interface Callback {
        void onDataItemClick(int position, MyOrderItemResponse myOrderItemResponse);
    }

    public void setData(ArrayList<MyOrderItemResponse> myOrderItemResponses) {
        this.myOrderItemResponseArrayList = myOrderItemResponses;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public MyOrderItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyOrderItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_my_order_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrderItemViewHolder holder, int position) {
        holder.setData(myOrderItemResponseArrayList.get(position), position);
    }


    @Override
    public int getItemCount() {
        return myOrderItemResponseArrayList.size();
    }
}
