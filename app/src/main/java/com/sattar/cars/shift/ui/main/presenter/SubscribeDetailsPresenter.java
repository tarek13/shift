package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.data.network.model.SubscribesResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.ISubscribeDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.SubscribeDetailsView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class SubscribeDetailsPresenter<V extends SubscribeDetailsView,I extends ISubscribeDetailsInteractor>
        extends BasePresenter<V,I> implements ISubscribeDetailsPresenter<V,I> {
    private static final String TAG = "Subscribeslist";

    @Inject
    public SubscribeDetailsPresenter(I mvpInteractor,
                                     SchedulerProvider schedulerProvider,
                                     CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getAvaliableTime(String lang, String selectedDate) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("date", selectedDate);;
        options.put("lang", lang);

        getCompositeDisposable().add(getInteractor().getAvaliableTime(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<AvaliableTimeResponse>() {
                    @Override
                    public void accept(AvaliableTimeResponse avaliableTimeResponse) {
                        getMvpView().hideLoading();

                        if(avaliableTimeResponse.getStatus()) {
                           getMvpView().showAvaliableTime(avaliableTimeResponse.getData());
                        }else {
                            getMvpView().onError(avaliableTimeResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoading();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                              getMvpView().onError(R.string.some_error);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.connection_error);

                        }
                    }
                }));
    }

    @Override
    public void getMakeOrder(String lang, String selectedDate, String selectedTime, String orderId, String clintId) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("date", selectedDate);;
        options.put("lang", lang);
        options.put("time_from", selectedTime);
        options.put("order_num", orderId);
        options.put("client_id", clintId);

        getCompositeDisposable().add(getInteractor().makerSubscribeOrder(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<GeneralResponse>() {
                    @Override
                    public void accept(GeneralResponse generalResponse) {
                        getMvpView().hideLoading();

                        if(generalResponse.getStatus()) {
                           getMvpView().showMessage(generalResponse.getMessage(),1);
                        }else {
                            getMvpView().onError(generalResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoading();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                              getMvpView().onError(R.string.some_error);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.connection_error);

                        }
                    }
                }));
    }
    @Override
    public boolean validateUserData(String date, String time) {
        if (date == null || date.trim().isEmpty() || date.trim().length() <= 0) {
            getMvpView().onError(R.string.date_error);
            return false;
        } else if (time == null || time.trim().isEmpty() || time.trim().length() <= 0) {
            getMvpView().onError(R.string.time_error_1);
            return false;
        }

        return true;
    }
}
