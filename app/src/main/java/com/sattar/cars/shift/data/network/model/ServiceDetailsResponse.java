package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Objects;

public class ServiceDetailsResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("service")
        @Expose
        private Service service;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }

    }

    public class Service {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("company_id")
        @Expose
        private String companyId;
        @SerializedName("package")
        @Expose
        private ArrayList<Package> _package = null;
        @SerializedName("discount")
        @Expose
        private Discount discount;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("descrbtion")
        @Expose
        private String descrbtion;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public ArrayList<Package> getPackage() {
            return _package;
        }

        public void setPackage(ArrayList<Package> _package) {
            this._package = _package;
        }

        public Discount getDiscount() {
            return discount;
        }

        public void setDiscount(Discount discount) {
            this.discount = discount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescrbtion() {
            return descrbtion;
        }

        public void setDescrbtion(String descrbtion) {
            this.descrbtion = descrbtion;
        }

    }

    public class Discount {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("discount")
        @Expose
        private String discount;
        @SerializedName("discount3")
        @Expose
        private String discount3;
        @SerializedName("discount6")
        @Expose
        private String discount6;
        @SerializedName("discount12")
        @Expose
        private String discount12;
        @SerializedName("car_wash3")
        @Expose
        private String carWash3;
        @SerializedName("car_wash6")
        @Expose
        private String carWash6;
        @SerializedName("car_wash12")
        @Expose
        private String carWash12;
        @SerializedName("deleted")
        @Expose
        private String deleted;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getDiscount3() {
            return discount3;
        }

        public void setDiscount3(String discount3) {
            this.discount3 = discount3;
        }

        public String getDiscount6() {
            return discount6;
        }

        public void setDiscount6(String discount6) {
            this.discount6 = discount6;
        }

        public String getDiscount12() {
            return discount12;
        }

        public void setDiscount12(String discount12) {
            this.discount12 = discount12;
        }

        public String getCarWash3() {
            return carWash3;
        }

        public void setCarWash3(String carWash3) {
            this.carWash3 = carWash3;
        }

        public String getCarWash6() {
            return carWash6;
        }

        public void setCarWash6(String carWash6) {
            this.carWash6 = carWash6;
        }

        public String getCarWash12() {
            return carWash12;
        }

        public void setCarWash12(String carWash12) {
            this.carWash12 = carWash12;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class Package {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("size")
        @Expose
        private ArrayList<Size> size = null;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ArrayList<Size> getSize() {
            return size;
        }

        public void setSize(ArrayList<Size> size) {
            this.size = size;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }


    public class Size {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("time")
        @Expose
        private String time;
        @SerializedName("icon")
        @Expose
        private String icon;
        @SerializedName("name")
        @Expose
        private String name;
        private boolean selected;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Size)) return false;
            Size size = (Size) o;
            return id.equals(size.id);
        }


    }
}
