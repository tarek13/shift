package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.SubscribesListModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.SubscribesListFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {SubscribesListModule.class})
public interface SubscribesListComponent {

    void inject(SubscribesListFragment subscribesListFragment);
}
