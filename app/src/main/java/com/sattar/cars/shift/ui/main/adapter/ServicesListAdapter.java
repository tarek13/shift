package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.ServicesItemResponse;
import com.sattar.cars.shift.ui.main.holder.ServiceItemViewHolder;

import java.util.ArrayList;

import javax.inject.Inject;

public class ServicesListAdapter extends RecyclerView.Adapter<ServiceItemViewHolder> {


    // View Types
    private ArrayList<ServicesItemResponse> servicesItemResponseArrayList = new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public ServicesListAdapter(Context context) {
        this.context = context;
    }

    public interface Callback {
        void onServiceItemClick(int position, ServicesItemResponse servicesItemResponse);
    }

    public void setData(ArrayList<ServicesItemResponse> servicesItemResponses) {
        this.servicesItemResponseArrayList = servicesItemResponses;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public ServiceItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ServiceItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_service_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceItemViewHolder holder, int position) {
        holder.setService(servicesItemResponseArrayList.get(position), position);
    }


    @Override
    public int getItemCount() {
        return servicesItemResponseArrayList.size();
    }
}
