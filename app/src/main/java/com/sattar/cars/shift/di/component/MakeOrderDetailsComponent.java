package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.MakeOrderDetailsModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.MakeOrderDetailsFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {MakeOrderDetailsModule.class})
public interface MakeOrderDetailsComponent {

    void inject(MakeOrderDetailsFragment makeOrderDetailsFragment);
}
