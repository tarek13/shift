/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.sattar.cars.shift.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.di.qualifier.PreferenceInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {



    private final SharedPreferences mPrefs;
    private String PREF_CURRENT_USER="PREF_CURRENT_USER";
    private static final String PREF_CURRENT_CATEGORY = "PREF_CURRENT_CATEGORY";
    private static final String PREF_CURRENT_MAIN_CATEGORY = "PREF_CURRENT_MAIN_CATEGORY";
    private static final String PREF_CURRENT_LANGUAGE = "PREF_CURRENT_LANGUAGE";
    private static final String PREF_CHANGE_LANGUAGE_FLAG = "PREF_CHANGE_LANGUAGE_FLAG";

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void saveCurrentUserInfo(UserInfo userInfo) {
        Gson gson = new Gson();
        String userInfoListJsonString = gson.toJson(userInfo);
        mPrefs.edit().putString(PREF_CURRENT_USER, userInfoListJsonString).apply();
    }

    @Override
    public UserInfo getCurrentUser() {
        Gson gson = new Gson();
        return gson.fromJson(mPrefs.getString(PREF_CURRENT_USER, ""), UserInfo.class);
    }

    @Override
    public void removeCurrentUser(){
        mPrefs.edit().remove(PREF_CURRENT_USER).apply();
    }
}
