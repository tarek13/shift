package com.sattar.cars.shift.ui.main.interactor;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.UserApis;
import com.sattar.cars.shift.data.network.model.CountriesResponse;
import com.sattar.cars.shift.data.network.model.RegisterErrorResponse;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.AppUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditUserInfoInteractor extends BaseInteractor implements IEditUserInfoInteractor {

    private Context mContext;
    private UserApis userApis;

    private Retrofit retrofit;

    @Inject
    public EditUserInfoInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper , UserApis userApis, Retrofit retrofit) {
        super(preferencesHelper);
        mContext=context;
        this.userApis=userApis;
        this.retrofit=retrofit;
    }

    @Override
    public Observable<RegisterResponse> doUserEditProfile(Map<String,String> registerRequestBody) {
        return userApis.doEditProfile(registerRequestBody);
    }

    @Override
    public ArrayList<CountriesResponse.Item> getCountries() {
        String jsonString = AppUtils.getAssetsJSON(mContext, AppConstants.JSON_FILE_ANDROID_WEAR);
        Log.d("ss", "Json: " + jsonString);
        Gson gson = new Gson();
        CountriesResponse baseWatch = gson.fromJson(jsonString, CountriesResponse.class);
        return  baseWatch.getItems();
    }

    @Override
    public void saveUserToSharedPreferences(UserInfo userInfo) {
        getPreferencesHelper().saveCurrentUserInfo(userInfo);
    }

    @Override
    public RegisterErrorResponse handleErrorResponse(Response<?> response) {
        Converter<ResponseBody, RegisterErrorResponse> converter =
                retrofit.responseBodyConverter(RegisterErrorResponse.class, new Annotation[0]);

        RegisterErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new RegisterErrorResponse();
        }

        return error;
    }
}
