package com.sattar.cars.shift.ui.main.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.network.OrdersApi;
import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;
import com.sattar.cars.shift.utils.NetworkUtils;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SearchMapInteractor extends BaseInteractor implements ISearchMapInteractor {

    private final OrdersApi ordersApi;
    private Context context;

    @Inject
    public SearchMapInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper, OrdersApi ordersApi) {
        super(preferencesHelper);
        this.context=context;
        this.ordersApi = ordersApi;
    }


    @Override
    public Observable<CompaniesResponse> searchByCampanies(Map<String, String> options) {
        return ordersApi.searchByCompanies(options);
    }

    @Override
    public boolean checkNetworkConnection() {
        return NetworkUtils.isNetworkConnected(context);
    }
}
