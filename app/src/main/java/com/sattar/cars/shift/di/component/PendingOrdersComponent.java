package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.PendingOrdersModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.PendingOrdersFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {PendingOrdersModule.class})
public interface PendingOrdersComponent {

    void inject(PendingOrdersFragment pendingOrdersFragment);
}
