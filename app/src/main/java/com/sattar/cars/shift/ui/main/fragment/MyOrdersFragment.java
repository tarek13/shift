package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.MyOrdersFragmentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yahia on 05/10/15.
 */
public class MyOrdersFragment extends BaseFragment {
    public static final String TAG = "FriendsListFragment";
    @BindView(R.id.my_orders_tabs)
    TabLayout myOrdersTabs;

    @BindView(R.id.my_orders_viewpager)
    ViewPager myOrdersViewpager;


    private MyOrdersFragmentAdapter adapter;
    private int selectedPages;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        MyOrdersFragment fragment = new MyOrdersFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.err.println(" in onCreateView ...");
        View rootView = inflater.inflate(R.layout.fragment_my_orders, container, false);
        setUnBinder(ButterKnife.bind(this, rootView));

        adapter = new MyOrdersFragmentAdapter(getChildFragmentManager(), getBaseActivity());
        myOrdersViewpager.setAdapter(adapter);
        myOrdersTabs.setupWithViewPager(myOrdersViewpager);
        //createTabIcons();
        myOrdersViewpager.setCurrentItem(selectedPages, true);
        //setShowMenu(false, pres.getInteractor().getPreferencesHelper().getCurrentUser());
        return rootView;
    }

    @Override
    protected void setUp(View view) {
        ((MainActivity)getBaseActivity()).getSupportActionBar().setTitle(R.string.menu_my_orders_title);


    }



   /* private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_account,
                R.drawable.navigation_menu_ic_devices,
                R.drawable.navigation_menu_ic_group,
                R.drawable.navigation_menu_ic_profiles,
                R.drawable.navigation_menu_ic_events
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
    }*/


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setSelectedPages(int selectedPages) {
        this.selectedPages = selectedPages;
    }

    
}
