package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IHomeInteractor;
import com.sattar.cars.shift.ui.main.view.HomeView;

@PerActivity
public interface IHomePresenter<V extends HomeView,I extends IHomeInteractor> extends IBasePresenter<V,I> {

    void loadDepartmentsData(String string);

    void loadBannerData(String string);

    void onBannerPageSelected(int position);
}

