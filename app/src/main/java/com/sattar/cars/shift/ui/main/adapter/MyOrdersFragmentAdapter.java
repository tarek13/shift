package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ui.main.fragment.FinishedOrdersFragment;
import com.sattar.cars.shift.ui.main.fragment.PendingOrdersFragment;
import com.sattar.cars.shift.ui.main.fragment.UnCompletedOrdersFragment;



/**
 * Created by tarek on 25/09/16.
 */

public class MyOrdersFragmentAdapter extends FragmentPagerAdapter {


    private static final String[] tabsTitles = new String[]{"Scan code", "Vouchers history"};
    private static final int NUMBER_OF_TABS = 3;
    private static final int PENDING_TAB = 0;
    private static final int UNFINISHED_TAB = 1;
    private static final int FINISHED_TAB = 2;
    private Fragment fragment;
    private Context context;


    public MyOrdersFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context=context;
    }


  

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case PENDING_TAB:
                fragment= new PendingOrdersFragment();
                break;
            case UNFINISHED_TAB:
                fragment= new UnCompletedOrdersFragment();
                break;
                case FINISHED_TAB:
                fragment= new FinishedOrdersFragment();
                break;
            default:
                break;
        }

        return fragment;
    }



    @Override
    public int getCount() {
        return NUMBER_OF_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        // return null to display only the icon
        switch (position) {
            case 0:
                return context.getString(R.string.pending);
            case 1:
                return context.getString(R.string.unfinshed);
                case 2:
                return context.getString(R.string.finished);
            default:
                return null;
        }

    }

}