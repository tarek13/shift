package com.sattar.cars.shift.ui.login.interactor;

import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.LoginErrorResponse;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;

public interface ILoginInteractor extends IBaseInteractor {

    Observable<RegisterResponse> doUserLogin(Map<String,String>  loginRequestBody);

    Observable<RegisterResponse> doSocialLogin(HashMap<String, String> options);

    void saveUserToSharedPreferences(UserInfo userInfo);

    LoginErrorResponse handleErrorResponse(Response<?> response);

}
