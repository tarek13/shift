package com.sattar.cars.shift.di.component;


import com.sattar.cars.shift.di.module.ForgetPasswordModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.forgetpassword.ForgetPasswordActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ForgetPasswordModule.class})
public interface ForgetPasswordComponent {

    void inject(ForgetPasswordActivity forgetPasswordActivity);
}
