package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.AboutUsResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

public interface AboutUsView extends IBaseView {

    void showAboutUs(AboutUsResponse.Result result);


    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);
}
