package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.ServicesItemResponse;
import com.sattar.cars.shift.data.network.model.SearchBodyRequest;
import com.sattar.cars.shift.di.component.ServicesListComponent;
import com.sattar.cars.shift.di.component.DaggerServicesListComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.ServicesListModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.ServicesListAdapter;
import com.sattar.cars.shift.ui.main.interactor.IServicesListInteractor;
import com.sattar.cars.shift.ui.main.presenter.IServicesListPresenter;
import com.sattar.cars.shift.ui.main.view.ServicesListView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesListFragment extends BaseFragment implements ServicesListView, ServicesListAdapter.Callback {
    public static final String TAG = "ServicesListFragment";

    @BindView(R.id.services_list_recycler_view)
    RecyclerView servicesListRecyclerView;

    @BindView(R.id.services_list_error)
    CustomTextView servicesListError;

    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;

    @BindView(R.id.services_list_continer)
    CardView servicesListContiner;

    @BindView(R.id.services_list_title_text_view)
    CustomTextView servicesListTitleTextView;

    @Inject
    IServicesListPresenter<ServicesListView, IServicesListInteractor> servicesListPresenter;

    @Inject
    ServicesListAdapter servicesListAdapter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    ServicesListComponent servicesListComponent;

    int comeFrom;


    private SearchBodyRequest searchRequestBody;
    private CompaniesItemResponse data;
    private DepartmentResponse.Data departmentData;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        ServicesListFragment fragment = new ServicesListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setComeFrom(int comeFrom, CompaniesItemResponse data, DepartmentResponse.Data departmentData) {
        this.comeFrom = comeFrom;
        this.data=data;
        this.departmentData=departmentData;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_services_list, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        servicesListComponent = DaggerServicesListComponent.builder()
                .servicesListModule(new ServicesListModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        servicesListComponent.inject(this);

        servicesListPresenter.onAttach(this);
     //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        ((MainActivity)getBaseActivity()).getSupportActionBar().setTitle(data.getName());

        servicesListRecyclerView.setLayoutManager(new GridLayoutManager(getBaseActivity(),2));
        servicesListRecyclerView.setAdapter(servicesListAdapter);
        servicesListAdapter.setCallback(this);
        servicesListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //servicesListPresenter.loadAllServices(getString(R.string.language),data.getCompanyId());
        showServicesList(data.getServices());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        servicesListPresenter.onDetach();
    }


    @Override
    public void showServicesList(ArrayList<ServicesItemResponse> servicesItemResponses) {
        if (servicesItemResponses != null && servicesItemResponses.size() > 0) {
            servicesListRecyclerView.setVisibility(View.VISIBLE);
            servicesListError.setVisibility(View.GONE);
            servicesListAdapter.setData(servicesItemResponses);
        } else {
            showErrorMessage(R.string.no_compaines_found, R.drawable.not_found);
        }


    }


    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            servicesListContiner.setVisibility(View.GONE);

        }

    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            servicesListContiner.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
        servicesListContiner.setVisibility(View.GONE);
        servicesListError.setVisibility(View.VISIBLE);
        servicesListError.setText(messageID);
        servicesListError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void onServiceItemClick(int position, ServicesItemResponse servicesItemResponse) {
       ((MainActivity)getBaseActivity()).showServiceDetailsFragment(servicesItemResponse.getServiceId(),servicesItemResponse.getCompanyId(),departmentData.getId(),data.getName());
    }

    public void setSearchRequestBody(SearchBodyRequest searchRequestBody) {
        this.searchRequestBody = searchRequestBody;
    }

    public SearchBodyRequest getSearchRequestBody() {
        return searchRequestBody;
    }

    public CompaniesItemResponse getData() {
        return data;
    }
}
