package com.sattar.cars.shift.data.db.model;

public class ImageVideoData {
    private  String isVideo;
    private String url;

    public String getIsVideo() {
        return isVideo;
    }

    public void setIsVideo(String isVideo) {
        this.isVideo = isVideo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
