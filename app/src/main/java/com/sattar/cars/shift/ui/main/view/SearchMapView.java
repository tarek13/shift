package com.sattar.cars.shift.ui.main.view;

import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface SearchMapView extends IBaseView {
    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int connectionError, int noWifi);

    void showStoresInMap(ArrayList<CompaniesItemResponse> data);
    //void showMainCategorySearchList(ArrayList<MainCategoryInfo> mainCategoryInfoArrayList);


}
