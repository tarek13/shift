package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.ISearchMapInteractor;
import com.sattar.cars.shift.ui.main.view.SearchMapView;

import java.util.HashMap;

@PerActivity
public interface ISearchMapPresenter<V extends SearchMapView,I extends ISearchMapInteractor> extends IBasePresenter<V,I> {

    void loadSearchByLocationOffers(HashMap<String, String> options);
}
