package com.sattar.cars.shift.ui.login.presenter;

import com.sattar.cars.shift.data.network.model.LoginRequestBody;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.login.interactor.ILoginInteractor;
import com.sattar.cars.shift.ui.login.view.LoginView;

@PerActivity
public interface  ILoginPresenter<V extends LoginView, I extends ILoginInteractor>
        extends IBasePresenter<V, I> {

    void onForgetPasswordButtonClick();

    void onRegisterButtonClick();

    void onLoginButtonClick(LoginRequestBody loginRequestBody);


    boolean validateUserData(String email, String password);

    void loadUserEmailPasswordFromPrefs();
}
