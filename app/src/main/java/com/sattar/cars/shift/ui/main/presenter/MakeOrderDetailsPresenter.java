package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IMakeOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.MakeOrderDetailsView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class MakeOrderDetailsPresenter<V extends MakeOrderDetailsView,I extends IMakeOrderDetailsInteractor>
        extends BasePresenter<V,I> implements IMakeOrderDetailsPresenter<V,I> {
    private static final String TAG = "MakeOrderslist";

    @Inject
    public MakeOrderDetailsPresenter(I mvpInteractor,
                                     SchedulerProvider schedulerProvider,
                                     CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }




    @Override
    public void getMakeOrder(HashMap<String, String> options) {
        getMvpView().showLoading();


        getCompositeDisposable().add(getInteractor().makerServiceOrder(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<GeneralResponse>() {
                    @Override
                    public void accept(GeneralResponse generalResponse) {
                        getMvpView().hideLoading();

                        if(generalResponse.getStatus()) {
                            getMvpView().existFragment(generalResponse.getMessage());
                        }else {
                            getMvpView().onError(generalResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoading();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().onError(R.string.some_error);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.connection_error);

                        }
                    }
                }));
    }



}
