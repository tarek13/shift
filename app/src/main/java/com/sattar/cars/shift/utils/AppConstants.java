

package com.sattar.cars.shift.utils;

import android.content.Context;

import com.sattar.cars.shift.R;

public final class AppConstants {

    public static final long SPLASH_TIME_OUT = 2000;

    public static final String WEBSITE_API_URL_1 = "http://ur-business.net/shift_new/Api/";
    public static final String WEBSITE_API_URL_2 = "http://ur-business.net/shift_new/Api/";

    public static final String PREF_NAME = "shift_pref";


    public static final int CUSTOM_LOGIN = 0;
    public static final String ENGLISH = "en";
    public static final String ARABIC = "ar";



    public static final String GENDER_FEMALE_TYPE = "2";
    public static final String GENDER_MALE_TYPE = "1";
    public static final String JSON_FILE_ANDROID_WEAR = "nationalities.json";
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    public static final int REQUEST_PLACE_PICKER = 2;

    public static String[] getMainMenuTitle(Context context) {
        return new String[]{context.getString(R.string.menu_home_title),context.getString(R.string.menu_profile_info_title), context.getString(R.string.menu_my_orders_title),context.getString(R.string.menu_about_us_title), context.getString(R.string.menu_logout_title)};
    }

    public static int MAIN_MENU_ICON[] = {R.drawable.home_m,R.drawable.user_p, R.drawable.cart_m, R.drawable.info_m, R.drawable.logout_m};

}

/*
        android:fontFamily="sans-serif"           // roboto regular
        android:fontFamily="sans-serif-light"     // roboto light
        android:fontFamily="sans-serif-condensed" // roboto condensed
        android:fontFamily="sans-serif-black"     // roboto black
        android:fontFamily="sans-serif-thin"      // roboto thin (android 4.2)
        android:fontFamily="sans-serif-medium"    // roboto medium (android 5.0)*/
