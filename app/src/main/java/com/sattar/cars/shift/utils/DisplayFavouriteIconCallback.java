package com.sattar.cars.shift.utils;

import android.view.View;

public abstract class DisplayFavouriteIconCallback {
     private View imageView;

     public DisplayFavouriteIconCallback(View imageView) {
          this.imageView = imageView;
     }

     public View getImageView() {
          return imageView;
     }

     public abstract void changeFavouriteIcon(boolean isFavourite);
     public abstract void hideFavouriteIcon();
}
