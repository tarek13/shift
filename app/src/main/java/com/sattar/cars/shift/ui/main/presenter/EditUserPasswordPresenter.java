package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IEditUserPasswordInteractor;
import com.sattar.cars.shift.ui.main.view.EditUserPasswordView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;
import retrofit2.Retrofit;

public class EditUserPasswordPresenter<V extends EditUserPasswordView, I extends IEditUserPasswordInteractor>
        extends BasePresenter<V, I> implements IEditUserPasswordPresenter<V, I> {

    @Inject
    public EditUserPasswordPresenter(I mvpInteractor,
                                     SchedulerProvider schedulerProvider,
                                     CompositeDisposable compositeDisposable, Retrofit retrofit) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onSaveButtonClick(final RegisterRequestBody registerRequestBody) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("client_id",getInteractor().getPreferencesHelper().getCurrentUser().getId());
        options.put("lang",registerRequestBody.getLang());
        options.put("old_password",registerRequestBody.getPassword());
        options.put("new_password",registerRequestBody.getNewPassword());


        getCompositeDisposable().add(getInteractor().doUserEditProfile(options)
                .subscribeOn(getSchedulerProvider().io())

                .observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<RegisterResponse>() {
                    @Override
                    public void accept(RegisterResponse registerResponse) throws Exception {
                        getMvpView().hideLoading();
                        if(registerResponse.getStatus()) {
                            getInteractor().getPreferencesHelper().removeCurrentUser();
                            getMvpView().exitTheApplication();
                        }else {
                            getMvpView().onError(registerResponse.getMessage());
                        }
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().hideLoading();
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().onError(R.string.some_error);

                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.some_error);
                        }

                    }
                }));

    }


    @Override
    public boolean validateUserData(String oldPassword, String password, String confirmPassword) {
        /* if (oldPassword == null || oldPassword.trim().isEmpty() || oldPassword.trim().length() <= 0) {
            getMvpView().onOldPasswordError(R.string.old_password_required_error);
            return false;
        }
        else if (!oldPassword.equals(getInteractor().getPreferencesHelper().getCurrentUser().getPassword())) {
            getMvpView().onOldPasswordError(R.string.password_not_match);
            return false;
        }*/
        if (password == null || password.trim().isEmpty() || password.trim().length() <= 0) {
            getMvpView().onNewPasswordError(R.string.password_required_error);
            return false;
        } else if (password.length() < 6) {
            getMvpView().onNewPasswordError(R.string.password_minmimum_length_error);
            return false;
        } else if (confirmPassword == null || confirmPassword.trim().isEmpty() || confirmPassword.trim().length() <= 0) {
            getMvpView().onConfirmPasswordError(R.string.confirm_password_required_error);
            return false;
        } else if (!password.equals(confirmPassword)) {
            getMvpView().onConfirmPasswordError(R.string.password_not_match);
            return false;
        }

        return true;
    }
}
