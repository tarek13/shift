package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IServicesListInteractor;
import com.sattar.cars.shift.ui.main.view.ServicesListView;

@PerActivity
public interface IServicesListPresenter<V extends ServicesListView,I extends IServicesListInteractor> extends IBasePresenter<V,I> {
    void loadAllServices(String lang, String companyId);
}

