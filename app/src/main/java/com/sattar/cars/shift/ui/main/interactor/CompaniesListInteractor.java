package com.sattar.cars.shift.ui.main.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.network.OrdersApi;
import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class CompaniesListInteractor extends BaseInteractor implements ICompaniesListInteractor {

    private Context context;
    private OrdersApi ordersApi;
    private Retrofit retrofit;

    @Inject
    public CompaniesListInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper, OrdersApi ordersApi, Retrofit retrofit) {
        super(preferencesHelper);
        this.context=context;
        this.ordersApi = ordersApi;
        this.retrofit=retrofit;
    }

    @Override
    public Observable<CompaniesResponse> getAllCompanies(HashMap<String, String> options) {
        return ordersApi.searchByCompanies(options);
    }
}
