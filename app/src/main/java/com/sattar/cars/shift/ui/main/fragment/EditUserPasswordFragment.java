package com.sattar.cars.shift.ui.main.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.di.component.DaggerEditUserPasswordComponent;
import com.sattar.cars.shift.di.component.EditUserPasswordComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.EditUserPasswordModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.login.LoginActivity;
import com.sattar.cars.shift.ui.main.interactor.IEditUserPasswordInteractor;
import com.sattar.cars.shift.ui.main.presenter.IEditUserPasswordPresenter;
import com.sattar.cars.shift.ui.main.view.EditUserPasswordView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditUserPasswordFragment extends BaseFragment implements EditUserPasswordView {


    @Inject
    IEditUserPasswordPresenter<EditUserPasswordView, IEditUserPasswordInteractor> editUserPasswordPresenter;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;

    EditUserPasswordComponent editUserPasswordComponent;
    @BindView(R.id.edit_profile_old_password_edit_text)
    EditText editProfileOldPasswordEditText;
    @BindView(R.id.edit_profile_error_old_password_text_view)
    CustomTextView editProfileErrorOldPasswordTextView;
    @BindView(R.id.edit_profile_password_edit_text)
    EditText editProfilePasswordEditText;
    @BindView(R.id.edit_profile_error_password_text_view)
    CustomTextView editProfileErrorPasswordTextView;
    @BindView(R.id.edit_profile_confirm_password_edit_text)
    EditText editProfileConfirmPasswordEditText;
    @BindView(R.id.edit_profile_error_confirm_password_text_view)
    CustomTextView editProfileErrorConfirmPasswordTextView;

    private String newPassword;
    private String oldPassword;
    private String confirmPassword;

    private UserInfo userInfo;

    public static Fragment newInstance() {
        Bundle args = new Bundle();
        EditUserPasswordFragment fragment = new EditUserPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_user_password, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        editUserPasswordComponent = DaggerEditUserPasswordComponent.builder()
                .editUserPasswordModule(new EditUserPasswordModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())

                .build();
        editUserPasswordComponent.inject(this);

        editUserPasswordPresenter.onAttach(this);
        setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {

        userInfo = editUserPasswordPresenter.getInteractor().getPreferencesHelper().getCurrentUser();


        editProfileErrorPasswordTextView.setVisibility(View.GONE);
        editProfileErrorOldPasswordTextView.setVisibility(View.GONE);
        editProfileErrorConfirmPasswordTextView.setVisibility(View.GONE);

    }

    @OnClick({R.id.edit_profile_save_button_container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_profile_save_button_container:
                changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.WEBSITE_API_URL_1);
                showChangePasswordAlertDialog(getActivity(),getString(R.string.change_password_alert_dialog_title),getString(R.string.password_alert_edit));
                break;
        }
    }

    private void fillUserDataFromUI() {


        editProfileErrorPasswordTextView.setVisibility(View.GONE);
        editProfileErrorConfirmPasswordTextView.setVisibility(View.GONE);
        editProfileErrorOldPasswordTextView.setVisibility(View.GONE);

        newPassword = editProfilePasswordEditText.getText().toString();
        oldPassword = editProfileOldPasswordEditText.getText().toString();
        confirmPassword = editProfileConfirmPasswordEditText.getText().toString();


    }

    private RegisterRequestBody buildRegisterRequestBody() {
        RegisterRequestBody editProfileRequestBody = new RegisterRequestBody();

        editProfileRequestBody.setLang(getString(R.string.language));

        editProfileRequestBody.setPassword(oldPassword);
        editProfileRequestBody.setNewPassword(newPassword);


        return editProfileRequestBody;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onOldPasswordError(int passwordRequiredError) {
        editProfileErrorOldPasswordTextView.setVisibility(View.VISIBLE);
        editProfileErrorOldPasswordTextView.setText(passwordRequiredError);
        editProfileOldPasswordEditText.requestFocus();
    }

    @Override
    public void onNewPasswordError(int passwordRequiredError) {
        editProfileErrorPasswordTextView.setVisibility(View.VISIBLE);
        editProfileErrorPasswordTextView.setText(passwordRequiredError);
        editProfilePasswordEditText.requestFocus();
    }
    @Override
    public void onConfirmPasswordError(int confirmPasswordRequiredError) {
        editProfileErrorConfirmPasswordTextView.setVisibility(View.VISIBLE);
        editProfileErrorConfirmPasswordTextView.setText(confirmPasswordRequiredError);
        editProfileConfirmPasswordEditText.requestFocus();
    }

    @Override
    public void exitTheApplication() {
        Intent intent =new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getBaseActivity().finish();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        editUserPasswordPresenter.onDetach();

    }

    public  void showChangePasswordAlertDialog(Context context, String title, String content){
        new MaterialDialog.Builder(context)
                .title(title)
                .titleColorRes(R.color.yellow_gold_color)
                .content(content)
                .positiveText(R.string.agree)
                .positiveColorRes(R.color.purple)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        fillUserDataFromUI();
                        if(editUserPasswordPresenter.validateUserData(oldPassword,newPassword,confirmPassword)){
                            editUserPasswordPresenter.onSaveButtonClick(buildRegisterRequestBody());
                        }
                    }
                })
                .show();
    }
}
