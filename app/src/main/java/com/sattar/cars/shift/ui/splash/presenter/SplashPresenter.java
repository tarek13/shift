/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.sattar.cars.shift.ui.splash.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.LoginRequestBody;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.splash.interactor.ISplashInteractor;
import com.sattar.cars.shift.ui.splash.view.SplashView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

/**
 * Created by janisharali on 27/01/17.
 */

public class SplashPresenter<V extends SplashView, I extends ISplashInteractor>
        extends BasePresenter<V, I> implements com.sattar.cars.shift.ui.splash.presenter.ISplashPresenter<V, I> {

    @Inject
    public SplashPresenter(I mvpInteractor,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);



    }

    @Override
    public UserInfo getCurrentUser(){
        return  getInteractor().getPreferencesHelper().getCurrentUser();

    }

    @Override
    public void onLoginButtonClick(final LoginRequestBody loginRequestBody) {
       // getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("lang",loginRequestBody.getLang());
        options.put("email",loginRequestBody.getEmail());
        options.put("password",loginRequestBody.getPassword());
        getCompositeDisposable().add(getInteractor().doUserLogin(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<RegisterResponse>() {
                    @Override
                    public void accept(RegisterResponse registerResponse) throws Exception {
                        UserInfo userInfo=new UserInfo();
                        getMvpView().hideLoading();
                        if(registerResponse.getStatus()) {
                            userInfo.setId(registerResponse.getData().getId());
                            userInfo.setId(registerResponse.getData().getId());
                            userInfo.setName(registerResponse.getData().getName());
                            userInfo.setEmail(registerResponse.getData().getEmail());
                            userInfo.setPassword(loginRequestBody.getPassword());
                            userInfo.setMobile(registerResponse.getData().getMobile());
                            userInfo.setGender(registerResponse.getData().getGender());
                            userInfo.setAge(registerResponse.getData().getAge());
                            userInfo.setAddress(registerResponse.getData().getAddress());
                            userInfo.setPlatform(registerResponse.getData().getPlatform());
                            userInfo.setPlatformToken(registerResponse.getData().getPlatformToken());
                            userInfo.setActiveCode(registerResponse.getData().getActiveCode());
                            userInfo.setBlocked(registerResponse.getData().getBlocked());
                            userInfo.setBlockReason(registerResponse.getData().getBlockReason());
                            userInfo.setGender(registerResponse.getData().getGender());
                            userInfo.setTypeOfLogin(AppConstants.CUSTOM_LOGIN);
                            getMvpView().hideLoading();
                            getInteractor().saveUserToSharedPreferences(userInfo);
                            getMvpView().openMainActivity();
                        }else {
                            getMvpView().onError(registerResponse.getMessage());
                            getMvpView().openLoginActivity();

                        }

                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        getMvpView().hideLoading();
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().onError(R.string.some_error);

                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.some_error);
                        }

                    }
                }));

    }

}
