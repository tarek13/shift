package com.sattar.cars.shift.ui.login.view;

import com.sattar.cars.shift.ui.base.view.IBaseView;

public interface LoginView extends IBaseView {
    void openRegisterActivity();
    void openForgetPasswordActivity();
    void openMainActivity();
    void fillEmailAndPasswordFromPrefs(String email, String password);

    void onErrorEmail(int email_required_error);

    void onErrorPassword(int password_required_error);
}
