package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.SearchBodyRequest;
import com.sattar.cars.shift.di.component.SubscribesListComponent;
import com.sattar.cars.shift.di.component.DaggerSubscribesListComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.SubscribesListModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.SubscribesListAdapter;
import com.sattar.cars.shift.ui.main.interactor.ISubscribesListInteractor;
import com.sattar.cars.shift.ui.main.presenter.ISubscribesListPresenter;
import com.sattar.cars.shift.ui.main.view.SubscribesListView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscribesListFragment extends BaseFragment implements SubscribesListView, SubscribesListAdapter.Callback {
    public static final String TAG = "SubscribesListFragment";

    @BindView(R.id.subscribes_list_recycler_view)
    RecyclerView subscribesListRecyclerView;

    @BindView(R.id.subscribes_list_error)
    CustomTextView subscribesListError;

    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;

    @BindView(R.id.subscribes_list_continer)
    CardView subscribesListContiner;

    @BindView(R.id.subscribes_list_title_text_view)
    CustomTextView subscribesListTitleTextView;

    @Inject
    ISubscribesListPresenter<SubscribesListView, ISubscribesListInteractor> subscribesListPresenter;

    @Inject
    SubscribesListAdapter subscribesListAdapter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    SubscribesListComponent subscribesListComponent;

    int comeFrom;


    private SearchBodyRequest searchRequestBody;
    private DepartmentResponse.Data data;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        SubscribesListFragment fragment = new SubscribesListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setData(DepartmentResponse.Data data) {
        this.data=data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscribes_list, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        subscribesListComponent = DaggerSubscribesListComponent.builder()
                .subscribesListModule(new SubscribesListModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        subscribesListComponent.inject(this);

        subscribesListPresenter.onAttach(this);
     //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        ((MainActivity)getBaseActivity()).getSupportActionBar().setTitle(data.getName());

        subscribesListRecyclerView.setLayoutManager(linearLayoutManager);
        subscribesListRecyclerView.setAdapter(subscribesListAdapter);
        subscribesListAdapter.setCallback(this);
        subscribesListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        subscribesListPresenter.loadAllSubscribes(getString(R.string.language),data.getId(),subscribesListPresenter.getInteractor().getPreferencesHelper().getCurrentUser().getId());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        subscribesListPresenter.onDetach();
    }


    @Override
    public void showSubscribesList(ArrayList<SubscribesItemResponse> subscribesItemResponses) {
        if (subscribesItemResponses != null && subscribesItemResponses.size() > 0) {
            subscribesListRecyclerView.setVisibility(View.VISIBLE);
            subscribesListError.setVisibility(View.GONE);
            subscribesListAdapter.setData(subscribesItemResponses);
        } else {
            showErrorMessage(R.string.no_compaines_found, R.drawable.not_found);
        }


    }


    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            subscribesListContiner.setVisibility(View.GONE);

        }

    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            subscribesListContiner.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
        subscribesListContiner.setVisibility(View.GONE);
        subscribesListError.setVisibility(View.VISIBLE);
        subscribesListError.setText(messageID);
        subscribesListError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void onSubscribeItemClick(int position, SubscribesItemResponse subscribesItemResponse) {
       ((MainActivity)getBaseActivity()).showSubscribeDetailsFragment(data,subscribesItemResponse);
    }

    public void setSearchRequestBody(SearchBodyRequest searchRequestBody) {
        this.searchRequestBody = searchRequestBody;
    }

    public SearchBodyRequest getSearchRequestBody() {
        return searchRequestBody;
    }

    public DepartmentResponse.Data getData() {
        return data;
    }
}
