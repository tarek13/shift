package com.sattar.cars.shift.di.module;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.MainMenuNavigationAdapter;
import com.sattar.cars.shift.ui.main.interactor.IMainInteractor;
import com.sattar.cars.shift.ui.main.interactor.MainInteractor;
import com.sattar.cars.shift.ui.main.presenter.IMainPresenter;
import com.sattar.cars.shift.ui.main.presenter.MainPresenter;
import com.sattar.cars.shift.ui.main.view.MainView;
import com.sattar.cars.shift.utils.views.DividerItemDecoration;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class MainModule {

    @Provides
    @PerActivity
    IMainPresenter<MainView,IMainInteractor> provideMainPresenter (MainPresenter<MainView,IMainInteractor> mainPresenter){
        return mainPresenter;
    }


    @Provides
    @PerActivity
    IMainInteractor provideMainInteractor (MainInteractor mainInteractor){
        return mainInteractor;
    }
    @Provides
    @PerActivity
    MainMenuNavigationAdapter provideMainMenuNavigationAdapter(@ActivityContext Context context){
        return new MainMenuNavigationAdapter(context);
    }

    @Provides
    @PerActivity
    LinearLayoutManager provideMainLinearManagerMainMenuNavigation(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }

    @Provides
    @PerActivity
    DividerItemDecoration provideDividerItemDecoration(@ActivityContext Context context){
        return new DividerItemDecoration(context,DividerItemDecoration.VERTICAL_LIST);
    }

}
