package com.sattar.cars.shift.ui.register.presenter;

import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.register.interactor.IRegistrationInteractor;
import com.sattar.cars.shift.ui.register.view.RegistrationView;

@PerActivity
public interface IRegistrationPresenter<V extends RegistrationView,I extends IRegistrationInteractor> extends IBasePresenter<V,I> {

    void onRegisterButtonClick(RegisterRequestBody registerRequestBody);


    boolean validateUserData(String firstName, String email, String password, String confirmPassword, String mobile);
}
