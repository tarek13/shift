package com.sattar.cars.shift.di.module;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.CarItemAdapter;
import com.sattar.cars.shift.ui.main.adapter.CarSizeListAdapter;
import com.sattar.cars.shift.ui.main.adapter.ServicesListAdapter;
import com.sattar.cars.shift.ui.main.interactor.IServiceDetailsInteractor;
import com.sattar.cars.shift.ui.main.interactor.ServiceDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.IServiceDetailsPresenter;
import com.sattar.cars.shift.ui.main.presenter.ServiceDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.ServiceDetailsView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class ServiceDetailsModule {

    @Provides
    @PerActivity
    IServiceDetailsPresenter<ServiceDetailsView, IServiceDetailsInteractor> provideServiceDetailsPresenter(ServiceDetailsPresenter<ServiceDetailsView, IServiceDetailsInteractor> companieDetailsPresenter){
        return companieDetailsPresenter;
    }

    @Provides
    @PerActivity
    IServiceDetailsInteractor provideServiceDetailsInteractor (ServiceDetailsInteractor eventDetailsInteractor) {
        return  eventDetailsInteractor;
    }
    @Provides
    @PerActivity
    CarSizeListAdapter provideCarSizeListAdapter(@ActivityContext Context context){
        return new CarSizeListAdapter(context);
    }

    @Provides
    @PerActivity
    CarItemAdapter provideCarItemAdapter(@ActivityContext Context context){
        return new CarItemAdapter(context);
    }
}
