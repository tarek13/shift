package com.sattar.cars.shift.di.module;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.MyOrdersListAdapter;
import com.sattar.cars.shift.ui.main.interactor.UnCompletedOrdersInteractor;
import com.sattar.cars.shift.ui.main.interactor.IUnCompletedOrdersInteractor;
import com.sattar.cars.shift.ui.main.presenter.UnCompletedOrdersPresenter;
import com.sattar.cars.shift.ui.main.presenter.IUnCompletedOrdersPresenter;
import com.sattar.cars.shift.ui.main.view.UnCompletedOrdersView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class UnCompletedOrdersModule {

    @Provides
    @PerActivity
    IUnCompletedOrdersPresenter<UnCompletedOrdersView, IUnCompletedOrdersInteractor> provideUnCompletedOrdersPresenter(UnCompletedOrdersPresenter<UnCompletedOrdersView, IUnCompletedOrdersInteractor> companiesListPresenter){
        return companiesListPresenter;
    }

    @Provides
    @PerActivity
    IUnCompletedOrdersInteractor provideUnCompletedOrdersInteractor (UnCompletedOrdersInteractor eventsListInteractor) {
        return  eventsListInteractor;
    }

    @Provides
    @PerActivity
    MyOrdersListAdapter provideUnCompletedOrdersAdapter(@ActivityContext Context context){
        return new MyOrdersListAdapter(context);
    }

    @Provides
    @PerActivity
    LinearLayoutManager provideLinearManagerUnCompletedOrders(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }
}
