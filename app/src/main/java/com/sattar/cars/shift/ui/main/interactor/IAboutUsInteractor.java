package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.AboutUsResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import io.reactivex.Observable;

public interface IAboutUsInteractor extends IBaseInteractor {
    Observable<AboutUsResponse> getAboutUs(String lang,String user_id);


}
