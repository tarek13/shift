package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyOrdersResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("pending")
        @Expose
        private ArrayList<MyOrderItemResponse> pending = null;
        @SerializedName("finshied")
        @Expose
        private ArrayList<MyOrderItemResponse> finshied = null;
        @SerializedName("unfinshied")
        @Expose
        private ArrayList<MyOrderItemResponse> unfinshied = null;

        public ArrayList<MyOrderItemResponse> getPending() {
            return pending;
        }

        public void setPending(ArrayList<MyOrderItemResponse> pending) {
            this.pending = pending;
        }

        public ArrayList<MyOrderItemResponse> getFinshied() {
            return finshied;
        }

        public void setFinshied(ArrayList<MyOrderItemResponse> finshied) {
            this.finshied = finshied;
        }

        public ArrayList<MyOrderItemResponse> getUnfinshied() {
            return unfinshied;
        }

        public void setUnfinshied(ArrayList<MyOrderItemResponse> unfinshied) {
            this.unfinshied = unfinshied;
        }

    }
}
