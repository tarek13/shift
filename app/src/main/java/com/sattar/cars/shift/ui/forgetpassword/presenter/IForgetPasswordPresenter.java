package com.sattar.cars.shift.ui.forgetpassword.presenter;


import com.sattar.cars.shift.data.network.model.ForgetPasswordRequestBody;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.forgetpassword.interactor.IForgetPasswordInteractor;
import com.sattar.cars.shift.ui.forgetpassword.view.ForgetPasswordView;

@PerActivity
public interface  IForgetPasswordPresenter<V extends ForgetPasswordView, I
        extends IForgetPasswordInteractor> extends IBasePresenter<V,I> {


    void onSendNowButtonClick(ForgetPasswordRequestBody forgetPasswordRequestBody);

    boolean validateUserData(String email);
}
