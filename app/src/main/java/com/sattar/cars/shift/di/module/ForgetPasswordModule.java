package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.forgetpassword.interactor.ForgetPasswordInteractor;
import com.sattar.cars.shift.ui.forgetpassword.interactor.IForgetPasswordInteractor;
import com.sattar.cars.shift.ui.forgetpassword.presenter.ForgetPasswordPresenter;
import com.sattar.cars.shift.ui.forgetpassword.presenter.IForgetPasswordPresenter;
import com.sattar.cars.shift.ui.forgetpassword.view.ForgetPasswordView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class ForgetPasswordModule {

    @Provides
    @PerActivity
    IForgetPasswordPresenter<ForgetPasswordView,IForgetPasswordInteractor> provideForgetPasswordPresenter(ForgetPasswordPresenter<ForgetPasswordView,IForgetPasswordInteractor> forgetPasswordPresenter){
        return forgetPasswordPresenter;
    }

    @Provides
    @PerActivity
    IForgetPasswordInteractor provideForgetPasswordInteractor(ForgetPasswordInteractor forgetPasswordInteractor){
        return forgetPasswordInteractor;
    }
}
