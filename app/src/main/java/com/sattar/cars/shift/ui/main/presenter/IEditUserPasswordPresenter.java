package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IEditUserPasswordInteractor;
import com.sattar.cars.shift.ui.main.view.EditUserPasswordView;


@PerActivity
public interface IEditUserPasswordPresenter<V extends EditUserPasswordView,I extends IEditUserPasswordInteractor> extends IBasePresenter<V,I> {

    void onSaveButtonClick(RegisterRequestBody registerRequestBody);


    boolean validateUserData(String oldPassword, String password, String confirmPassword);
}
