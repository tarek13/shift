package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginErrorResponse {
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("errors")
    @Expose
    private LoginErrors errors;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public LoginErrors getErrors() {
        return errors;
    }

    public void setErrors(LoginErrors errors) {
        this.errors = errors;
    }

}
