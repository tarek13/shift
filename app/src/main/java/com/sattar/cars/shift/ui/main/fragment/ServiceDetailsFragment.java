package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.CarDetailsRequestBody;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.MakeOrderRequestBody;
import com.sattar.cars.shift.data.network.model.SearchBodyRequest;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.data.network.model.ServicesItemResponse;
import com.sattar.cars.shift.di.component.DaggerServiceDetailsComponent;
import com.sattar.cars.shift.di.component.ServiceDetailsComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.ServiceDetailsModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.CarItemAdapter;
import com.sattar.cars.shift.ui.main.adapter.CarSizeListAdapter;
import com.sattar.cars.shift.ui.main.interactor.IServiceDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.IServiceDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.ServiceDetailsView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceDetailsFragment extends BaseFragment implements ServiceDetailsView, CarSizeListAdapter.Callback, CarItemAdapter.Callback {
    public static final String TAG = "ServiceDetailsFragment";

    @BindView(R.id.service_details_logo_image_view)
    ImageView serviceDetailsLogoImageView;
    @BindView(R.id.service_details_inside_button)
    CustomTextView serviceDetailsInsideButton;
    @BindView(R.id.service_details_outside_button)
    CustomTextView serviceDetailsOutsideButton;
    @BindView(R.id.service_details_inside_outside_button)
    CustomTextView serviceDetailsInsideOutsideButton;
    @BindView(R.id.service_details_car_size_recycler_view)
    RecyclerView serviceDetailsCarSizeRecyclerView;
    @BindView(R.id.service_details_add_other_car_button)
    RelativeLayout serviceDetailsAddOtherCarButton;
    @BindView(R.id.service_details_date_edit_text)
    EditText serviceDetailsDateEditText;
    @BindView(R.id.service_details_time_edit_text)
    EditText serviceDetailsTimeEditText;
    @BindView(R.id.service_details_one_time_button)
    LinearLayout serviceDetailsOneTimeButton;
    @BindView(R.id.service_details_3_month_button)
    LinearLayout serviceDetails3MonthButton;
    @BindView(R.id.service_details_6_month_button)
    LinearLayout serviceDetails6MonthButton;
    @BindView(R.id.service_details_12_month_button)
    LinearLayout serviceDetails12MonthButton;
    @BindView(R.id.service_details_final_price_text_view)
    CustomTextView serviceDetailsFinalPriceTextView;
    @BindView(R.id.service_details_taxes_text_view)
    CustomTextView serviceDetailsTaxesTextView;
    @BindView(R.id.service_details_car_recycler_view)
    RecyclerView serviceDetailsCarRecyclerView;

    @Inject
    IServiceDetailsPresenter<ServiceDetailsView, IServiceDetailsInteractor> serviceDetailsPresenter;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    ServiceDetailsComponent serviceDetailsComponent;

    int comeFrom;

    @Inject
    CarSizeListAdapter carSizeListAdapter;
    @Inject
    CarItemAdapter carItemAdapter;


    private SearchBodyRequest searchRequestBody;
    private DepartmentResponse.Data data;
    private ServicesItemResponse servicesItemResponse;
    private String dateReserve;
    private String selectedDateReserve;
    private int selectedTimeIndex = -1;
    private String selectedTime;
    private ServiceDetailsResponse.Data serviceDetailsResponse;
    private String companyId;
    private String serviceId;
    private String selectedWashType;
    private String selectSizeId;
    private String selectedTimePackage;
    private String selectedPackageId;
    private String selectedCarSize;
    private ArrayList<CarDetailsRequestBody> carPriceRequestArrayList = new ArrayList<>();
    private String selectedPrice;
    private int finalPrice;
    private int selectedDiscount;
    private int selectedCarWash;
    private String departmentId;
    private String companyName;
    private String selectedDuration;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        ServiceDetailsFragment fragment = new ServiceDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_details, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        serviceDetailsComponent = DaggerServiceDetailsComponent.builder()
                .serviceDetailsModule(new ServiceDetailsModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        serviceDetailsComponent.inject(this);

        serviceDetailsPresenter.onAttach(this);
        //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {

        serviceDetailsPresenter.getServiceDetails(getString(R.string.language), serviceId, companyId);
        serviceDetailsCarSizeRecyclerView.setLayoutManager(new GridLayoutManager(getBaseActivity(), 2));
        serviceDetailsCarSizeRecyclerView.setAdapter(carSizeListAdapter);
        carSizeListAdapter.setCallback(this);
        serviceDetailsCarSizeRecyclerView.setItemAnimator(new DefaultItemAnimator());

        serviceDetailsCarRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        serviceDetailsCarRecyclerView.setAdapter(carItemAdapter);
        carItemAdapter.setCallback(this);
        serviceDetailsCarRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        serviceDetailsPresenter.onDetach();

    }


    private void showCustomDatePicker() {

        DatePickerDialog datepickerdialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year1, int monthOfYear, int dayOfMonth) {
                        DatePickerDialog datePicker = view;
                        int day = dayOfMonth;
                        int month = monthOfYear + 1;
                        int year = year1;

                        String dayRequest = "";
                        String monthRequest = "";
                        String yearRequest = "";


                        String date = "";
                        if (day < 10) {
                            DecimalFormat decimalFormat = new DecimalFormat("00");
                            dayRequest = decimalFormat.format(day);
                            date = date + decimalFormat.format(day) + "/";


                        } else {
                            dayRequest = day + "";
                            date = date + day + "/";
                        }
                        if (month < 10) {
                            DecimalFormat decimalFormat = new DecimalFormat("00");
                            monthRequest = decimalFormat.format(month);

                            date = date + decimalFormat.format(month) + "/";
                        } else {
                            monthRequest = month + "";
                            date = date + month + "/";
                        }
                        yearRequest = year + "";
                        dateReserve = date + year;
                        serviceDetailsDateEditText.setText(dateReserve);

                        selectedDateReserve = yearRequest + "-" + monthRequest + "-" + dayRequest;

                    }
                }
        );
        datepickerdialog.setAccentColor(getResources().getColor(R.color.colorAccent)); // custom accent color
        datepickerdialog.setTitle(getString(R.string.select_birth_date)); //dialog title
        Date dateFromDate = null;
        if (selectedDateReserve != null) {
            try {
                dateFromDate = new SimpleDateFormat("yyyy-MM-dd").parse(selectedDateReserve);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        if (dateFromDate != null) {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTimeInMillis(dateFromDate.getTime());
        }

        datepickerdialog.show(getBaseActivity().getFragmentManager(), "Datepickerdialog"); //show dialog
    }

    @Override
    public void showAvaliableTime(ArrayList<String> data) {

        new MaterialDialog.Builder(getBaseActivity())
                .title(R.string.select_time)
                .items(data)
                .itemsCallbackSingleChoice(selectedTimeIndex, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        selectedTimeIndex = dialog.getSelectedIndex();
                        if (selectedTimeIndex != -1) {
                            selectedTime = text.toString();
                            serviceDetailsTimeEditText.setText(selectedTime);
                        }
                        return true;
                    }
                })
                .positiveText(R.string.select).negativeText(R.string.cancel)
                .show();
    }

    @Override
    public void showServiceDetails(ServiceDetailsResponse.Data data) {
        this.serviceDetailsResponse = data;
        getBaseActivity().getSupportActionBar().setTitle(data.getService().getName());
        if (serviceDetailsResponse.getService().getImage() != null) {
            //   GlideApp.with(getBaseActivity()).load("http://ur-business.net/shift_news/"+serviceDetailsResponse.getService().getImage()).error(R.drawable.car_wash_1).into(serviceDetailsLogoImageView);
        }
        serviceDetailsInsideOutsideButton.setClickable(false);
        serviceDetailsInsideOutsideButton.setEnabled(false);
        serviceDetailsInsideOutsideButton.setAlpha(.4f);
        serviceDetailsOutsideButton.setClickable(false);
        serviceDetailsOutsideButton.setEnabled(false);
        serviceDetailsOutsideButton.setAlpha(.4f);
        serviceDetailsInsideButton.setClickable(false);
        serviceDetailsInsideButton.setEnabled(false);
        serviceDetailsInsideButton.setAlpha(.4f);
        for (ServiceDetailsResponse.Package package1 : serviceDetailsResponse.getService().getPackage()) {
            if ((package1.getName().equals("from inside and outside") || package1.getName().equals(" من الداخل و الخارج") || package1.getName().equals("داخلي وخارجي")) && !package1.getSize().isEmpty()) {
                serviceDetailsInsideOutsideButton.setClickable(true);
                serviceDetailsInsideOutsideButton.setEnabled(true);
                serviceDetailsInsideOutsideButton.setAlpha(1);
            }
            if ((package1.getName().equals("outside") || package1.getName().equals("خارجي")) && !package1.getSize().isEmpty()) {
                serviceDetailsOutsideButton.setClickable(true);
                serviceDetailsOutsideButton.setEnabled(true);
                serviceDetailsOutsideButton.setAlpha(1);
            }
            if ((package1.getName().equals("inside") || package1.getName().equals("داخلي")) && !package1.getSize().isEmpty()) {
                serviceDetailsInsideButton.setClickable(true);
                serviceDetailsInsideButton.setEnabled(true);
                serviceDetailsInsideButton.setAlpha(1);
            }


        }

    }

    @OnClick({R.id.service_details_inside_button, R.id.service_details_outside_button, R.id.service_details_inside_outside_button, R.id.service_details_add_other_car_button, R.id.service_details_date_edit_text, R.id.service_details_time_edit_text, R.id.service_details_one_time_button, R.id.service_details_3_month_button, R.id.service_details_6_month_button, R.id.service_details_12_month_button, R.id.service_detail_save_button_container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.service_details_inside_button:
                if (serviceDetailsInsideButton.isEnabled()) {
                    for (ServiceDetailsResponse.Package package1 : serviceDetailsResponse.getService().getPackage()) {
                        if ((package1.getName().equals("inside") || package1.getName().equals("داخلي")) && !package1.getSize().isEmpty()) {
                            serviceDetailsInsideButton.setBackgroundColor(getResources().getColor(R.color.yellow_gold_color));
                            // serviceDetailsInsideButton.setTextColor(getResources().getColor(R.color.black));
                            carSizeListAdapter.setData(package1.getSize());
                            if (serviceDetailsOutsideButton.isEnabled()) {
                                serviceDetailsOutsideButton.setBackgroundColor(getResources().getColor(R.color.white));
                                // serviceDetailsOutsideButton.setTextColor(getResources().getColor(R.color.color_gray));
                            }
                            if (serviceDetailsInsideOutsideButton.isEnabled()) {
                                serviceDetailsInsideOutsideButton.setBackgroundColor(getResources().getColor(R.color.white));
                                //serviceDetailsInsideOutsideButton.setTextColor(getResources().getColor(R.color.color_gray));
                            }
                            selectedWashType = package1.getName();
                            selectedPackageId = package1.getId();
                            break;
                        }
                    }
                }

                break;
            case R.id.service_details_outside_button:
                if (serviceDetailsOutsideButton.isEnabled()) {
                    for (ServiceDetailsResponse.Package package1 : serviceDetailsResponse.getService().getPackage()) {
                        if ((package1.getName().equals("outside") || package1.getName().equals("خارجي")) && !package1.getSize().isEmpty()) {
                            serviceDetailsOutsideButton.setBackgroundColor(getResources().getColor(R.color.yellow_gold_color));
                            //    serviceDetailsOutsideButton.setTextColor(getResources().getColor(R.color.black));
                            carSizeListAdapter.setData(package1.getSize());
                            if (serviceDetailsInsideButton.isEnabled()) {
                                serviceDetailsInsideButton.setBackgroundColor(getResources().getColor(R.color.white));
                                //    serviceDetailsInsideButton.setTextColor(getResources().getColor(R.color.color_gray));
                            }
                            if (serviceDetailsInsideOutsideButton.isEnabled()) {
                                serviceDetailsInsideOutsideButton.setBackgroundColor(getResources().getColor(R.color.white));
                                //    serviceDetailsInsideOutsideButton.setTextColor(getResources().getColor(R.color.color_gray));
                            }
                            selectedWashType = package1.getName();
                            selectedPackageId = package1.getId();
                            break;
                        }
                    }
                }
                break;
            case R.id.service_details_inside_outside_button:
                if (serviceDetailsInsideOutsideButton.isEnabled()) {
                    for (ServiceDetailsResponse.Package package1 : serviceDetailsResponse.getService().getPackage()) {
                        if ((package1.getName().equals("from inside and outside") || package1.getName().equals(" من الداخل و الخارج") || package1.getName().equals("داخلي وخارجي")) && !package1.getSize().isEmpty()) {
                            serviceDetailsInsideOutsideButton.setBackgroundColor(getResources().getColor(R.color.yellow_gold_color));
                            serviceDetailsInsideOutsideButton.setTextColor(getResources().getColor(R.color.black));
                            carSizeListAdapter.setData(package1.getSize());
                            if (serviceDetailsInsideButton.isEnabled()) {
                                serviceDetailsInsideButton.setBackgroundColor(getResources().getColor(R.color.white));
                                //  serviceDetailsInsideButton.setTextColor(getResources().getColor(R.color.color_gray));
                            }
                            if (serviceDetailsOutsideButton.isEnabled()) {
                                serviceDetailsOutsideButton.setBackgroundColor(getResources().getColor(R.color.white));
                                // serviceDetailsOutsideButton.setTextColor(getResources().getColor(R.color.color_gray));
                            }
                            selectedWashType = package1.getName();
                            selectedPackageId = package1.getId();
                            break;
                        }
                    }
                }
                break;
            case R.id.service_details_add_other_car_button:
                if (!carSizeListAdapter.getSelectedItems().isEmpty()) {
                    selectedCarSize = carSizeListAdapter.getSelectedItems().get(0).getName();
                    selectedPrice = carSizeListAdapter.getSelectedItems().get(0).getPrice();
                    selectSizeId = carSizeListAdapter.getSelectedItems().get(0).getId();
                    selectedTimePackage = carSizeListAdapter.getSelectedItems().get(0).getTime();
                } else {
                    selectedCarSize = null;
                    selectedPrice = null;
                    selectSizeId = null;
                    selectedTimePackage = null;
                }
                if (selectedCarSize != null && !selectedCarSize.isEmpty()
                        && selectedWashType != null && !selectedWashType.isEmpty()
                ) {
                    CarDetailsRequestBody carPriceRequest = new CarDetailsRequestBody();
                    carPriceRequest.setSizeName(selectedCarSize);
                    carPriceRequest.setPackageName(selectedWashType);
                    carPriceRequest.setPrice(selectedPrice);
                    carPriceRequest.setSizeId(selectSizeId);
                    carPriceRequest.setPackageId(selectedPackageId);
                    carPriceRequest.setTime(selectedTimePackage);
                    carPriceRequestArrayList.add(carPriceRequest);
                    carItemAdapter.setData(carPriceRequestArrayList);
                    calculateFinalPrice();
                } else {
                    if (selectedWashType == null || selectedWashType.isEmpty()) {
                        onError(R.string.wash_type_error);
                    } else if (selectedCarSize == null || selectedCarSize.isEmpty()) {
                        onError(R.string.car_size_error);
                    }
                }
                break;
            case R.id.service_details_date_edit_text:
                showCustomDatePicker();
                break;
            case R.id.service_details_time_edit_text:
                if (selectedDateReserve != null && !selectedDateReserve.isEmpty()) {
                    serviceDetailsPresenter.getAvaliableTime(getString(R.string.language), selectedDateReserve);
                } else {
                    onError(R.string.time_error);
                }
                break;
            case R.id.service_details_one_time_button:
                if (selectedWashType == null || selectedWashType.isEmpty()) {
                    onError(R.string.wash_type_error);
                } else if (selectedCarSize == null || selectedCarSize.isEmpty()) {
                    onError(R.string.car_size_error);
                } else {
                    serviceDetailsOneTimeButton.setBackgroundColor(getResources().getColor(R.color.yellow_gold_color));
                    // serviceDetailsOneTimeButton.setTextColor(getResources().getColor(R.color.black));

                    serviceDetails3MonthButton.setBackgroundColor(getResources().getColor(R.color.white));
                    //  serviceDetails3MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails6MonthButton.setBackgroundColor(getResources().getColor(R.color.white));
                    // serviceDetails6MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails12MonthButton.setBackgroundColor(getResources().getColor(R.color.white));
                    // serviceDetails12MonthButton.setTextColor(getResources().getColor(R.color.color_gray));
                    selectedDuration = "1";
                    selectedCarWash = 1;
                    selectedDiscount = 0;
                    try {
                        selectedDiscount = Integer.valueOf(serviceDetailsResponse.getService().getDiscount().getDiscount());
                        calculateFinalPrice();
                    } catch (Exception e) {
                        e.printStackTrace();
                        calculateFinalPrice();
                    }
                }

                break;
            case R.id.service_details_3_month_button:
                if (selectedWashType == null || selectedWashType.isEmpty()) {
                    onError(R.string.wash_type_error);
                } else if (selectedCarSize == null || selectedCarSize.isEmpty()) {
                    onError(R.string.car_size_error);
                } else {
                    serviceDetails3MonthButton.setBackgroundColor(getResources().getColor(R.color.yellow_gold_color));
                    // serviceDetailsOneTimeButton.setTextColor(getResources().getColor(R.color.black));

                    serviceDetailsOneTimeButton.setBackgroundColor(getResources().getColor(R.color.white));
                    //  serviceDetails3MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails6MonthButton.setBackgroundColor(getResources().getColor(R.color.white));
                    // serviceDetails6MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails12MonthButton.setBackgroundColor(getResources().getColor(R.color.white));

                    selectedDuration = "3";
                    selectedDiscount = 0;
                    selectedCarWash = 1;
                    try {
                        selectedDiscount = Integer.valueOf(serviceDetailsResponse.getService().getDiscount().getDiscount3());
                        selectedCarWash = Integer.valueOf(serviceDetailsResponse.getService().getDiscount().getCarWash3());
                        calculateFinalPrice();
                    } catch (Exception e) {
                        e.printStackTrace();
                        calculateFinalPrice();
                    }

                }

                break;
            case R.id.service_details_6_month_button:
                if (selectedWashType == null || selectedWashType.isEmpty()) {
                    onError(R.string.wash_type_error);
                } else if (selectedCarSize == null || selectedCarSize.isEmpty()) {
                    onError(R.string.car_size_error);
                } else {
                    serviceDetails6MonthButton.setBackgroundColor(getResources().getColor(R.color.yellow_gold_color));
                    // serviceDetailsOneTimeButton.setTextColor(getResources().getColor(R.color.black));

                    serviceDetailsOneTimeButton.setBackgroundColor(getResources().getColor(R.color.white));
                    //  serviceDetails3MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails3MonthButton.setBackgroundColor(getResources().getColor(R.color.white));
                    // serviceDetails6MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails12MonthButton.setBackgroundColor(getResources().getColor(R.color.white));
                    selectedDuration = "6";
                    selectedDiscount=0;
                    selectedCarWash=1;
                    try {
                    selectedDiscount = Integer.valueOf(serviceDetailsResponse.getService().getDiscount().getDiscount6());
                    selectedCarWash = Integer.valueOf(serviceDetailsResponse.getService().getDiscount().getCarWash6());
                    calculateFinalPrice();
                    }catch (Exception e){

                    }
                }
                break;
            case R.id.service_details_12_month_button:
                if (selectedWashType == null || selectedWashType.isEmpty()) {
                    onError(R.string.wash_type_error);
                } else if (selectedCarSize == null || selectedCarSize.isEmpty()) {
                    onError(R.string.car_size_error);
                } else {
                    serviceDetails12MonthButton.setBackgroundColor(getResources().getColor(R.color.yellow_gold_color));
                    // serviceDetailsOneTimeButton.setTextColor(getResources().getColor(R.color.black));

                    serviceDetailsOneTimeButton.setBackgroundColor(getResources().getColor(R.color.white));
                    //  serviceDetails3MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails3MonthButton.setBackgroundColor(getResources().getColor(R.color.white));
                    // serviceDetails6MonthButton.setTextColor(getResources().getColor(R.color.color_gray));

                    serviceDetails6MonthButton.setBackgroundColor(getResources().getColor(R.color.white));

                    selectedDuration = "12";
                    selectedCarWash=1;
                    selectedDiscount=0;
                    try {
                        selectedDiscount = Integer.valueOf(serviceDetailsResponse.getService().getDiscount().getDiscount12());
                        selectedCarWash = Integer.valueOf(serviceDetailsResponse.getService().getDiscount().getCarWash12());
                        calculateFinalPrice();
                    }catch (Exception e){
                        e.printStackTrace();
                        calculateFinalPrice();
                    }

                }
                break;
            case R.id.service_detail_save_button_container:
                if (serviceDetailsPresenter.validateUserData(selectedWashType, selectedCarSize, selectedDateReserve, selectedTime, selectedDuration)) {
                    calculateFinalPrice();
                    MakeOrderRequestBody makeOrderRequestBody = new MakeOrderRequestBody();
                    makeOrderRequestBody.setDate(selectedDateReserve);
                    makeOrderRequestBody.setTime(selectedTime);
                    makeOrderRequestBody.setFinalPrice(String.valueOf(finalPrice));
                    makeOrderRequestBody.setDepartmentId(departmentId);
                    makeOrderRequestBody.setCompanyName(companyName);
                    makeOrderRequestBody.setDuration(selectedDuration);
                    if (carPriceRequestArrayList != null && carPriceRequestArrayList.isEmpty()) {
                        makeOrderRequestBody.setCarDetailsRequestBodyArrayList(carPriceRequestArrayList);
                    } else {
                        CarDetailsRequestBody carPriceRequest = new CarDetailsRequestBody();
                        carPriceRequest.setSizeName(selectedCarSize);
                        carPriceRequest.setPackageName(selectedWashType);
                        carPriceRequest.setPrice(selectedPrice);
                        carPriceRequest.setSizeId(selectSizeId);
                        carPriceRequest.setPackageId(selectedPackageId);
                        carPriceRequest.setTime(selectedTimePackage);
                        carPriceRequestArrayList.add(carPriceRequest);
                        carItemAdapter.setData(carPriceRequestArrayList);
                        makeOrderRequestBody.setCarDetailsRequestBodyArrayList(carPriceRequestArrayList);

                    }
                    ((MainActivity) getBaseActivity()).showMakeOrderDetailsFragment(serviceDetailsResponse, makeOrderRequestBody);
                }
                break;
        }
    }

    public void setComeFrom(String serviceId, String companyId, String departmentId, String companyName) {
        this.serviceId = serviceId;
        this.companyId = companyId;
        this.departmentId = departmentId;
        this.companyName = companyName;
    }

    @Override
    public void onCarSizeItemClick(int position, ServiceDetailsResponse.Size carSizesItemResponse) {
        for (ServiceDetailsResponse.Size storePriceOptionsResponse : carSizeListAdapter.getCarSizesItemResponseArrayList()) {
            if (!storePriceOptionsResponse.equals(carSizesItemResponse)
                    && storePriceOptionsResponse.isSelected()) {
                storePriceOptionsResponse.setSelected(false);
            } else if (storePriceOptionsResponse.equals(carSizesItemResponse)
                    && carSizesItemResponse.isSelected()) {
                storePriceOptionsResponse.setSelected(true);
            }
        }
        /*if(selectedPriceOptionsResponse!=null && priceOptionsResponse.isSelected() && !priceOptionsResponse.getId().equals(selectedPriceOptionsResponse)) {
            priceOptionsListAdapter.setSelectedPriceOptionsResponse(null);
        }*/

        //carSizeListAdapter.setSelectedItem(null);
        if (carSizeListAdapter.getSelectedItems() != null && !carSizeListAdapter.getSelectedItems().isEmpty()) {
            selectedCarSize = carSizeListAdapter.getSelectedItems().get(0).getName();
            selectedPrice = carSizeListAdapter.getSelectedItems().get(0).getPrice();
            selectSizeId = carSizeListAdapter.getSelectedItems().get(0).getId();
            selectedTimePackage = carSizeListAdapter.getSelectedItems().get(0).getTime();
            calculateFinalPrice();
        }
        carSizeListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDeleteClick(int position, CarDetailsRequestBody carPriceRequest) {
        carPriceRequestArrayList.remove(position);
        carItemAdapter.notifyDataSetChanged();
        calculateFinalPrice();
    }

    private void calculateFinalPrice() {
        if (carPriceRequestArrayList != null && !carPriceRequestArrayList.isEmpty()) {
            int price = 0;
            for (CarDetailsRequestBody carPriceRequest : carPriceRequestArrayList) {
                price = price + Integer.valueOf(carPriceRequest.getPrice());
            }
            int discountAmout = (price * selectedDiscount) / 100;
            price = price - discountAmout;
            if (selectedCarWash > 0) {
                price = price * (selectedCarWash);
            }
            finalPrice = price;
            serviceDetailsFinalPriceTextView.setText(String.valueOf(price));
        } else {

            int price = Integer.parseInt(selectedPrice);
            int discountAmout = (price * selectedDiscount) / 100;
            price = price - discountAmout;
            if (selectedCarWash > 0) {
                price = price * (selectedCarWash);
            }
            finalPrice = price;
            serviceDetailsFinalPriceTextView.setText(String.valueOf(price));

        }
    }
}
