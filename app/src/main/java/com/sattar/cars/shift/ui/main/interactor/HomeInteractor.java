package com.sattar.cars.shift.ui.main.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.network.OrdersApi;
import com.sattar.cars.shift.data.network.model.BannerResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class HomeInteractor extends BaseInteractor implements IHomeInteractor {

    private Context context;
    private OrdersApi ordersApi;
    private Retrofit retrofit;

    @Inject
    public HomeInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper, OrdersApi ordersApi, Retrofit retrofit) {
        super(preferencesHelper);
        this.context=context;
        this.ordersApi = ordersApi;
        this.retrofit=retrofit;
    }


    @Override
    public Observable<DepartmentResponse> getDepartmentsData(Map<String, String> lang) {
        return ordersApi.getDepartmentsData(lang);
    }

    @Override
    public Observable<BannerResponse> getBannersData(HashMap<String, String> options) {
        return ordersApi.getBannersData(options);
    }
}
