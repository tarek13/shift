package com.sattar.cars.shift.di.component;

import com.sattar.cars.shift.di.module.LoginModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.login.LoginActivity;


import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {LoginModule.class})

public interface LoginComponent {

    void inject(LoginActivity loginActivity);

}
