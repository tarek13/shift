package com.sattar.cars.shift.ui.forgetpassword.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.network.UserApis;
import com.sattar.cars.shift.data.network.model.ForgetPasswordResponse;
import com.sattar.cars.shift.data.network.model.LoginErrorResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgetPasswordInteractor extends BaseInteractor implements IForgetPasswordInteractor {

    private Context mContext;
    private UserApis userApis;
    private Retrofit retrofit;

    @Inject
    public ForgetPasswordInteractor(@ApplicationContext Context context,PreferencesHelper preferencesHelper,UserApis userApis,Retrofit retrofit) {
        super(preferencesHelper);
        mContext=context;
        this.userApis=userApis;
        this.retrofit=retrofit;
    }
    @Override
    public Observable<ForgetPasswordResponse> sendForgetPassword(Map<String,String> forgetPasswordRequestBody){
        return userApis.forgetPassword(forgetPasswordRequestBody);
    }

    @Override
    public LoginErrorResponse handleErrorResponse(Response<?> response) {
        Converter<ResponseBody, LoginErrorResponse> converter =
                retrofit.responseBodyConverter(LoginErrorResponse.class, new Annotation[0]);

        LoginErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new LoginErrorResponse();
        }

        return error;
    }
}
