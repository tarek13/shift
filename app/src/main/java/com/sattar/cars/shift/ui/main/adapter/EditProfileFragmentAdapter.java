package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ui.main.fragment.EditUserInfoFragment;
import com.sattar.cars.shift.ui.main.fragment.EditUserPasswordFragment;


/**
 * Created by tarek on 25/09/16.
 */

public class EditProfileFragmentAdapter extends FragmentPagerAdapter {


    private static final String[] tabsTitles = new String[]{"Scan code", "Vouchers history"};
    private static final int NUMBER_OF_TABS = 2;
    private static final int EDIT_USER_INFO_TAB = 0;
    private static final int EDIT_USER_PASSWORD_TAB = 1;
    private Fragment fragment;
    private Context context;


    public EditProfileFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context=context;
    }


  

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case EDIT_USER_INFO_TAB:
                fragment= new EditUserInfoFragment();
                break;
            case EDIT_USER_PASSWORD_TAB:
                fragment = new EditUserPasswordFragment();
                break;
            default:
                break;
        }

        return fragment;
    }



    @Override
    public int getCount() {
        return NUMBER_OF_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        // return null to display only the icon
        switch (position) {
            case 0:
                return context.getString(R.string.user_info);
            case 1:
                return context.getString(R.string.user_password);
            default:
                return null;
        }

    }

    /*public UserDetailsFragment getFragment() {
        return fragment;
    }
    public String getFragmentTag(int viewPagerId, int fragmentPosition)
    {
        return "android:switcher:" + viewPagerId + ":" + fragmentPosition;
    }*/
}