package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.CompaniesListModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.CompaniesListFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {CompaniesListModule.class})
public interface CompaniesListComponent {

    void inject(CompaniesListFragment companiesListFragment);
}
