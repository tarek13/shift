package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IRateInteractor;
import com.sattar.cars.shift.ui.main.view.RateView;

@PerActivity
public interface IRatePresenter<V extends RateView,I extends IRateInteractor> extends IBasePresenter<V,I> {

    void rateOrder(String lang, String rate, String companyId, String comment, String clintId);

}

