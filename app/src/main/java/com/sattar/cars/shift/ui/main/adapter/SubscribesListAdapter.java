package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.ui.main.holder.SubscribeItemViewHolder;

import java.util.ArrayList;

import javax.inject.Inject;

public class SubscribesListAdapter extends RecyclerView.Adapter<SubscribeItemViewHolder> {


    // View Types
    private ArrayList<SubscribesItemResponse> subscribesItemResponseArrayList = new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public SubscribesListAdapter(Context context) {
        this.context = context;
    }

    public interface Callback {
        void onSubscribeItemClick(int position, SubscribesItemResponse subscribesItemResponse);
    }

    public void setData(ArrayList<SubscribesItemResponse> subscribesItemResponses) {
        this.subscribesItemResponseArrayList = subscribesItemResponses;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public SubscribeItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubscribeItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_subscribe_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscribeItemViewHolder holder, int position) {
        holder.setSubscribe(subscribesItemResponseArrayList.get(position), position);
    }


    @Override
    public int getItemCount() {
        return subscribesItemResponseArrayList.size();
    }
}
