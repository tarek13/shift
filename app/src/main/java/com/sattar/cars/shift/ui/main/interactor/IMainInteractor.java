package com.sattar.cars.shift.ui.main.interactor;

import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.ArrayList;

public interface IMainInteractor extends IBaseInteractor {



    ArrayList<String> getHeaderTitle();


}
