package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountriesResponse {

    @SerializedName("items")
    @Expose
    private ArrayList<Item> items = null;

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
    public class Item {

        @SerializedName("demonym")
        @Expose
        private String demonym;

        public String getDemonym() {
            return demonym;
        }

        public void setDemonym(String demonym) {
            this.demonym = demonym;
        }
    }
}
