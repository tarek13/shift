package com.sattar.cars.shift.di.component;

import com.sattar.cars.shift.di.module.EditUserInfoModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.EditUserInfoFragment;
;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = {EditUserInfoModule.class})
public interface EditUserInfoComponent {
    void inject(EditUserInfoFragment editUserInfoFragment);
}
