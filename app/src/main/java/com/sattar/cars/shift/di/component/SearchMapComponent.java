package com.sattar.cars.shift.di.component;

import com.sattar.cars.shift.di.module.SearchMapModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.SearchMapFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {SearchMapModule.class})
public interface SearchMapComponent {

    void inject(SearchMapFragment searchMapFragment);
}
