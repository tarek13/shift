package com.sattar.cars.shift.di.component;


import com.sattar.cars.shift.di.module.SplashModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.splash.SplashActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {SplashModule.class})
public interface SplashComponent {

    void inject(SplashActivity activity);
}
