package com.sattar.cars.shift.ui.main.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.network.OrdersApi;
import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class SubscribeDetailsInteractor extends BaseInteractor implements ISubscribeDetailsInteractor {

    private Context context;
    private OrdersApi ordersApi;
    private Retrofit retrofit;

    @Inject
    public SubscribeDetailsInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper, OrdersApi ordersApi, Retrofit retrofit) {
        super(preferencesHelper);
        this.context=context;
        this.ordersApi = ordersApi;
        this.retrofit=retrofit;
    }

    @Override
    public Observable<AvaliableTimeResponse> getAvaliableTime(HashMap<String, String> options) {
        return ordersApi.getAvaliableTime(options);
    }

    @Override
    public Observable<GeneralResponse> makerSubscribeOrder(HashMap<String, String> options) {
        return ordersApi.makerSubscribeOrder(options);
    }
}
