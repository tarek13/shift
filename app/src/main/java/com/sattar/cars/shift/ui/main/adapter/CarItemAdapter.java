package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CarDetailsRequestBody;
import com.sattar.cars.shift.ui.main.holder.CarItemViewHolder;

import java.util.ArrayList;

import javax.inject.Inject;

public class CarItemAdapter extends RecyclerView.Adapter<CarItemViewHolder> {



    // View Types
    private ArrayList<CarDetailsRequestBody> carPriceRequestArrayList = new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public CarItemAdapter(Context context) {
        this.context = context;
    }



    public interface Callback {
        void onDeleteClick(int position, CarDetailsRequestBody carPriceRequest);
    }

    public void setData(ArrayList<CarDetailsRequestBody> carPriceRequestArrayList) {
        this.carPriceRequestArrayList = carPriceRequestArrayList;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public CarItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CarItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_car_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull CarItemViewHolder holder, int position) {
        holder.setData(carPriceRequestArrayList.get(position), position);
    }


    @Override
    public int getItemCount() {
        return carPriceRequestArrayList.size();
    }
}
