package com.sattar.cars.shift.di.module;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.ISubscribeDetailsInteractor;
import com.sattar.cars.shift.ui.main.interactor.SubscribeDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.ISubscribeDetailsPresenter;
import com.sattar.cars.shift.ui.main.presenter.SubscribeDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.SubscribeDetailsView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class SubscribeDetailsModule {

    @Provides
    @PerActivity
    ISubscribeDetailsPresenter<SubscribeDetailsView, ISubscribeDetailsInteractor> provideSubscribeDetailsPresenter(SubscribeDetailsPresenter<SubscribeDetailsView, ISubscribeDetailsInteractor> companieDetailsPresenter){
        return companieDetailsPresenter;
    }

    @Provides
    @PerActivity
    ISubscribeDetailsInteractor provideSubscribeDetailsInteractor (SubscribeDetailsInteractor eventDetailsInteractor) {
        return  eventDetailsInteractor;
    }

}
