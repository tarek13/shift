package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarDetailsRequestBody {
    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("package_id")
    @Expose
    private String packageId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("sizeName")
    @Expose
    private String sizeName;
    @SerializedName("size_id")
    @Expose
    private String sizeId;
    @SerializedName("time")
    @Expose
    private String time;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
