package com.sattar.cars.shift.di.component;

import com.sattar.cars.shift.di.module.EditUserPasswordModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.EditUserPasswordFragment;

import dagger.Component;

;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = {EditUserPasswordModule.class})
public interface EditUserPasswordComponent {
    void inject(EditUserPasswordFragment editUserPasswordFragment);
}
