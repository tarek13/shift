package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IEditUserInfoInteractor;
import com.sattar.cars.shift.ui.main.view.EditUserInfoView;


@PerActivity
public interface IEditUserInfoPresenter<V extends EditUserInfoView,I extends IEditUserInfoInteractor> extends IBasePresenter<V,I> {

    void onSaveButtonClick(RegisterRequestBody registerRequestBody);


    boolean validateUserData(String firstName, String email, String mobile,  String hospital);

    boolean checkIfEmailChange(String email);
}
