package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.AboutUsInteractor;
import com.sattar.cars.shift.ui.main.interactor.IAboutUsInteractor;
import com.sattar.cars.shift.ui.main.presenter.AboutUsPresenter;
import com.sattar.cars.shift.ui.main.presenter.IAboutUsPresenter;
import com.sattar.cars.shift.ui.main.view.AboutUsView;


import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class AboutUsModule {

    @Provides
    @PerActivity
    IAboutUsPresenter<AboutUsView,IAboutUsInteractor> provideAboutUsPresenter (AboutUsPresenter<AboutUsView,IAboutUsInteractor> aboutUsPresenter){
        return aboutUsPresenter;
    }


    @Provides
    @PerActivity
    IAboutUsInteractor provideAboutUsInteractor (AboutUsInteractor aboutUsInteractor){
        return aboutUsInteractor;
    }
   

}
