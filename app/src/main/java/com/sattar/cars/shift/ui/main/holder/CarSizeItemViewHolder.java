package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse.Size;
import com.sattar.cars.shift.ui.main.adapter.CarSizeListAdapter;
import com.sattar.cars.shift.utils.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarSizeItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.car_size_logo_image_view)
    ImageView carSizeIconImageView;
    @BindView(R.id.car_size_title_text_view)
    CustomTextView carSizeTitleImageView;
    @BindView(R.id.car_size_percentage_text_view)
    CustomTextView carSizePercentageTextView;
    @BindView(R.id.car_size_container)
    RelativeLayout carSizeContainer;

    private CarSizeListAdapter.Callback callback;
    private Context context;
    private int position;
    private ServiceDetailsResponse.Size carSizesItemResponse;
    private ServiceDetailsResponse.Size selectedItem;

    public CarSizeItemViewHolder(View itemView, Context context, CarSizeListAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;
        itemView.setOnClickListener(this);
    }
    public void setCarSize(ServiceDetailsResponse.Size carSizesItemResponse, int position) {
        this.carSizesItemResponse = carSizesItemResponse;
        this.position=position;
        setup();
    }

    private void setup() {
        carSizeTitleImageView.setText(carSizesItemResponse.getName());
        
        if(carSizesItemResponse.getIcon()!=null   ) {
            GlideApp.with(context).load("http://ur-business.net/shift_new/"+carSizesItemResponse.getIcon()).error(R.drawable.care_care).into(carSizeIconImageView);
        }

        if(selectedItem!=null && selectedItem.getId().equals(carSizesItemResponse.getId())) {
            carSizesItemResponse.setSelected(true);
            //storesTitleRadioButton.setChecked(carSizesItemResponse.isSelected());
            if(carSizesItemResponse.isSelected()){
                carSizeContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_gold_color));
            }else {
                carSizeContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
            }

        }else {
            if(carSizesItemResponse.isSelected()){
                carSizeContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_gold_color));
            }else {
                carSizeContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
        }

        setChecked(carSizesItemResponse.isSelected());

    }

    @Override
    public void onClick(View v) {
        setChecked(!carSizesItemResponse.isSelected());
        callback.onCarSizeItemClick(position, carSizesItemResponse);
    }

    public void setChecked(boolean value) {
        carSizesItemResponse.setSelected(value);
        if(value){
            carSizeContainer.setBackgroundColor(context.getResources().getColor(R.color.yellow_gold_color));
        }else {
            carSizeContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

    }
}
