package com.sattar.cars.shift.di.module;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.ISearchMapInteractor;
import com.sattar.cars.shift.ui.main.interactor.SearchMapInteractor;
import com.sattar.cars.shift.ui.main.presenter.ISearchMapPresenter;
import com.sattar.cars.shift.ui.main.presenter.SearchMapPresenter;
import com.sattar.cars.shift.ui.main.view.SearchMapView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class SearchMapModule {

    @Provides
    @PerActivity
    ISearchMapPresenter<SearchMapView,ISearchMapInteractor> provideSearchMapPresenter(SearchMapPresenter<SearchMapView,ISearchMapInteractor> searchMapPresenter){
        return  searchMapPresenter;
    }

    @Provides
    @PerActivity
    ISearchMapInteractor provideSearchMapInteractor (SearchMapInteractor searchMapInteractor) {
        return  searchMapInteractor;
    }

   


}
