package com.sattar.cars.shift.ui.forgetpassword.interactor;

import com.sattar.cars.shift.data.network.model.ForgetPasswordResponse;
import com.sattar.cars.shift.data.network.model.LoginErrorResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;

public interface IForgetPasswordInteractor extends IBaseInteractor {

    Observable<ForgetPasswordResponse> sendForgetPassword(Map<String,String> forgetPasswordRequestBody);

    LoginErrorResponse handleErrorResponse(Response<?> response);
}
