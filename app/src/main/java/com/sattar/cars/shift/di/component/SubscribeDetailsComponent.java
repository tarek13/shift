package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.SubscribeDetailsModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.SubscribeDetailsFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {SubscribeDetailsModule.class})
public interface SubscribeDetailsComponent {

    void inject(SubscribeDetailsFragment subscribeDetailsFragment);
}
