/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.sattar.cars.shift;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.sattar.cars.shift.di.component.ApplicationComponent;
import com.sattar.cars.shift.di.component.DaggerApplicationComponent;
import com.sattar.cars.shift.di.module.ApplicationModule;



/**
 * Created by janisharali on 27/01/17.
 */

public class ShiftApp extends Application {


    private ApplicationComponent mApplicationComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);


      //  MultiDex.install(this);
       // FacebookSdk.sdkInitialize(getApplicationContext());
       // OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.DEBUG);

        /*if(getResources().getString(R.string.language).equals(AppConstants.ARABIC)) {
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/tajawal_regular.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }*/
    }



    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
