/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.sattar.cars.shift.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;

import com.sattar.cars.shift.R;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeoutException;


public final class AppUtils {

    private AppUtils() {
        // This class is not publicly instantiable
    }

    public static void openPlayStoreForApp(Context context) {
        /*final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context
                            .getResources()
                            .getString(R.string.app_market_link) + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context
                            .getResources()
                            .getString(R.string.app_google_play_store_link) + appPackageName)));
        }*/
    }
    public static String getPathFromURI(Uri contentUri,Context context) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

/*
    public static void  selectImage(final Context context, final EditProfileInfoFragment editProfileInfoFragment) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    editProfileInfoFragment.startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    editProfileInfoFragment.startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
*/

    public static String convertDateFormatToAnother(String srcDateFormat,String srcDate,String destDateFormat,Locale locale) {
        try {
            DateFormat srcDf = new SimpleDateFormat(srcDateFormat,Locale.ENGLISH);
            // parse the date string into Date object
            Date date = srcDf.parse(srcDate);
            DateFormat destDf = new SimpleDateFormat(destDateFormat, locale);
            // format the date into another format
            return destDf.format(date);
        }
        catch (Exception e) {
            e.printStackTrace();
            return srcDate;
        }

    }

    public static String convertArrayListOfStringToOneString(ArrayList<String> valueArrayList,int limit){
        String result="";
        if(valueArrayList!=null && valueArrayList.size()>0) {
            StringBuilder resultStringBuilder = new StringBuilder();
            for (int i = 0; i < limit; i++) {
                resultStringBuilder.append(valueArrayList.get(i));
                resultStringBuilder.append(",");
            }
            result = resultStringBuilder.toString();
            result = result.substring(0, result.length() - ",".length());
        }
        return result;
    }


    /*public static int getComeFromFlag(String title){
        if(title.equals("featured_offers")){
            return AppConstants.COME_FROM_PREMIUM_OFFERS;
        }else if(title.equals("latest_offers")){
            return AppConstants.COME_FROM_LATEST_OFFERS;
        }else if(title.equals("goomla_10_or_less")){
            return AppConstants.COME_FROM_GOOMLA_TEN_PEOPLE_LEFT_OFFERS;
        }else {
            return AppConstants.COME_FROM_PREMIUM_OFFERS;
        }
    }*/
    public static String fetchErrorMessage(Context context,Throwable throwable) {
        String errorMsg = context.getResources().getString(R.string.error_msg_unknown);

        if (!NetworkUtils.isNetworkConnected(context)) {
            errorMsg = context.getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = context.getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    public static String diffBetweenToDates(String dateStop){

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = Calendar.getInstance().getTime();
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            DecimalFormat decimalFormat= new DecimalFormat("00");
            String days="00";
            if(diffDays<10){
                days= decimalFormat.format(diffDays);
            }else {
                days=decimalFormat.format(diffDays)+"";
            }
            String hours="00";
            if(diffHours<10){
                hours= decimalFormat.format(diffHours);
            }else {
                hours=decimalFormat.format(diffHours)+"";
            }
            String minutes="00";
            if(diffMinutes<10){
                minutes= decimalFormat.format(diffMinutes);
            }else {
                minutes=decimalFormat.format(diffMinutes)+"";
            }
            String seconds="00";
            if(diffSeconds<10){
                seconds= decimalFormat.format(diffSeconds);
            }else {
                seconds=decimalFormat.format(diffSeconds)+"";
            }
            return days+"-"+hours+"-"+minutes+"-"+seconds;

        } catch (Exception e) {
            e.printStackTrace();
            return "00-00-00-00-00";
        }

    }

    public static String countDownTimerToDate(long millisUntilFinished){
        long  diffDays = (int) ((millisUntilFinished / 1000) / 86400);
        long diffHours = (int) (((millisUntilFinished / 1000) - (diffDays * 86400)) / 3600);
        long diffMinutes = (int) (((millisUntilFinished / 1000) - ((diffDays * 86400) + (diffHours * 3600))) / 60);
        long diffSeconds = (int) ((millisUntilFinished / 1000) % 60);

        DecimalFormat decimalFormat= new DecimalFormat("00");
        String days="00";
        if(diffDays<10){
            days= decimalFormat.format(diffDays);
        }else {
            days=diffDays+"";
        }
        String hours="00";
        if(diffHours<10){
            hours= decimalFormat.format(diffHours);
        }else {
            hours=diffHours+"";
        }
        String minutes="00";
        if(diffMinutes<10){
            minutes= decimalFormat.format(diffMinutes);
        }else {
            minutes=diffMinutes+"";
        }
        String seconds="00";
        if(diffSeconds<10){
            seconds= decimalFormat.format(diffSeconds);
        }else {
            seconds=diffSeconds+"";
        }
        return days+"-"+hours+"-"+minutes+"-"+seconds;
    }

    public static Object getKeyFromValue(HashMap<Integer, String> hm, Object value) {
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }
    public static Bitmap createDrawableFromView(Context context, View view) {
        /*DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);*/
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(canvas);
        return bitmap;
    }

    public    static String getAssetsJSON(Context context, String fileName) {
        String json = null;
        try {
            InputStream inputStream = context.getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }
}
