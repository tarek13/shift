package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.FinishedOrdersModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.FinishedOrdersFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {FinishedOrdersModule.class})
public interface FinishedOrdersComponent {

    void inject(FinishedOrdersFragment finishedOrdersFragment);
}
