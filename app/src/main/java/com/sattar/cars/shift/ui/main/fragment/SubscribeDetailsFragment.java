package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.SearchBodyRequest;
import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.di.component.DaggerSubscribeDetailsComponent;
import com.sattar.cars.shift.di.component.SubscribeDetailsComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.SubscribeDetailsModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.interactor.ISubscribeDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.ISubscribeDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.SubscribeDetailsView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubscribeDetailsFragment extends BaseFragment implements SubscribeDetailsView {
    public static final String TAG = "SubscribeDetailsFragment";

    @BindView(R.id.subscribe_company_icon_image_view)
    ImageView subscribeCompanyIconImageView;
    @BindView(R.id.subscribe_company_title_text_view)
    CustomTextView subscribeCompanyTitleTextView;
    @BindView(R.id.subscribe_service_title_text_view)
    CustomTextView subscribeServiceTitleTextView;
    @BindView(R.id.subscribe_rate_container)
    CustomTextView subscribeRateContainer;
    @BindView(R.id.subscribe_details_date_edit_text)
    EditText subscribeDetailsDateEditText;
    @BindView(R.id.subscribe_details_time_edit_text)
    EditText subscribeDetailsTimeEditText;

    @Inject
    ISubscribeDetailsPresenter<SubscribeDetailsView, ISubscribeDetailsInteractor> subscribeDetailsPresenter;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    SubscribeDetailsComponent subscribeDetailsComponent;

    int comeFrom;



    private SearchBodyRequest searchRequestBody;
    private DepartmentResponse.Data data;
    private SubscribesItemResponse subscribesItemResponse;
    private String dateReserve;
    private String selectedDateReserve;
    private int selectedTimeIndex=-1;
    private String selectedTime;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        SubscribeDetailsFragment fragment = new SubscribeDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setComeFrom( DepartmentResponse.Data data, SubscribesItemResponse subscribesItemResponse) {
        this.comeFrom = comeFrom;
        this.data = data;
        this.subscribesItemResponse = subscribesItemResponse;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscribe_details, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        subscribeDetailsComponent = DaggerSubscribeDetailsComponent.builder()
                .subscribeDetailsModule(new SubscribeDetailsModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        subscribeDetailsComponent.inject(this);

        subscribeDetailsPresenter.onAttach(this);
        //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        getBaseActivity().getSupportActionBar().setTitle(data.getName());
        subscribeCompanyTitleTextView.setText(subscribesItemResponse.getCompanyName());
        subscribeServiceTitleTextView.setText(subscribesItemResponse.getServiceName());
        String rate=subscribesItemResponse.getDone()+"/"+subscribesItemResponse.getWashCount();
        subscribeRateContainer.setText(rate);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        subscribeDetailsPresenter.onDetach();

    }


    @OnClick({R.id.subscribe_details_date_edit_text, R.id.subscribe_details_time_edit_text, R.id.subscribe_detail_save_button_container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.subscribe_details_date_edit_text:
                showCustomDatePicker();
                break;
            case R.id.subscribe_details_time_edit_text:
                if(selectedDateReserve!=null && !selectedDateReserve.isEmpty()) {
                    subscribeDetailsPresenter.getAvaliableTime(getString(R.string.language),selectedDateReserve);
                }else{
                    onError(R.string.time_error);
                }
                break;
            case R.id.subscribe_detail_save_button_container:
                if(subscribeDetailsPresenter.validateUserData(selectedDateReserve,selectedTime)){
                    subscribeDetailsPresenter.getMakeOrder(getString(R.string.language),selectedDateReserve,selectedTime,subscribesItemResponse.getOrderNum(),subscribeDetailsPresenter.getInteractor().getPreferencesHelper().getCurrentUser().getId());
                }
                break;
        }
    }

    private void showCustomDatePicker() {

        DatePickerDialog datepickerdialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year1, int monthOfYear, int dayOfMonth) {
                        DatePickerDialog datePicker = view;
                        int day = dayOfMonth;
                        int month = monthOfYear + 1;
                        int year = year1;

                        String dayRequest = "";
                        String monthRequest = "";
                        String yearRequest = "";


                        String date = "";
                        if (day < 10) {
                            DecimalFormat decimalFormat = new DecimalFormat("00");
                            dayRequest = decimalFormat.format(day);
                            date = date + decimalFormat.format(day) + "/";


                        } else {
                            dayRequest = day + "";
                            date = date + day + "/";
                        }
                        if (month < 10) {
                            DecimalFormat decimalFormat = new DecimalFormat("00");
                            monthRequest = decimalFormat.format(month);

                            date = date + decimalFormat.format(month) + "/";
                        } else {
                            monthRequest = month + "";
                            date = date + month + "/";
                        }
                        yearRequest = year + "";
                        dateReserve = date + year;
                        subscribeDetailsDateEditText.setText(dateReserve);

                        selectedDateReserve = yearRequest + "-" + monthRequest + "-" + dayRequest;

                    }
                }
        );
        datepickerdialog.setAccentColor(getResources().getColor(R.color.colorAccent)); // custom accent color
        datepickerdialog.setTitle(getString(R.string.select_birth_date)); //dialog title
        Date dateFromDate = null;
        if (selectedDateReserve != null) {
            try {
                dateFromDate = new SimpleDateFormat("yyyy-MM-dd").parse(selectedDateReserve);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        if (dateFromDate != null) {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTimeInMillis(dateFromDate.getTime());
        }

        datepickerdialog.show(getBaseActivity().getFragmentManager(), "Datepickerdialog"); //show dialog
    }

    @Override
    public void showAvaliableTime(ArrayList<String> data) {

        new MaterialDialog.Builder(getBaseActivity())
                .title(R.string.select_time)
                .items(data)
                .itemsCallbackSingleChoice(selectedTimeIndex, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        selectedTimeIndex = dialog.getSelectedIndex();
                        if (selectedTimeIndex != -1) {
                            selectedTime = text.toString();
                            subscribeDetailsTimeEditText.setText(selectedTime);
                        }
                        return true;
                    }
                })
                .positiveText(R.string.select).negativeText(R.string.cancel)
                .show();
    }
}
