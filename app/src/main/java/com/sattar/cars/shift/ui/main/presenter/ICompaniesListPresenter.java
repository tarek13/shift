package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.ICompaniesListInteractor;
import com.sattar.cars.shift.ui.main.view.CompaniesListView;

@PerActivity
public interface ICompaniesListPresenter<V extends CompaniesListView,I extends ICompaniesListInteractor> extends IBasePresenter<V,I> {
    void loadAllCompanies(String lang,String departmentId);
}

