package com.sattar.cars.shift.ui.main.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.network.OrdersApi;
import com.sattar.cars.shift.data.network.model.AboutUsResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class AboutUsInteractor extends BaseInteractor implements IAboutUsInteractor {

    private Context context;
    private OrdersApi ordersApi;
    private Retrofit retrofit;

    @Inject
    public AboutUsInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper, OrdersApi ordersApi, Retrofit retrofit) {
        super(preferencesHelper);
        this.context=context;
        this.ordersApi = ordersApi;
        this.retrofit=retrofit;
    }


    @Override
    public Observable<AboutUsResponse> getAboutUs(String lang,String user_id) {
        return ordersApi.getAboutUs(lang,user_id);
    }
}
