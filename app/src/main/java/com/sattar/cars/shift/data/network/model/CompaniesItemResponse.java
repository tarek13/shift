package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CompaniesItemResponse {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("name_inner")
    @Expose
    private String nameInner;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("services")
    @Expose
    private ArrayList<ServicesItemResponse> services = null;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getNameInner() {
        return nameInner;
    }

    public void setNameInner(String nameInner) {
        this.nameInner = nameInner;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ArrayList<ServicesItemResponse> getServices() {
        return services;
    }

    public void setServices(ArrayList<ServicesItemResponse> services) {
        this.services = services;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
