package com.sattar.cars.shift.ui.main.fragment;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.di.component.DaggerRateComponent;
import com.sattar.cars.shift.di.component.RateComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.RateModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.interactor.IRateInteractor;
import com.sattar.cars.shift.ui.main.presenter.IRatePresenter;
import com.sattar.cars.shift.ui.main.view.RateView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RateFragment extends BaseFragment implements RateView {
    public static final String TAG = "RateFragment";


    @Inject
    IRatePresenter<RateView, IRateInteractor> ratePresenter;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    RateComponent rateComponent;
    @BindView(R.id.rate_company_icon_image_view)
    ImageView rateCompanyIconImageView;
    @BindView(R.id.rate_company_title_text_view)
    CustomTextView rateCompanyTitleTextView;
    @BindView(R.id.rate_service_title_text_view)
    CustomTextView rateServiceTitleTextView;
    @BindView(R.id.rate_info_container)
    LinearLayout rateInfoContainer;
    @BindView(R.id.rate_rate_container)
    CustomTextView rateRateContainer;
    @BindView(R.id.my_order_rate_container)
    RatingBar myOrderRateContainer;
    @BindView(R.id.rate_comment_edit_text)
    EditText rateCommentEditText;
    @BindView(R.id.rate_detail_save_button_container)
    LinearLayout rateDetailSaveButtonContainer;


    private MyOrderItemResponse myOrderItemResponse;
    private float selectedRating=0;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        RateFragment fragment = new RateFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setData(MyOrderItemResponse myOrderItemResponse) {
        this.myOrderItemResponse = myOrderItemResponse;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_make_rate, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        rateComponent = DaggerRateComponent.builder()
                .rateModule(new RateModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        rateComponent.inject(this);

        ratePresenter.onAttach(this);
        //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        rateCompanyTitleTextView.setText(myOrderItemResponse.getNameInner());
        if(getString(R.string.language).equals(AppConstants.ARABIC)) {
            rateServiceTitleTextView.setText(myOrderItemResponse.getNameAR());
        }else {
            rateServiceTitleTextView.setText(myOrderItemResponse.getNameEN());
        }
        if(myOrderItemResponse.getLogo()!=null   ) {
            GlideApp.with(getBaseActivity()).load("http://ur-business.net/shift_new/"+myOrderItemResponse.getLogo()).error(R.drawable.care_care).into(rateCompanyIconImageView);
        }

        myOrderRateContainer.setNumStars(5);
        LayerDrawable stars = (LayerDrawable) myOrderRateContainer.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ContextCompat.getColor(getBaseActivity(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);

        myOrderRateContainer.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                selectedRating=rating;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ratePresenter.onDetach();


    }


    @OnClick(R.id.rate_detail_save_button_container)
    public void onViewClicked() {
        if(selectedRating<0 || rateCommentEditText.getText().toString().isEmpty()){
            onError(R.string.comment_error);
        }else {
            ratePresenter.rateOrder(getString(R.string.language),String.valueOf(selectedRating),myOrderItemResponse.getCompanyId(),rateCommentEditText.getText().toString(),ratePresenter.getInteractor().getPreferencesHelper().getCurrentUser().getId());

        }

    }

    @Override
    public void showSuccess(String message) {
        showMessage(message, Toast.LENGTH_LONG);
        myOrderRateContainer.setRating(0);
        rateCommentEditText.setText(null);
    }
}
