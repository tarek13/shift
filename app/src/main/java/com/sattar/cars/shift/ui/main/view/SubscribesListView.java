package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface SubscribesListView extends IBaseView {

    void showSubscribesList(ArrayList<SubscribesItemResponse> eventItemResponseArrayList);


    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);
}
