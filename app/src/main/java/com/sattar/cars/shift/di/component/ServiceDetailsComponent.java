package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.ServiceDetailsModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.ServiceDetailsFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ServiceDetailsModule.class})
public interface ServiceDetailsComponent {

    void inject(ServiceDetailsFragment subscribeDetailsFragment);
}
