package com.sattar.cars.shift.ui.main.view;

import com.sattar.cars.shift.ui.base.view.IBaseView;

public interface EditUserPasswordView extends IBaseView {

    void onOldPasswordError(int passwordRequiredError);

    void onNewPasswordError(int passwordRequiredError);

    void onConfirmPasswordError(int confirmPasswordRequiredError);

    void exitTheApplication();
}
