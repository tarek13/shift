package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IMyOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.MyOrderDetailsView;

@PerActivity
public interface IMyOrderDetailsPresenter<V extends MyOrderDetailsView,I extends IMyOrderDetailsInteractor> extends IBasePresenter<V,I> {

    void rateOrder(String lang, String rate, String companyId, String comment, String clintId);

}

