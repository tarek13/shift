package com.sattar.cars.shift.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.di.component.DaggerRegistrationComponent;
import com.sattar.cars.shift.di.component.RegistrationComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.RegistrationModule;
import com.sattar.cars.shift.ui.base.BaseActivity;
import com.sattar.cars.shift.ui.login.LoginActivity;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.register.interactor.IRegistrationInteractor;
import com.sattar.cars.shift.ui.register.presenter.IRegistrationPresenter;
import com.sattar.cars.shift.ui.register.view.RegistrationView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends BaseActivity implements RegistrationView {

    @BindView(R.id.register_first_name_edit_text)
    EditText registerFirstNameEditText;
    @BindView(R.id.register_error_first_name_text_view)
    CustomTextView registerErrorFirstNameTextView;
    @BindView(R.id.register_email_edit_text)
    EditText registerEmailEditText;
    @BindView(R.id.register_error_email_text_view)
    CustomTextView registerErrorEmailTextView;
    @BindView(R.id.register_password_edit_text)
    EditText registerPasswordEditText;
    @BindView(R.id.register_error_password_text_view)
    CustomTextView registerErrorPasswordTextView;
    @BindView(R.id.register_confirm_password_edit_text)
    EditText registerConfirmPasswordEditText;
    @BindView(R.id.register_error_confirm_password_text_view)
    CustomTextView registerErrorConfirmPasswordTextView;
    @BindView(R.id.register_mobile_edit_text)
    EditText registerMobileEditText;
    @BindView(R.id.register_error_mobile_text_view)
    CustomTextView registerErrorMobileTextView;

    @BindView(R.id.register_signup_button_container)
    LinearLayout registerSignupButtonContainer;

    @Inject
    IRegistrationPresenter<RegistrationView, IRegistrationInteractor> registrationPresenter;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;

    RegistrationComponent registrationComponent;

    private String firstName;
    private String email;
    private String password;
    private String confirmPassword;
    private String mobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setUnBinder(ButterKnife.bind(this));
        registrationComponent = DaggerRegistrationComponent.builder()
                .registrationModule(new RegistrationModule())
                .activityModule(new ActivityModule(this))
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .applicationComponent(((ShiftApp) getApplication()).getComponent())

                .build();
        registrationComponent.inject(this);

        registrationPresenter.onAttach(this);
        //comeFromFlag = getIntent().getIntExtra(AppConstants.COME_FROM_FLAG_KEY, 0);

        setUp();
    }

    @Override
    protected void setUp() {
        registerErrorFirstNameTextView.setVisibility(View.GONE);
        registerErrorEmailTextView.setVisibility(View.GONE);
        registerErrorPasswordTextView.setVisibility(View.GONE);
        registerErrorConfirmPasswordTextView.setVisibility(View.GONE);
        registerErrorMobileTextView.setVisibility(View.GONE);


    }

    @OnClick({R.id.register_signup_button_container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.register_signup_button_container:
                fillUserDataFromUI();
                if (registrationPresenter.validateUserData(firstName, email, password, confirmPassword,
                       mobile)) {
                    changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.WEBSITE_API_URL_1);
                    registrationPresenter.onRegisterButtonClick(buildRegisterRequestBody());
                }
                break;
        }
    }

    private void fillUserDataFromUI() {

        registerErrorFirstNameTextView.setVisibility(View.GONE);
        registerErrorEmailTextView.setVisibility(View.GONE);
        registerErrorPasswordTextView.setVisibility(View.GONE);
        registerErrorConfirmPasswordTextView.setVisibility(View.GONE);
        registerErrorMobileTextView.setVisibility(View.GONE);


        firstName = registerFirstNameEditText.getText().toString();
        email = registerEmailEditText.getText().toString();
        password = registerPasswordEditText.getText().toString();
        confirmPassword = registerConfirmPasswordEditText.getText().toString();
        mobile=registerMobileEditText.getText().toString();



    }

    private RegisterRequestBody buildRegisterRequestBody() {
        RegisterRequestBody registerRequestBody = new RegisterRequestBody();

        registerRequestBody.setLang(getString(R.string.language));
        registerRequestBody.setFirstName(firstName);
        registerRequestBody.setMail(email);
        registerRequestBody.setPassword(password);
        registerRequestBody.setMobile(mobile);


        return registerRequestBody;
    }

    @Override
    public void goToLoginActivity(String msg) {
        showMessage(msg, Toast.LENGTH_LONG);
        Intent intentMainActivity = new Intent(RegistrationActivity.this, LoginActivity.class);
        startActivity(intentMainActivity);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void openMainActivity() {
        hideKeyboard();
        Intent intentMainActivity = new Intent(RegistrationActivity.this, MainActivity.class);
        startActivity(intentMainActivity);
        finish();
    }

    @Override
    public void onFirstNameError(int firstNameError) {
        registerErrorFirstNameTextView.setVisibility(View.VISIBLE);
        registerErrorFirstNameTextView.setText(firstNameError);
        registerFirstNameEditText.requestFocus();
    }


    @Override
    public void onEmailError(int emailRequiredError) {
        registerErrorEmailTextView.setVisibility(View.VISIBLE);
        registerErrorEmailTextView.setText(emailRequiredError);
        registerEmailEditText.requestFocus();
    }

    @Override
    public void onPasswordError(int passwordRequiredError) {
        registerErrorPasswordTextView.setVisibility(View.VISIBLE);
        registerErrorPasswordTextView.setText(passwordRequiredError);
        registerPasswordEditText.requestFocus();
    }

    @Override
    public void onConfirmPasswordError(int confirmPasswordRequiredError) {
        registerErrorConfirmPasswordTextView.setVisibility(View.VISIBLE);
        registerErrorConfirmPasswordTextView.setText(confirmPasswordRequiredError);
        registerConfirmPasswordEditText.requestFocus();
    }


    @Override
    public void onMobileError(int mobileError) {
        registerErrorMobileTextView.setVisibility(View.VISIBLE);
        registerErrorMobileTextView.setText(mobileError);
        registerMobileEditText.requestFocus();
    }



    @Override
    protected void onDestroy() {
        registrationPresenter.onDetach();
        super.onDestroy();
    }
}
