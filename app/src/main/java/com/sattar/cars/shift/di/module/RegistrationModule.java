package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.register.interactor.IRegistrationInteractor;
import com.sattar.cars.shift.ui.register.interactor.RegistrationInteractor;
import com.sattar.cars.shift.ui.register.presenter.IRegistrationPresenter;
import com.sattar.cars.shift.ui.register.presenter.RegistrationPresenter;
import com.sattar.cars.shift.ui.register.view.RegistrationView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class RegistrationModule {

    @Provides
    @PerActivity
    IRegistrationPresenter<RegistrationView,IRegistrationInteractor> provideRegistrationPresenter(RegistrationPresenter<RegistrationView,IRegistrationInteractor> registrationPresenter){
        return  registrationPresenter;
    }

    @Provides
    @PerActivity
    IRegistrationInteractor provideRegistrationInteractor (RegistrationInteractor registrationInteractor){
        return registrationInteractor;
    }


}
