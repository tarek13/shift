package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.splash.interactor.ISplashInteractor;
import com.sattar.cars.shift.ui.splash.interactor.SplashInteractor;
import com.sattar.cars.shift.ui.splash.presenter.ISplashPresenter;
import com.sattar.cars.shift.ui.splash.presenter.SplashPresenter;
import com.sattar.cars.shift.ui.splash.view.SplashView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class SplashModule {

    @Provides
    @PerActivity
    ISplashPresenter<SplashView, ISplashInteractor> provideSplashPresenter(
            SplashPresenter<SplashView, ISplashInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ISplashInteractor provideSplashMvpInteractor(SplashInteractor interactor) {
        return interactor;
    }


}
