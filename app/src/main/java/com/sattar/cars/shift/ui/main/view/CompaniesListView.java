package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface CompaniesListView extends IBaseView {

    void showCompaniesList(ArrayList<CompaniesItemResponse> eventItemResponseArrayList);


    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);
}
