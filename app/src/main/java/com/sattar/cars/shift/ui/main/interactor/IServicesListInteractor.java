package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.ServicesResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;

import io.reactivex.Observable;

public interface IServicesListInteractor extends IBaseInteractor {
    Observable<ServicesResponse> getAllServices(HashMap<String, String> lang);

}
