package com.sattar.cars.shift.ui.forgetpassword.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.ForgetPasswordRequestBody;
import com.sattar.cars.shift.data.network.model.ForgetPasswordResponse;
import com.sattar.cars.shift.data.network.model.LoginErrorResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.forgetpassword.interactor.IForgetPasswordInteractor;
import com.sattar.cars.shift.ui.forgetpassword.view.ForgetPasswordView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class ForgetPasswordPresenter<V extends ForgetPasswordView, I extends IForgetPasswordInteractor>
        extends BasePresenter<V, I> implements IForgetPasswordPresenter<V, I> {

    @Inject
    public ForgetPasswordPresenter(I mvpInteractor,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onSendNowButtonClick(final ForgetPasswordRequestBody forgetPasswordRequestBody) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("lang",forgetPasswordRequestBody.getLang());
        options.put("email",forgetPasswordRequestBody.getEmail());
        getCompositeDisposable().add(getInteractor().sendForgetPassword(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<ForgetPasswordResponse>() {
                    @Override
                    public void accept(ForgetPasswordResponse forgetPasswordResponse) throws Exception {
                        getMvpView().hideLoading();
                        if(forgetPasswordResponse.getStatus()) {
                            getMvpView().returnToLoginPage(forgetPasswordResponse.getMessage());
                        }else {
                            getMvpView().showMessage(forgetPasswordResponse.getMessage(),1);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().hideLoading();
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).response().code() == 404) {
                                LoginErrorResponse loginErrorResponse = getInteractor().handleErrorResponse(((HttpException) throwable).response());
                                if (loginErrorResponse.getErrors().getEmail() != null && !loginErrorResponse.getErrors().getEmail().isEmpty()) {
                                    getMvpView().onError(loginErrorResponse.getErrors().getEmail().get(0));
                                }

                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }


                        }

                    }
                }));

    }

    @Override
    public boolean validateUserData(String email){
        if(email==null || email.trim().isEmpty() || email.trim().length()<=0){
            getMvpView().onEmailError(R.string.email_required_error);
            return false;
        }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            getMvpView().onEmailError(R.string.invalid_email_error);
            return false;
        }
        return true;
    }
}
