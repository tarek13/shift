package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.CategoryInfo;
import com.sattar.cars.shift.ui.main.adapter.MainMenuNavigationAdapter;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.views.CustomTextView;


import butterknife.BindView;
import butterknife.ButterKnife;

public class MainNavigationMenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.main_nav_menu_main_title)
    CustomTextView mainNavMenuMainTitle;
    @BindView(R.id.main_nav_menu_main_arrow)
    ImageView mainNavMenuMainArrow;
    private MainMenuNavigationAdapter.Callback callback;

    private Context context;

    private CategoryInfo mainCategoryInfo;

    private int position;
    private int lastIndex;

    public MainNavigationMenuViewHolder(View itemView, Context context, MainMenuNavigationAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
        this.callback = callback;
        itemView.setOnClickListener(this);

    }

    public void setMainMenuTitle(CategoryInfo mainMenuTitle, int position, int lastIndex) {
        this.mainCategoryInfo = mainMenuTitle;
        this.position = position;
        this.lastIndex = lastIndex;
        setup();
    }

    private void setup() {
        mainNavMenuMainTitle.setText(mainCategoryInfo.getTitle());
        if (context.getString(R.string.language).equals(AppConstants.ENGLISH)) {
            mainNavMenuMainArrow.setRotation(0);
        } else if (context.getString(R.string.language).equals(AppConstants.ARABIC)) {
            mainNavMenuMainArrow.setRotation(180);
        }

        mainNavMenuMainTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(mainCategoryInfo.getIcon(), 0, 0, 0);

    }

    @Override
    public void onClick(View v) {
        callback.onMainMenuItemClick(position, mainCategoryInfo, lastIndex);
    }
}
