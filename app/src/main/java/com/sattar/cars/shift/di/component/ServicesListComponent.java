package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.ServicesListModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.ServicesListFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ServicesListModule.class})
public interface ServicesListComponent {

    void inject(ServicesListFragment servicesListFragment);
}
