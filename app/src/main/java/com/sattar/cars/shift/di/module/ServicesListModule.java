package com.sattar.cars.shift.di.module;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.ServicesListAdapter;
import com.sattar.cars.shift.ui.main.interactor.ServicesListInteractor;
import com.sattar.cars.shift.ui.main.interactor.IServicesListInteractor;
import com.sattar.cars.shift.ui.main.presenter.ServicesListPresenter;
import com.sattar.cars.shift.ui.main.presenter.IServicesListPresenter;
import com.sattar.cars.shift.ui.main.view.ServicesListView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class ServicesListModule {

    @Provides
    @PerActivity
    IServicesListPresenter<ServicesListView, IServicesListInteractor> provideServicesListPresenter(ServicesListPresenter<ServicesListView, IServicesListInteractor> servicesListPresenter){
        return servicesListPresenter;
    }

    @Provides
    @PerActivity
    IServicesListInteractor provideServicesListInteractor (ServicesListInteractor eventsListInteractor) {
        return  eventsListInteractor;
    }

    @Provides
    @PerActivity
    ServicesListAdapter provideServicesListAdapter(@ActivityContext Context context){
        return new ServicesListAdapter(context);
    }

    @Provides
    @PerActivity
    LinearLayoutManager provideLinearManagerServicesList(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }
}
