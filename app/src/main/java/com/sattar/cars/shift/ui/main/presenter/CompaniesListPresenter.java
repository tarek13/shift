package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.ICompaniesListInteractor;
import com.sattar.cars.shift.ui.main.view.CompaniesListView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class CompaniesListPresenter<V extends CompaniesListView,I extends ICompaniesListInteractor>
        extends BasePresenter<V,I> implements ICompaniesListPresenter<V,I> {
    private static final String TAG = "Companieslist";

    @Inject
    public CompaniesListPresenter(I mvpInteractor,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void loadAllCompanies(String lang,String departmentId) {
        getMvpView().showLoadingProgressbar();
        HashMap<String,String> options=new HashMap<>();
        options.put("latitude", "24.626683");
        options.put("longitude", "46.709545");
        options.put("department_id", departmentId);
        options.put("lang", lang);

        getCompositeDisposable().add(getInteractor().getAllCompanies(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<CompaniesResponse>() {
                    @Override
                    public void accept(CompaniesResponse eventsResponse) {
                        getMvpView().hideLoadingProgressbar();

                        if(eventsResponse.getStatus()) {
                            getMvpView().showCompaniesList(eventsResponse.getData());
                        }else {
                            getMvpView().showErrorMessage(R.string.no_compaines_found, R.drawable.not_found);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoadingProgressbar();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                            }
                        }else {
                            getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);

                        }
                    }
                }));
    }

}
