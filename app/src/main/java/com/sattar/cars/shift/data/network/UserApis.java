package com.sattar.cars.shift.data.network;


import com.sattar.cars.shift.data.network.model.CountriesResponse;
import com.sattar.cars.shift.data.network.model.ForgetPasswordResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.data.network.model.RegisterResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UserApis {

    @FormUrlEncoded
    @POST("registeration")
    Observable<RegisterResponse> userRegister(@FieldMap Map<String, String> registerRequestBody);

    @FormUrlEncoded
    @POST("login")
    Observable<RegisterResponse> userLogin(@FieldMap Map<String, String> userParams);

    @FormUrlEncoded
    @POST("forget_password")
    Observable<ForgetPasswordResponse> forgetPassword(@FieldMap Map<String, String> userParams);

    @GET
    Observable<ArrayList<CountriesResponse>> getCountries();

    @FormUrlEncoded
    @POST("update_user_info")
    Observable<RegisterResponse> doEditProfile(@FieldMap Map<String, String> registerRequestBody);

    @FormUrlEncoded
    @POST("update_user_password")
    Observable<RegisterResponse> doChangePassword(@FieldMap Map<String, String> registerRequestBody);

    @FormUrlEncoded
    @POST("social_register")
    Observable<RegisterResponse> socialLogin(@FieldMap HashMap<String, String> options);

    @FormUrlEncoded
    @POST("support")
    Observable<GeneralResponse> doSupport(@FieldMap Map<String, String> registerRequestBody);

}
