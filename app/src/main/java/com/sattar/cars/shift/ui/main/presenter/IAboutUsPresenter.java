package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IAboutUsInteractor;
import com.sattar.cars.shift.ui.main.view.AboutUsView;

@PerActivity
public interface IAboutUsPresenter<V extends AboutUsView,I extends IAboutUsInteractor> extends IBasePresenter<V,I> {
    void loadAboutUs(String options);
}

