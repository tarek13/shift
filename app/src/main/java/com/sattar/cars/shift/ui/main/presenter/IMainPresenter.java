package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.data.db.model.CategoryInfo;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IMainInteractor;
import com.sattar.cars.shift.ui.main.view.MainView;

@PerActivity
public interface IMainPresenter<V extends MainView, I extends IMainInteractor> extends IBasePresenter<V, I> {
    void onMainMenuItemClick(int position, CategoryInfo mainCategoryInfo, int lastIndex);

    void loadMainMenuItem();

    void loadUserProfileDataFromPrefs();

    void doLogout();


}
