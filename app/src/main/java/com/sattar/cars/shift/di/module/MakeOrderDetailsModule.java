package com.sattar.cars.shift.di.module;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.IMakeOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.interactor.MakeOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.IMakeOrderDetailsPresenter;
import com.sattar.cars.shift.ui.main.presenter.MakeOrderDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.MakeOrderDetailsView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class MakeOrderDetailsModule {

    @Provides
    @PerActivity
    IMakeOrderDetailsPresenter<MakeOrderDetailsView, IMakeOrderDetailsInteractor> provideMakeOrderDetailsPresenter(MakeOrderDetailsPresenter<MakeOrderDetailsView, IMakeOrderDetailsInteractor> makeOrderDetailsPresenter){
        return makeOrderDetailsPresenter;
    }

    @Provides
    @PerActivity
    IMakeOrderDetailsInteractor provideMakeOrderDetailsInteractor (MakeOrderDetailsInteractor makeOrderDetailsInteractor) {
        return  makeOrderDetailsInteractor;
    }

}
