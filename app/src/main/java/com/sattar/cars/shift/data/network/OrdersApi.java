package com.sattar.cars.shift.data.network;

import com.sattar.cars.shift.data.network.model.AboutUsResponse;
import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.BannerResponse;
import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.data.network.model.MyOrdersResponse;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.data.network.model.ServicesResponse;
import com.sattar.cars.shift.data.network.model.SubscribesResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface OrdersApi {

     @FormUrlEncoded
     @POST("get_departments")
     Observable<DepartmentResponse> getDepartmentsData(@FieldMap Map<String, String> lang) ;

     @FormUrlEncoded
     @POST("get_all_banners")
     Observable<BannerResponse> getBannersData(@FieldMap Map<String, String> lang) ;

     @GET("get_about_us")
     Observable<AboutUsResponse> getAboutUs(@Query("lang") String lang, @Query("user_id") String id) ;

    @FormUrlEncoded
     @POST("get_new_companies")
    Observable<CompaniesResponse> searchByCompanies(@FieldMap Map<String, String> lang);

     @FormUrlEncoded
     @POST("get_services")
    Observable<ServicesResponse> getServices(@FieldMap HashMap<String, String> options);

     @FormUrlEncoded
     @POST("get_user_subscribe")
    Observable<SubscribesResponse> getAllSubscribes(@FieldMap HashMap<String, String> options);

     @FormUrlEncoded
     @POST("get_available_time")
     Observable<AvaliableTimeResponse> getAvaliableTime(@FieldMap HashMap<String, String> options);

     @FormUrlEncoded
     @POST("make_subscribe_order")
     Observable<GeneralResponse> makerSubscribeOrder(@FieldMap HashMap<String, String> options);

    @FormUrlEncoded
    @POST("get_status_orders")
    Observable<MyOrdersResponse> getMyOrders(@FieldMap HashMap<String, String> options);

    @FormUrlEncoded
    @POST("make_company_rate")
    Observable<GeneralResponse> rateOrder(@FieldMap HashMap<String, String> options);

    @FormUrlEncoded
    @POST("make_order")
    Observable<GeneralResponse> makerServiceOrder(@FieldMap HashMap<String, String> options);

    @FormUrlEncoded
    @POST("get_service_details")
    Observable<ServiceDetailsResponse> getServiceDetails(@FieldMap HashMap<String, String> options);
}
