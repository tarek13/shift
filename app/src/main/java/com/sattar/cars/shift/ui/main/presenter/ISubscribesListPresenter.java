package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.ISubscribesListInteractor;
import com.sattar.cars.shift.ui.main.view.SubscribesListView;

@PerActivity
public interface ISubscribesListPresenter<V extends SubscribesListView,I extends ISubscribesListInteractor> extends IBasePresenter<V,I> {
    void loadAllSubscribes(String lang, String departmentId, String id);
}

