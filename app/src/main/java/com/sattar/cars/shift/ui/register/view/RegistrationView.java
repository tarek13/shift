package com.sattar.cars.shift.ui.register.view;

import com.sattar.cars.shift.ui.base.view.IBaseView;

public interface RegistrationView extends IBaseView {

    void goToLoginActivity(String msg);

    void openMainActivity();

    void onFirstNameError(int firstNameError);

    void onEmailError(int emailRequiredError);

    void onPasswordError(int passwordRequiredError);

    void onConfirmPasswordError(int confirmPasswordRequiredError);

    void onMobileError(int governmentError);


}
