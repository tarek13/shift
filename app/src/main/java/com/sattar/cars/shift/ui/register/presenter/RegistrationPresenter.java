package com.sattar.cars.shift.ui.register.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.register.interactor.IRegistrationInteractor;
import com.sattar.cars.shift.ui.register.view.RegistrationView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;
import retrofit2.Retrofit;

public class RegistrationPresenter<V extends RegistrationView, I extends IRegistrationInteractor>
        extends BasePresenter<V, I> implements IRegistrationPresenter<V, I> {

    @Inject
    public RegistrationPresenter(I mvpInteractor,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable, Retrofit retrofit) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onRegisterButtonClick(final RegisterRequestBody registerRequestBody) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("lang",registerRequestBody.getLang());
        options.put("name",registerRequestBody.getFirstName());
        options.put("email",registerRequestBody.getMail());
        options.put("mobile",registerRequestBody.getMobile());
        options.put("password",registerRequestBody.getPassword());

        getCompositeDisposable().add(getInteractor().doUserRegistration(options)
                .subscribeOn(getSchedulerProvider().io())

                .observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<RegisterResponse>() {
                    @Override
                    public void accept(RegisterResponse registerResponse) throws Exception {
                        getMvpView().hideLoading();
                        if(registerResponse.getStatus()) {
                            getMvpView().goToLoginActivity(registerResponse.getMessage());
                        }else {
                            getMvpView().onError(registerResponse.getMessage());
                        }
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().hideLoading();
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().onError(R.string.some_error);

                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.some_error);
                        }

                    }
                }));

    }



    @Override
    public boolean validateUserData(String firstName, String email, String password, String confirmPassword, String mobile) {
        if (firstName == null || firstName.trim().isEmpty() || firstName.trim().length() <= 0) {
            getMvpView().onFirstNameError(R.string.first_name_error);
            return false;
        } else if (email == null || email.trim().isEmpty() || email.trim().length() <= 0) {
            getMvpView().onEmailError(R.string.email_required_error);
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            getMvpView().onEmailError(R.string.invalid_email_error);
            return false;
        }
        else if (mobile == null || mobile.trim().isEmpty() || mobile.trim().length() <= 0) {
            getMvpView().onMobileError(R.string.invalid_phone_number);
            return false;
        }
        else if (password == null || password.trim().isEmpty() || password.trim().length() <= 0) {
            getMvpView().onPasswordError(R.string.password_required_error);
            return false;
        } else if (password.length() < 6) {
            getMvpView().onPasswordError((R.string.password_minmimum_length_error));
            return false;
        } else if (confirmPassword == null || confirmPassword.trim().isEmpty() || confirmPassword.trim().length() <= 0) {
            getMvpView().onConfirmPasswordError(R.string.confirm_password_required_error);
            return false;
        } else if (!password.equals(confirmPassword)) {
            getMvpView().onConfirmPasswordError(R.string.password_not_match);
            return false;

        }


        return true;
    }
}
