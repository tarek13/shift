package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.ui.main.adapter.SubscribesListAdapter;
import com.sattar.cars.shift.utils.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscribeItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.subscribe_company_icon_image_view)
    ImageView companyIconImageView;
    @BindView(R.id.subscribe_company_title_text_view)
    CustomTextView companyTitleImageView;   
    @BindView(R.id.subscribe_service_title_text_view)
    CustomTextView subscribeServiceTitleTextView;
    @BindView(R.id.subscribe_rate_container)
    CustomTextView companyRateRatingBar;

    private SubscribesListAdapter.Callback callback;
    private Context context;
    private int position;
    private SubscribesItemResponse subscribesItemResponse;

    public SubscribeItemViewHolder(View itemView, Context context, SubscribesListAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;
        itemView.setOnClickListener(this);
    }
    public void setSubscribe(SubscribesItemResponse companiesItemResponse, int position) {
        this.subscribesItemResponse = companiesItemResponse;
        this.position=position;
        setup();
    }

    private void setup() {
        companyTitleImageView.setText(subscribesItemResponse.getCompanyName());
        subscribeServiceTitleTextView.setText(subscribesItemResponse.getServiceName());
        String rate=subscribesItemResponse.getDone()+"/"+subscribesItemResponse.getWashCount();
        companyRateRatingBar.setText(rate);
    }

    @Override
    public void onClick(View v) {
        callback.onSubscribeItemClick(position, subscribesItemResponse);
    }

}
