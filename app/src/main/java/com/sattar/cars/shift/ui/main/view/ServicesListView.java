package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.ServicesItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface ServicesListView extends IBaseView {

    void showServicesList(ArrayList<ServicesItemResponse> eventItemResponseArrayList);


    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);
}
