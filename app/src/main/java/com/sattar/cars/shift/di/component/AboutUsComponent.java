package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.AboutUsModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.AboutUsFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {AboutUsModule.class})
public interface AboutUsComponent {

    void inject(AboutUsFragment aboutUsFragment);
}
