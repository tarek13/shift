package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.MakeOrderRequestBody;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.di.component.DaggerMakeOrderDetailsComponent;
import com.sattar.cars.shift.di.component.MakeOrderDetailsComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.MakeOrderDetailsModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.interactor.IMakeOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.IMakeOrderDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.MakeOrderDetailsView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MakeOrderDetailsFragment extends BaseFragment implements MakeOrderDetailsView {
    public static final String TAG = "MakeOrderDetailsFragment";

    @BindView(R.id.make_order_details_service_name_text_view)
    CustomTextView makeOrderDetailsServiceNameTextView;
    @BindView(R.id.make_order_details_service_price_text_view)
    CustomTextView makeOrderDetailsServicePriceTextView;
    @BindView(R.id.make_order_details_taxes_text_view)
    CustomTextView makeOrderDetailsTaxesTextView;
    @BindView(R.id.make_order_details_final_price_text_view)
    CustomTextView makeOrderDetailsFinalPriceTextView;
    @BindView(R.id.make_order_detail_save_button_container)
    LinearLayout makeOrderDetailSaveButtonContainer;
    @BindView(R.id.make_order_details_company_name_text_view)
    CustomTextView makeOrderDetailsCompanyNameTextView;

    @Inject
    IMakeOrderDetailsPresenter<MakeOrderDetailsView, IMakeOrderDetailsInteractor> makeOrderDetailsPresenter;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    MakeOrderDetailsComponent makeOrderDetailsComponent;
    private ServiceDetailsResponse.Data serviceDetailsResponse;
    private MakeOrderRequestBody makeOrderRequestBody;


    //  private MakeOrderItemResponse makeOrderItemResponse;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        MakeOrderDetailsFragment fragment = new MakeOrderDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    /*public void setData(MakeOrderItemResponse makeOrderItemResponse) {
        this.makeOrderItemResponse = makeOrderItemResponse;
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_make_order_details, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        makeOrderDetailsComponent = DaggerMakeOrderDetailsComponent.builder()
                .makeOrderDetailsModule(new MakeOrderDetailsModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        makeOrderDetailsComponent.inject(this);

        makeOrderDetailsPresenter.onAttach(this);
        //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        //getBaseActivity().getSupportActionBar().setTitle(data.getName());

        makeOrderDetailsCompanyNameTextView.setText(makeOrderRequestBody.getCompanyName());
        makeOrderDetailsServiceNameTextView.setText(serviceDetailsResponse.getService().getName());



        makeOrderDetailsServicePriceTextView.setText(makeOrderRequestBody.getFinalPrice());
        makeOrderDetailsTaxesTextView.setText("12%");
        int finalPrice = Integer.valueOf(makeOrderRequestBody.getFinalPrice()) + ((12 * Integer.valueOf(makeOrderRequestBody.getFinalPrice())) / 100);

        makeOrderDetailsFinalPriceTextView.setText(finalPrice +" "+getString(R.string.riyal));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        makeOrderDetailsPresenter.onDetach();


    }


    @OnClick(R.id.make_order_detail_save_button_container)
    public void onViewClicked() {
        HashMap<String,String> options=new HashMap<>();
        options.put("lang",getString(R.string.language));
        options.put("client_id",makeOrderDetailsPresenter.getInteractor().getPreferencesHelper().getCurrentUser().getId());
        options.put("service_id",serviceDetailsResponse.getService().getId());
        options.put("department_id",makeOrderRequestBody.getDepartmentId());
        options.put("company_id",serviceDetailsResponse.getService().getCompanyId());
        options.put("time_from",makeOrderRequestBody.getTime());
        options.put("date",makeOrderRequestBody.getDate());
        options.put("duration",makeOrderRequestBody.getDuration());
        options.put("car_details", new Gson().toJson(makeOrderRequestBody.getCarDetailsRequestBodyArrayList()));



       makeOrderDetailsPresenter.getMakeOrder(options);
    }

    public void setData(ServiceDetailsResponse.Data serviceDetailsResponse, MakeOrderRequestBody makeOrderRequestBody) {
        this.serviceDetailsResponse=serviceDetailsResponse;
        this.makeOrderRequestBody=makeOrderRequestBody;
    }

    @Override
    public void existFragment(String message) {
        showMessage(message, Toast.LENGTH_LONG);
        ((MainActivity)getBaseActivity()).showHomeFragment();
    }
}
