package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.BannerItemResponse;
import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BannerViewPagerAdapter extends PagerAdapter {

    @BindView(R.id.banner_image_view)
    ImageView productImageView;
    @BindView(R.id.banner_title_text_view)
    CustomTextView bannerTitleTextView;
    @BindView(R.id.banner_discription_text_view)
    CustomTextView bannerDiscriptionTextView;
    private Context context;
    private ArrayList<BannerItemResponse> dataList = new ArrayList<>();
    private Callback callback;

    @Inject
    public BannerViewPagerAdapter(@ActivityContext Context context) {
        this.context = context;
    }

    public void setDataList(ArrayList<BannerItemResponse> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }


    public void clear() {
        dataList.clear();
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemClick(int position);
    }


    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;

    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_of_banner_card_view, container, false);

        ButterKnife.bind(this, itemView);
        /*TextViewWithArabicDigits imageView = (TextViewWithArabicDigits) itemView.findViewById(R.id.top_offers_description_text_view);
        imageView.setText(dataList.get(position));*/
        if(dataList.get(position).getImage()!=null) {
            GlideApp.with(context).load("http://ur-business.net/shift_new/" + dataList.get(position).getImage()).error(R.drawable.car_wash_1).into(productImageView);
        }
        bannerTitleTextView.setText(dataList.get(position).getTitle());
        bannerDiscriptionTextView.setText(dataList.get(position).getDescrbtion());

        container.addView(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemClick(position);
            }
        });
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((CardView) object);
    }

    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

}
