package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.SearchBodyRequest;
import com.sattar.cars.shift.di.component.DaggerCompaniesListComponent;
import com.sattar.cars.shift.di.component.CompaniesListComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.CompaniesListModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.CompaniesListAdapter;
import com.sattar.cars.shift.ui.main.interactor.ICompaniesListInteractor;
import com.sattar.cars.shift.ui.main.presenter.ICompaniesListPresenter;
import com.sattar.cars.shift.ui.main.view.CompaniesListView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompaniesListFragment extends BaseFragment implements CompaniesListView, CompaniesListAdapter.Callback {
    public static final String TAG = "CompaniesListFragment";

    @BindView(R.id.companies_list_recycler_view)
    RecyclerView companiesListRecyclerView;

    @BindView(R.id.companies_list_error)
    CustomTextView companiesListError;

    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;

    @BindView(R.id.companies_list_continer)
    CardView companiesListContiner;

    @BindView(R.id.companies_list_title_text_view)
    CustomTextView companiesListTitleTextView;

    @Inject
    ICompaniesListPresenter<CompaniesListView, ICompaniesListInteractor> companiesListPresenter;

    @Inject
    CompaniesListAdapter companiesListAdapter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    CompaniesListComponent companiesListComponent;

    int comeFrom;


    private SearchBodyRequest searchRequestBody;
    private DepartmentResponse.Data data;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        CompaniesListFragment fragment = new CompaniesListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setComeFrom(int comeFrom, DepartmentResponse.Data data) {
        this.comeFrom = comeFrom;
        this.data=data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_companies_list, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        companiesListComponent = DaggerCompaniesListComponent.builder()
                .companiesListModule(new CompaniesListModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        companiesListComponent.inject(this);

        companiesListPresenter.onAttach(this);
     //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        ((MainActivity)getBaseActivity()).getSupportActionBar().setTitle(data.getName());

        companiesListRecyclerView.setLayoutManager(linearLayoutManager);
        companiesListRecyclerView.setAdapter(companiesListAdapter);
        companiesListAdapter.setCallback(this);
        companiesListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        companiesListPresenter.loadAllCompanies(getString(R.string.language),data.getId());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        companiesListPresenter.onDetach();
    }


    @Override
    public void showCompaniesList(ArrayList<CompaniesItemResponse> companiesItemResponses) {
        if (companiesItemResponses != null && companiesItemResponses.size() > 0) {
            companiesListRecyclerView.setVisibility(View.VISIBLE);
            companiesListError.setVisibility(View.GONE);
            companiesListAdapter.setData(companiesItemResponses);
        } else {
            showErrorMessage(R.string.no_compaines_found, R.drawable.not_found);
        }


    }


    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            companiesListContiner.setVisibility(View.GONE);

        }

    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            companiesListContiner.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
        companiesListContiner.setVisibility(View.GONE);
        companiesListError.setVisibility(View.VISIBLE);
        companiesListError.setText(messageID);
        companiesListError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void onCompanyItemClick(int position, CompaniesItemResponse companiesItemResponse) {
        ((MainActivity)getBaseActivity()).showServicesListFragment(0,companiesItemResponse,data);
    }

    public void setSearchRequestBody(SearchBodyRequest searchRequestBody) {
        this.searchRequestBody = searchRequestBody;
    }

    public SearchBodyRequest getSearchRequestBody() {
        return searchRequestBody;
    }

    public DepartmentResponse.Data getData() {
        return data;
    }
}
