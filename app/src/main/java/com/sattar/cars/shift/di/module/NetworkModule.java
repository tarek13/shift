package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 *
 *
 */

@Module
public class NetworkModule {

    private String baseUrl;

    public NetworkModule(String baseUrl ) {
        this.baseUrl=baseUrl;
    }


    @Provides
    ChangeBaseUrlInterceptorWithLogging provideInterceptor() { // This is where the Interceptor object is constructed
        return ChangeBaseUrlInterceptorWithLogging.get();
    }

    @Provides
    OkHttpClient provideOkHttpClient(ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
// add your other interceptors …

// add logging as last interceptor

        httpClient.addInterceptor(logging);
        httpClient.addInterceptor(changeBaseUrlInterceptorWithLogging);

        // <-- this is the important line!
     /*   httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.writeTimeout(60,TimeUnit.SECONDS);
        httpClient.readTimeout(60,TimeUnit.SECONDS);*/
        return httpClient.build();
    }

    @Provides
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }



    @Provides
    @PerActivity
    Retrofit provideRetrofit(OkHttpClient okHttpClient, GsonConverterFactory factory) {
        return new Retrofit.Builder()
                .baseUrl(AppConstants.WEBSITE_API_URL_1)
                .client(okHttpClient)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
