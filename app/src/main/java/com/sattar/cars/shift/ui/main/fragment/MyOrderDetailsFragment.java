package com.sattar.cars.shift.ui.main.fragment;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.di.component.DaggerMyOrderDetailsComponent;
import com.sattar.cars.shift.di.component.MyOrderDetailsComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.MyOrderDetailsModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.interactor.IMyOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.IMyOrderDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.MyOrderDetailsView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyOrderDetailsFragment extends BaseFragment implements MyOrderDetailsView {
    public static final String TAG = "MyOrderDetailsFragment";
    @BindView(R.id.my_order_company_icon_image_view)
    ImageView myOrderCompanyIconImageView;
    @BindView(R.id.my_order_company_title_text_view)
    CustomTextView myOrderCompanyTitleTextView;
    @BindView(R.id.my_order_service_title_text_view)
    CustomTextView myOrderServiceTitleTextView;
    @BindView(R.id.my_order_rate_container)
    RatingBar myOrderRateContainer;
    @BindView(R.id.my_order_info_container)
    LinearLayout myOrderInfoContainer;
    @BindView(R.id.my_order_details_service_name_text_view)
    CustomTextView myOrderDetailsServiceNameTextView;
    @BindView(R.id.my_order_details_service_price_text_view)
    CustomTextView myOrderDetailsServicePriceTextView;
    @BindView(R.id.my_order_details_taxes_text_view)
    CustomTextView myOrderDetailsTaxesTextView;
    @BindView(R.id.my_order_details_final_price_text_view)
    CustomTextView myOrderDetailsFinalPriceTextView;
    @BindView(R.id.my_order_details_date_edit_text)
    CustomTextView myOrderDetailsDateEditText;
    @BindView(R.id.my_order_detail_save_button_container)
    LinearLayout myOrderDetailSaveButtonContainer;

    @Inject
    IMyOrderDetailsPresenter<MyOrderDetailsView, IMyOrderDetailsInteractor> myOrderDetailsPresenter;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    MyOrderDetailsComponent myOrderDetailsComponent;


    private MyOrderItemResponse myOrderItemResponse;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        MyOrderDetailsFragment fragment = new MyOrderDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setData( MyOrderItemResponse myOrderItemResponse) {
        this.myOrderItemResponse = myOrderItemResponse;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_order_details, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        myOrderDetailsComponent = DaggerMyOrderDetailsComponent.builder()
                .myOrderDetailsModule(new MyOrderDetailsModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        myOrderDetailsComponent.inject(this);

        myOrderDetailsPresenter.onAttach(this);
        //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        //getBaseActivity().getSupportActionBar().setTitle(data.getName());

        myOrderRateContainer.setNumStars(5);
        LayerDrawable stars = (LayerDrawable) myOrderRateContainer.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ContextCompat.getColor(getBaseActivity(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);


        myOrderCompanyTitleTextView.setText(myOrderItemResponse.getNameInner());
        if(getString(R.string.language).equals(AppConstants.ARABIC)) {
            myOrderServiceTitleTextView.setText(myOrderItemResponse.getNameAR());
            myOrderDetailsServiceNameTextView.setText(myOrderItemResponse.getNameAR());
        }else {
            myOrderServiceTitleTextView.setText(myOrderItemResponse.getNameEN());
            myOrderDetailsServiceNameTextView.setText(myOrderItemResponse.getNameEN());
        }

        if(myOrderItemResponse.getLogo()!=null   ) {
            GlideApp.with(getBaseActivity()).load("http://ur-business.net/shift_new/"+myOrderItemResponse.getLogo()).error(R.drawable.care_care).into(myOrderCompanyIconImageView);
        }

        myOrderDetailsDateEditText.setText(myOrderItemResponse.getDate());
        myOrderDetailsServicePriceTextView.setText(myOrderItemResponse.getPrice());
        myOrderDetailsTaxesTextView.setText("12%");
        int finalPrice=Integer.valueOf(myOrderItemResponse.getPrice())+((12*Integer.valueOf(myOrderItemResponse.getPrice()))/100);

        myOrderDetailsFinalPriceTextView.setText(String.valueOf(finalPrice));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        myOrderDetailsPresenter.onDetach();
        

    }



    

    @OnClick(R.id.my_order_detail_save_button_container)
    public void onViewClicked() {
        ((MainActivity)getBaseActivity()).showRateFragment(myOrderItemResponse);
    }
}
