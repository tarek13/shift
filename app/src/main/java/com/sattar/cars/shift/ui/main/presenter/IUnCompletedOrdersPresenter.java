package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IUnCompletedOrdersInteractor;
import com.sattar.cars.shift.ui.main.view.UnCompletedOrdersView;

@PerActivity
public interface IUnCompletedOrdersPresenter<V extends UnCompletedOrdersView,I extends IUnCompletedOrdersInteractor> extends IBasePresenter<V,I> {
    void loadUnCompletedOrders(String lang, String clintId);
}

