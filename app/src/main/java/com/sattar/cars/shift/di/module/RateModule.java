package com.sattar.cars.shift.di.module;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.IRateInteractor;
import com.sattar.cars.shift.ui.main.interactor.RateInteractor;
import com.sattar.cars.shift.ui.main.presenter.IRatePresenter;
import com.sattar.cars.shift.ui.main.presenter.RatePresenter;
import com.sattar.cars.shift.ui.main.view.RateView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class RateModule {

    @Provides
    @PerActivity
    IRatePresenter<RateView, IRateInteractor> provideRatePresenter(RatePresenter<RateView, IRateInteractor> companieDetailsPresenter){
        return companieDetailsPresenter;
    }

    @Provides
    @PerActivity
    IRateInteractor provideRateInteractor (RateInteractor eventDetailsInteractor) {
        return  eventDetailsInteractor;
    }

}
