package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface IServiceDetailsInteractor extends IBaseInteractor {
    Observable<AvaliableTimeResponse> getAvaliableTime(HashMap<String, String> lang);

    Observable<GeneralResponse> makerServiceOrder(HashMap<String, String> options);

    Observable<ServiceDetailsResponse> getServiceDetails(HashMap<String, String> options);
}
