package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.data.network.model.SearchBodyRequest;
import com.sattar.cars.shift.di.component.DaggerFinishedOrdersComponent;
import com.sattar.cars.shift.di.component.FinishedOrdersComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.FinishedOrdersModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.MyOrdersListAdapter;
import com.sattar.cars.shift.ui.main.interactor.IFinishedOrdersInteractor;
import com.sattar.cars.shift.ui.main.presenter.IFinishedOrdersPresenter;
import com.sattar.cars.shift.ui.main.view.FinishedOrdersView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FinishedOrdersFragment extends BaseFragment implements FinishedOrdersView, MyOrdersListAdapter.Callback {
    public static final String TAG = "CompaniesListFragment";

    @BindView(R.id.my_orders_list_recycler_view)
    RecyclerView finishedOrdersRecyclerView;

    @BindView(R.id.my_orders_list_error)
    CustomTextView finishedOrdersError;

    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;

    @BindView(R.id.my_orders_list_continer)
    CardView finishedOrdersContiner;



    @Inject
    IFinishedOrdersPresenter<FinishedOrdersView, IFinishedOrdersInteractor> finishedOrdersPresenter;

    @Inject
    MyOrdersListAdapter finishedOrdersAdapter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    FinishedOrdersComponent finishedOrdersComponent;

    int comeFrom;


    private SearchBodyRequest searchRequestBody;
    private DepartmentResponse.Data data;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        FinishedOrdersFragment fragment = new FinishedOrdersFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setComeFrom(int comeFrom, DepartmentResponse.Data data) {
        this.comeFrom = comeFrom;
        this.data=data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_orders_list, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        finishedOrdersComponent = DaggerFinishedOrdersComponent.builder()
                .finishedOrdersModule(new FinishedOrdersModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        finishedOrdersComponent.inject(this);

        finishedOrdersPresenter.onAttach(this);
        //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        ((MainActivity)getBaseActivity()).getSupportActionBar().setTitle(R.string.menu_my_orders_title);

        finishedOrdersRecyclerView.setLayoutManager(linearLayoutManager);
        finishedOrdersRecyclerView.setAdapter(finishedOrdersAdapter);
        finishedOrdersAdapter.setCallback(this);
        finishedOrdersRecyclerView.setItemAnimator(new DefaultItemAnimator());
        finishedOrdersPresenter.loadFinishedOrders(getString(R.string.language),
                finishedOrdersPresenter.getInteractor().getPreferencesHelper().getCurrentUser().getId());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        finishedOrdersPresenter.onDetach();
    }


    @Override
    public void showFinishedOrdersList(ArrayList<MyOrderItemResponse> myOrderItemResponseArrayList) {
        if (myOrderItemResponseArrayList != null && myOrderItemResponseArrayList.size() > 0) {
            finishedOrdersRecyclerView.setVisibility(View.VISIBLE);
            finishedOrdersError.setVisibility(View.GONE);
            finishedOrdersAdapter.setData(myOrderItemResponseArrayList);
        } else {
            showErrorMessage(R.string.no_orders_found, R.drawable.not_found);
        }


    }


    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            finishedOrdersContiner.setVisibility(View.GONE);

        }

    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            finishedOrdersContiner.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
        finishedOrdersContiner.setVisibility(View.GONE);
        finishedOrdersError.setVisibility(View.VISIBLE);
        finishedOrdersError.setText(messageID);
        finishedOrdersError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void  onDataItemClick(int position, MyOrderItemResponse myOrderItemResponse) {
         ((MainActivity)getBaseActivity()).showMyOrderDetailsFragment(myOrderItemResponse);
    }

    public void setSearchRequestBody(SearchBodyRequest searchRequestBody) {
        this.searchRequestBody = searchRequestBody;
    }

    public SearchBodyRequest getSearchRequestBody() {
        return searchRequestBody;
    }

    public DepartmentResponse.Data getData() {
        return data;
    }
}
