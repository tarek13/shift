package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.HomeModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.HomeFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {HomeModule.class})
public interface HomeComponent {

    void inject(HomeFragment homeFragment);
}
