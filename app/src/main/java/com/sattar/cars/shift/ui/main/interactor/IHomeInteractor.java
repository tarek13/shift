package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.BannerResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public interface IHomeInteractor extends IBaseInteractor {

    Observable<DepartmentResponse> getDepartmentsData(Map<String, String> lang);

    Observable<BannerResponse> getBannersData(HashMap<String, String> options);
}
