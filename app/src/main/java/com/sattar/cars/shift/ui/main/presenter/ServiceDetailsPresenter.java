package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IServiceDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.ServiceDetailsView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class ServiceDetailsPresenter<V extends ServiceDetailsView,I extends IServiceDetailsInteractor>
        extends BasePresenter<V,I> implements IServiceDetailsPresenter<V,I> {
    private static final String TAG = "Serviceslist";

    @Inject
    public ServiceDetailsPresenter(I mvpInteractor,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getAvaliableTime(String lang, String selectedDate) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("date", selectedDate);;
        options.put("lang", lang);

        getCompositeDisposable().add(getInteractor().getAvaliableTime(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<AvaliableTimeResponse>() {
                    @Override
                    public void accept(AvaliableTimeResponse avaliableTimeResponse) {
                        getMvpView().hideLoading();

                        if(avaliableTimeResponse.getStatus()) {
                           getMvpView().showAvaliableTime(avaliableTimeResponse.getData());
                        }else {
                            getMvpView().onError(avaliableTimeResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoading();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                              getMvpView().onError(R.string.some_error);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.connection_error);

                        }
                    }
                }));
    }

    @Override
    public void getMakeOrder(String lang, String selectedDate, String selectedTime, String orderId, String clintId) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("date", selectedDate);;
        options.put("lang", lang);
        options.put("time_from", selectedTime);
        options.put("order_num", orderId);
        options.put("client_id", clintId);

        getCompositeDisposable().add(getInteractor().makerServiceOrder(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<GeneralResponse>() {
                    @Override
                    public void accept(GeneralResponse generalResponse) {
                        getMvpView().hideLoading();

                        if(generalResponse.getStatus()) {
                           getMvpView().showMessage(generalResponse.getMessage(),1);
                        }else {
                            getMvpView().onError(generalResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoading();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                              getMvpView().onError(R.string.some_error);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.connection_error);

                        }
                    }
                }));
    }

    @Override
    public void getServiceDetails(String lang, String serviceId, String companyId) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("service_id", serviceId);;
        options.put("lang", lang);
        options.put("company_id", companyId);


        getCompositeDisposable().add(getInteractor().getServiceDetails(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<ServiceDetailsResponse>() {
                    @Override
                    public void accept(ServiceDetailsResponse serviceDetailsResponse) {
                        getMvpView().hideLoading();

                        if(serviceDetailsResponse.getStatus()) {
                            getMvpView().showServiceDetails(serviceDetailsResponse.getData());
                        }else {
                            getMvpView().onError(serviceDetailsResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoading();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().onError(R.string.some_error);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.connection_error);

                        }
                    }
                }));
    }


    @Override
    public boolean validateUserData(String selectedWashType, String selectedCarSize, String date, String time, String selectedDuration) {

        if(selectedWashType==null || selectedWashType.isEmpty()){
            getMvpView().onError(R.string.wash_type_error);
            return false;
        }
        else if(selectedCarSize==null || selectedCarSize.isEmpty()){
            getMvpView().onError(R.string.car_size_error);
            return false;
        }
        else if (date == null || date.trim().isEmpty() || date.trim().length() <= 0) {
            getMvpView().onError(R.string.date_error);
            return false;
        } else if (time == null || time.trim().isEmpty() || time.trim().length() <= 0) {
            getMvpView().onError(R.string.time_error_1);
            return false;
        }else if (selectedDuration == null || selectedDuration.trim().isEmpty() || selectedDuration.trim().length() <= 0) {
            getMvpView().onError(R.string.subscription_error);
            return false;
        }


        return true;
    }
}
