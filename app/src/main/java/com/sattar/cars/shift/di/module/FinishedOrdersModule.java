package com.sattar.cars.shift.di.module;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sattar.cars.shift.di.qualifier.ActivityContext;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.adapter.MyOrdersListAdapter;
import com.sattar.cars.shift.ui.main.interactor.IFinishedOrdersInteractor;
import com.sattar.cars.shift.ui.main.interactor.FinishedOrdersInteractor;
import com.sattar.cars.shift.ui.main.presenter.IFinishedOrdersPresenter;
import com.sattar.cars.shift.ui.main.presenter.FinishedOrdersPresenter;
import com.sattar.cars.shift.ui.main.view.FinishedOrdersView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class FinishedOrdersModule {

    @Provides
    @PerActivity
    IFinishedOrdersPresenter<FinishedOrdersView, IFinishedOrdersInteractor> provideFinishedOrdersPresenter(FinishedOrdersPresenter<FinishedOrdersView, IFinishedOrdersInteractor> companiesListPresenter){
        return companiesListPresenter;
    }

    @Provides
    @PerActivity
    IFinishedOrdersInteractor provideFinishedOrdersInteractor (FinishedOrdersInteractor eventsListInteractor) {
        return  eventsListInteractor;
    }

    @Provides
    @PerActivity
    MyOrdersListAdapter provideFinishedOrdersAdapter(@ActivityContext Context context){
        return new MyOrdersListAdapter(context);
    }

    @Provides
    @PerActivity
    LinearLayoutManager provideLinearManagerFinishedOrders(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }
}
