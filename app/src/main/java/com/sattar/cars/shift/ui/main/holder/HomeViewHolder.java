package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.ui.main.adapter.HomeAdapter;
import com.sattar.cars.shift.utils.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.department_icon_image_view)
    ImageView departmentIconImageView;
    @BindView(R.id.department_title_image_view)
    CustomTextView departmentTitleImageView;

    private HomeAdapter.Callback callback;
    private Context context;
    private int position;
    private DepartmentResponse.Data data;

    public HomeViewHolder(View itemView, Context context, HomeAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;
        itemView.setOnClickListener(this);
    }
    public void setData(DepartmentResponse.Data data, int position) {
        this.data = data;
        this.position=position;
        setup();
    }

    private void setup() {
        departmentTitleImageView.setText(data.getName());
        if(Integer.valueOf(data.getId())==1){
            departmentIconImageView.setImageResource(R.drawable.care_care);
        }
        else if(Integer.valueOf(data.getId())==2){
            departmentIconImageView.setImageResource(R.drawable.premium_badge);
        }else if(Integer.valueOf(data.getId())==3){
            departmentIconImageView.setImageResource(R.drawable.car_wash);
        }else if(Integer.valueOf(data.getId())==4){
            departmentIconImageView.setImageResource(R.drawable.car_shine);
        }


    }

    @Override
    public void onClick(View v) {
        callback.onHomeClick(position, data);
    }

}
