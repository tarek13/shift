package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface SubscribeDetailsView extends IBaseView {


    void showAvaliableTime(ArrayList<String> data);
}
