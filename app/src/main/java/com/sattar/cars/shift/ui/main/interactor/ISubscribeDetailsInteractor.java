package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.data.network.model.SubscribesResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface ISubscribeDetailsInteractor extends IBaseInteractor {
    Observable<AvaliableTimeResponse> getAvaliableTime(HashMap<String, String> lang);

    Observable<GeneralResponse> makerSubscribeOrder(HashMap<String, String> options);
}
