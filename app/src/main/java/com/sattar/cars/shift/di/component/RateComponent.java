package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.RateModule;
import com.sattar.cars.shift.di.module.SubscribeDetailsModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.RateFragment;
import com.sattar.cars.shift.ui.main.fragment.SubscribeDetailsFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {RateModule.class})
public interface RateComponent {

    void inject(RateFragment rateFragment);
}
