package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.AvaliableTimeResponse;
import com.sattar.cars.shift.data.network.model.GeneralResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IMyOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.MyOrderDetailsView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class MyOrderDetailsPresenter<V extends MyOrderDetailsView,I extends IMyOrderDetailsInteractor>
        extends BasePresenter<V,I> implements IMyOrderDetailsPresenter<V,I> {
    private static final String TAG = "MyOrderslist";

    @Inject
    public MyOrderDetailsPresenter(I mvpInteractor,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }




    @Override
    public void rateOrder(String lang, String rate, String companyId, String comment, String clintId) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("rate", rate);;
        options.put("lang", lang);
        options.put("company_id", companyId);
        options.put("comment", comment);
        options.put("client_id", clintId);

        getCompositeDisposable().add(getInteractor().rateOrder(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<GeneralResponse>() {
                    @Override
                    public void accept(GeneralResponse generalResponse) {
                        getMvpView().hideLoading();

                        if(generalResponse.getStatus()) {
                           getMvpView().showMessage(generalResponse.getMessage(),1);
                        }else {
                            getMvpView().onError(generalResponse.getMessage());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoading();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                              getMvpView().onError(R.string.some_error);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.connection_error);

                        }
                    }
                }));
    }

}
