package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface ServiceDetailsView extends IBaseView {


    void showAvaliableTime(ArrayList<String> data);

    void showServiceDetails(ServiceDetailsResponse.Data data);
}
