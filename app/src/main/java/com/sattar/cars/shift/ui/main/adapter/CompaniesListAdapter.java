package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.ui.main.holder.CompanyItemViewHolder;

import java.util.ArrayList;

import javax.inject.Inject;

public class CompaniesListAdapter extends RecyclerView.Adapter<CompanyItemViewHolder> {


    // View Types
    private ArrayList<CompaniesItemResponse> companiesItemResponseArrayList = new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public CompaniesListAdapter(Context context) {
        this.context = context;
    }

    public interface Callback {
        void onCompanyItemClick(int position, CompaniesItemResponse companiesItemResponse);
    }

    public void setData(ArrayList<CompaniesItemResponse> companiesItemResponses) {
        this.companiesItemResponseArrayList = companiesItemResponses;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public CompanyItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CompanyItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_company_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyItemViewHolder holder, int position) {
        holder.setCompany(companiesItemResponseArrayList.get(position), position);
    }


    @Override
    public int getItemCount() {
        return companiesItemResponseArrayList.size();
    }
}
