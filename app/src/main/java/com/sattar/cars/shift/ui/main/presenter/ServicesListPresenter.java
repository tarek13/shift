package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.ServicesResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IServicesListInteractor;
import com.sattar.cars.shift.ui.main.view.ServicesListView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class ServicesListPresenter<V extends ServicesListView,I extends IServicesListInteractor>
        extends BasePresenter<V,I> implements IServicesListPresenter<V,I> {
    private static final String TAG = "Serviceslist";

    @Inject
    public ServicesListPresenter(I mvpInteractor,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void loadAllServices(String lang,String companyId) {
        getMvpView().showLoadingProgressbar();
        HashMap<String,String> options=new HashMap<>();
        options.put("company_id", companyId);
        options.put("lang", lang);

        getCompositeDisposable().add(getInteractor().getAllServices(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<ServicesResponse>() {
                    @Override
                    public void accept(ServicesResponse eventsResponse) {
                        getMvpView().hideLoadingProgressbar();

                        if(eventsResponse.getStatus()) {
                            getMvpView().showServicesList(eventsResponse.getData());
                        }else {
                            getMvpView().showErrorMessage(R.string.no_services_found, R.drawable.not_found);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoadingProgressbar();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                            }
                        }else {
                            getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);

                        }
                    }
                }));
    }

}
