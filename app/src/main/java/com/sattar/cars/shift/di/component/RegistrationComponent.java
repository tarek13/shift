package com.sattar.cars.shift.di.component;

import com.sattar.cars.shift.di.module.RegistrationModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.register.RegistrationActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = {RegistrationModule.class})
public interface RegistrationComponent {
    void inject(RegistrationActivity registrationActivity);
}
