package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.ISearchMapInteractor;
import com.sattar.cars.shift.ui.main.view.SearchMapView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class SearchMapPresenter<V extends SearchMapView,I extends ISearchMapInteractor>
        extends BasePresenter<V,I> implements ISearchMapPresenter<V,I> {

    private int totalPages;
    private int currentPage;
    private boolean isLastPage;

    @Inject
    public SearchMapPresenter(I mvpInteractor,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }
    @Override
    public void loadSearchByLocationOffers(HashMap<String,String> options) {
       //getMvpView().showLoadingProgressbar();


        getCompositeDisposable().add(getInteractor().searchByCampanies(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<CompaniesResponse>() {
                    @Override
                    public void accept(CompaniesResponse searchByMapResponse) {
                      //  getMvpView().hideLoadingProgressbar();
                        // getMvpView().showListOfSpecificCategory((SearchForCampaignsResponse) searchForCampaignsResponse.getCampaignsListItemResponses());
                        getMvpView().showStoresInMap( searchByMapResponse.getData());


                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                    //    getMvpView().hideLoadingProgressbar();
                        String userId="not login";
                        if(getInteractor().getPreferencesHelper().getCurrentUser()!=null){
                            userId= String.valueOf(getInteractor().getPreferencesHelper().getCurrentUser().getId());
                        }
                        if (!getInteractor().checkNetworkConnection()) {
                            getMvpView().onError(R.string.error_msg_no_internet);

                        } else if (throwable instanceof TimeoutException) {
                            getMvpView().onError(R.string.error_msg_timeout);

                        } else {
                            if (throwable instanceof HttpException) {
                                getMvpView().onError(((HttpException) throwable).message());

                                if (((HttpException) throwable).response().code() == 400) {
                                    // toDO handle error
                                    getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);

                                }
                                else if (((HttpException) throwable).response().code() == 500) {
                                    getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);

                                }
                                else if((((HttpException) throwable).response().code() == 401)){
                                    getMvpView().openActivityOnTokenExpire();
                                }
                                else {
                                    getMvpView().onError(R.string.connection_error);

                                }
                            } else {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);

                            }
                        }
                    }
                }));
    }


}
