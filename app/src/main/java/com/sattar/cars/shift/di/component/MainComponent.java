package com.sattar.cars.shift.di.component;

import com.sattar.cars.shift.di.module.MainModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.MainActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {MainModule.class})
public interface MainComponent {

    void inject(MainActivity mainActivity);
}
