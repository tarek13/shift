package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.ServicesItemResponse;
import com.sattar.cars.shift.ui.main.adapter.ServicesListAdapter;
import com.sattar.cars.shift.utils.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.service_logo_image_view)
    ImageView serviceIconImageView;
    @BindView(R.id.service_title_text_view)
    CustomTextView serviceTitleImageView;
    @BindView(R.id.service_percentage_text_view)
    CustomTextView servicePercentageTextView;

    private ServicesListAdapter.Callback callback;
    private Context context;
    private int position;
    private ServicesItemResponse servicesItemResponse;

    public ServiceItemViewHolder(View itemView, Context context, ServicesListAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;
        itemView.setOnClickListener(this);
    }
    public void setService(ServicesItemResponse servicesItemResponse, int position) {
        this.servicesItemResponse = servicesItemResponse;
        this.position=position;
        setup();
    }

    private void setup() {
        serviceTitleImageView.setText(servicesItemResponse.getName());
        
        if(servicesItemResponse.getImage()!=null   ) {
            GlideApp.with(context).load("http://ur-business.net/shift_new/"+servicesItemResponse.getImage()).error(R.drawable.care_care).into(serviceIconImageView);
        }
        if(servicesItemResponse.getDiscount()!=null && Integer.valueOf(servicesItemResponse.getDiscount())!=0){
            servicePercentageTextView.setText(servicesItemResponse.getDiscount());
        }

    }

    @Override
    public void onClick(View v) {
        callback.onServiceItemClick(position, servicesItemResponse);
    }

}
