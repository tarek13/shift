package com.sattar.cars.shift.ui.main.holder;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.ui.main.adapter.CompaniesListAdapter;
import com.sattar.cars.shift.utils.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompanyItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.company_icon_image_view)
    ImageView companyIconImageView;
    @BindView(R.id.company_title_image_view)
    CustomTextView companyTitleImageView;
    @BindView(R.id.company_rate_rating_bar)
    RatingBar companyRateRatingBar;

    private CompaniesListAdapter.Callback callback;
    private Context context;
    private int position;
    private CompaniesItemResponse companiesItemResponse;

    public CompanyItemViewHolder(View itemView, Context context, CompaniesListAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;
        itemView.setOnClickListener(this);
    }
    public void setCompany(CompaniesItemResponse companiesItemResponse, int position) {
        this.companiesItemResponse = companiesItemResponse;
        this.position=position;
        setup();
    }

    private void setup() {
        companyRateRatingBar.setNumStars(5);
        LayerDrawable stars = (LayerDrawable) companyRateRatingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ContextCompat.getColor(context, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        companyTitleImageView.setText(companiesItemResponse.getName());
        if(companiesItemResponse.getRate()!=null) {
            companyRateRatingBar.setRating(Integer.valueOf(companiesItemResponse.getRate()));
        }
        if(companiesItemResponse.getLogo()!=null   ) {
            GlideApp.with(context).load("http://ur-business.net/shift_new/"+companiesItemResponse.getLogo()).error(R.drawable.care_care).into(companyIconImageView);
        }

    }

    @Override
    public void onClick(View v) {
        callback.onCompanyItemClick(position, companiesItemResponse);
    }

}
