package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServicesItemResponse {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discount3")
    @Expose
    private String discount3;
    @SerializedName("discount6")
    @Expose
    private String discount6;
    @SerializedName("discount12")
    @Expose
    private String discount12;
    @SerializedName("car_wash3")
    @Expose
    private String carWash3;
    @SerializedName("car_wash6")
    @Expose
    private String carWash6;
    @SerializedName("car_wash12")
    @Expose
    private String carWash12;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("descrbtion")
    @Expose
    private String descrbtion;
    @SerializedName("descrbtionAR")
    @Expose
    private String descrbtionAR;
    @SerializedName("descrbtionEN")
    @Expose
    private String descrbtionEN;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount3() {
        return discount3;
    }

    public void setDiscount3(String discount3) {
        this.discount3 = discount3;
    }

    public String getDiscount6() {
        return discount6;
    }

    public void setDiscount6(String discount6) {
        this.discount6 = discount6;
    }

    public String getDiscount12() {
        return discount12;
    }

    public void setDiscount12(String discount12) {
        this.discount12 = discount12;
    }

    public String getCarWash3() {
        return carWash3;
    }

    public void setCarWash3(String carWash3) {
        this.carWash3 = carWash3;
    }

    public String getCarWash6() {
        return carWash6;
    }

    public void setCarWash6(String carWash6) {
        this.carWash6 = carWash6;
    }

    public String getCarWash12() {
        return carWash12;
    }

    public void setCarWash12(String carWash12) {
        this.carWash12 = carWash12;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescrbtion() {
        return descrbtion;
    }

    public void setDescrbtion(String descrbtion) {
        this.descrbtion = descrbtion;
    }

    public String getDescrbtionAR() {
        return descrbtionAR;
    }

    public void setDescrbtionAR(String descrbtionAR) {
        this.descrbtionAR = descrbtionAR;
    }

    public String getDescrbtionEN() {
        return descrbtionEN;
    }

    public void setDescrbtionEN(String descrbtionEN) {
        this.descrbtionEN = descrbtionEN;
    }
}
