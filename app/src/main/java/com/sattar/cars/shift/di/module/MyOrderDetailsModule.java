package com.sattar.cars.shift.di.module;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.IMyOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.interactor.MyOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.presenter.IMyOrderDetailsPresenter;
import com.sattar.cars.shift.ui.main.presenter.MyOrderDetailsPresenter;
import com.sattar.cars.shift.ui.main.view.MyOrderDetailsView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class MyOrderDetailsModule {

    @Provides
    @PerActivity
    IMyOrderDetailsPresenter<MyOrderDetailsView, IMyOrderDetailsInteractor> provideMyOrderDetailsPresenter(MyOrderDetailsPresenter<MyOrderDetailsView, IMyOrderDetailsInteractor> companieDetailsPresenter){
        return companieDetailsPresenter;
    }

    @Provides
    @PerActivity
    IMyOrderDetailsInteractor provideMyOrderDetailsInteractor (MyOrderDetailsInteractor eventDetailsInteractor) {
        return  eventDetailsInteractor;
    }

}
