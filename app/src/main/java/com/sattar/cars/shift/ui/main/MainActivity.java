package com.sattar.cars.shift.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.CategoryInfo;
import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.MakeOrderRequestBody;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.di.component.DaggerMainComponent;
import com.sattar.cars.shift.di.component.MainComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.MainModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.BaseActivity;
import com.sattar.cars.shift.ui.login.LoginActivity;
import com.sattar.cars.shift.ui.main.adapter.MainMenuNavigationAdapter;
import com.sattar.cars.shift.ui.main.fragment.AboutUsFragment;
import com.sattar.cars.shift.ui.main.fragment.CompaniesListFragment;
import com.sattar.cars.shift.ui.main.fragment.EditUserFragment;
import com.sattar.cars.shift.ui.main.fragment.HomeFragment;
import com.sattar.cars.shift.ui.main.fragment.MakeOrderDetailsFragment;
import com.sattar.cars.shift.ui.main.fragment.MyOrderDetailsFragment;
import com.sattar.cars.shift.ui.main.fragment.MyOrdersFragment;
import com.sattar.cars.shift.ui.main.fragment.RateFragment;
import com.sattar.cars.shift.ui.main.fragment.SearchMapFragment;
import com.sattar.cars.shift.ui.main.fragment.ServiceDetailsFragment;
import com.sattar.cars.shift.ui.main.fragment.ServicesListFragment;
import com.sattar.cars.shift.ui.main.fragment.SubscribeDetailsFragment;
import com.sattar.cars.shift.ui.main.fragment.SubscribesListFragment;
import com.sattar.cars.shift.ui.main.interactor.IMainInteractor;
import com.sattar.cars.shift.ui.main.presenter.IMainPresenter;
import com.sattar.cars.shift.ui.main.view.MainView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.rx.RxBus;
import com.sattar.cars.shift.utils.views.CustomTextView;
import com.sattar.cars.shift.utils.views.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements MainView, MainMenuNavigationAdapter.Callback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.main_content_frame_layout)
    FrameLayout mainContentFrameLayout;

    @BindView(R.id.main_profile_image_view)
    CircleImageView mainProfileImageView;

    @BindView(R.id.main_name_text_view)
    CustomTextView mainNameTextView;

    @BindView(R.id.main_email_text_view)
    CustomTextView mainEmailTextView;


    @BindView(R.id.nav_view)
    NavigationView navView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;


    @BindView(R.id.main_nav_menu_recycler_view)
    RecyclerView mainNavMenuRecyclerView;

    @BindView(R.id.main_login_container)
    RelativeLayout mainLoginContainer;

    @BindView(R.id.main_account_settings_button_container)
    CustomTextView mainAccountSettingsButtonContainer;

    @Inject
    IMainPresenter<MainView, IMainInteractor> mainPresenter;

    @Inject
    MainMenuNavigationAdapter mainMenuNavigationAdapter;

    @Inject
    LinearLayoutManager mMainLayoutManager;


    @Inject
    DividerItemDecoration dividerItemDecoration;


    @Inject
    RxBus rxBus;

    MainComponent mainComponent;



    private ActionBarDrawerToggle mDrawerToggle;

    private boolean mToolBarNavigationListenerIsRegistered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUnBinder(ButterKnife.bind(this));
        mainComponent = DaggerMainComponent.builder()
                .mainModule(new MainModule())
                .applicationComponent(((ShiftApp) getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .activityModule(new ActivityModule(this))
                .build();


        mainComponent.inject(this);

        mainPresenter.onAttach(this);

        setUp();

    }






    @Override
    protected void setUp() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.great_minds_title);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        setupNavMenu();
    }

    private void setupNavMenu() {

        mainPresenter.loadUserProfileDataFromPrefs();

        mainNavMenuRecyclerView.setLayoutManager(mMainLayoutManager);
        mainNavMenuRecyclerView.setAdapter(mainMenuNavigationAdapter);
        mainNavMenuRecyclerView.addItemDecoration(dividerItemDecoration);
        mainMenuNavigationAdapter.setCallback(this);


        mainPresenter.loadMainMenuItem();
        showHomeFragment();

    }


    @Override
    public void onMainMenuItemClick(int position, CategoryInfo mainCategoryInfo, int lastIndex) {
        mainPresenter.onMainMenuItemClick(position, mainCategoryInfo, lastIndex);
    }

    @Override
    public void showMainMenuItem(ArrayList<CategoryInfo> mainCategoryInfoArrayList) {
        mainMenuNavigationAdapter.setMainMenuTitleListTitle(mainCategoryInfoArrayList);

    }

    @Override
    public void closeNavigationDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(Gravity.START);
        }
    }

    @Override
    public void lockDrawer() {
        if (drawerLayout != null)
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void unlockDrawer() {
        if (drawerLayout != null)
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }


    @Override
    public void showHomeFragment() {
        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout) != null && getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals("HomeFragment"))) {
            HomeFragment homeFragment = (HomeFragment) HomeFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack("HomeFragment")
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, homeFragment, "HomeFragment")
                    .commit();
        }

        showHamburgerIcon();

    }





    @Override
    public void showAboutUsFragment() {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(AboutUsFragment.TAG))) {
            AboutUsFragment aboutUsFragment = (AboutUsFragment) AboutUsFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(AboutUsFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, aboutUsFragment, AboutUsFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }
    @Override
    public void showMyOrdersFragment() {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(MyOrdersFragment.TAG))) {
            MyOrdersFragment myOrdersFragment = (MyOrdersFragment) MyOrdersFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(MyOrdersFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, myOrdersFragment, MyOrdersFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }

    @Override
    public void showCompaniesListFragment(int comeFrom,DepartmentResponse.Data data) {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(CompaniesListFragment.TAG))) {
            CompaniesListFragment companiesListFragment = (CompaniesListFragment) CompaniesListFragment.newInstance();
            companiesListFragment.setComeFrom(comeFrom,data);
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(CompaniesListFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, companiesListFragment, CompaniesListFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }

    @Override
    public void showServicesListFragment(int comeFrom, CompaniesItemResponse companiesItemResponse, DepartmentResponse.Data data) {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(ServicesListFragment.TAG))) {
            ServicesListFragment servicesListFragment = (ServicesListFragment) ServicesListFragment.newInstance();
            servicesListFragment.setComeFrom(comeFrom,companiesItemResponse,data);
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(ServicesListFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, servicesListFragment, ServicesListFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }

    @Override
    public void showSubscribeDetailsFragment(DepartmentResponse.Data departmentItemResponse, SubscribesItemResponse subscribesItemResponse) {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(SubscribeDetailsFragment.TAG))) {
            SubscribeDetailsFragment subscribeDetailsFragment = (SubscribeDetailsFragment) SubscribeDetailsFragment.newInstance();
            subscribeDetailsFragment.setComeFrom(departmentItemResponse,subscribesItemResponse);
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(SubscribeDetailsFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, subscribeDetailsFragment, SubscribeDetailsFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }
    @Override
    public void showServiceDetailsFragment(String serviceId, String companyId, String departmentId, String companyName) {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(SubscribeDetailsFragment.TAG))) {
            ServiceDetailsFragment serviceDetailsFragment = (ServiceDetailsFragment) ServiceDetailsFragment.newInstance();
            serviceDetailsFragment.setComeFrom(serviceId,companyId,departmentId,companyName);
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(ServiceDetailsFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, serviceDetailsFragment, ServiceDetailsFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }
    @Override
    public void showMyOrderDetailsFragment(MyOrderItemResponse myOrderItemResponse) {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(SubscribeDetailsFragment.TAG))) {
            MyOrderDetailsFragment myOrderDetailsFragment = (MyOrderDetailsFragment) MyOrderDetailsFragment.newInstance();
            myOrderDetailsFragment.setData(myOrderItemResponse);
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(MyOrderDetailsFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, myOrderDetailsFragment, MyOrderDetailsFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }

    @Override
    public void showMakeOrderDetailsFragment(ServiceDetailsResponse.Data serviceDetailsResponse, MakeOrderRequestBody makeOrderRequestBody) {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(SubscribeDetailsFragment.TAG))) {
            MakeOrderDetailsFragment makeOrderDetailsFragment = (MakeOrderDetailsFragment) MakeOrderDetailsFragment.newInstance();
            makeOrderDetailsFragment.setData(serviceDetailsResponse,makeOrderRequestBody);
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(MakeOrderDetailsFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, makeOrderDetailsFragment, MakeOrderDetailsFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }

    @Override
    public void showRateFragment(MyOrderItemResponse myOrderItemResponse) {

        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout).getTag().equals(SubscribeDetailsFragment.TAG))) {
            RateFragment rateFragment = (RateFragment) RateFragment.newInstance();
            rateFragment.setData(myOrderItemResponse);
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(RateFragment.TAG)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .add(R.id.main_content_frame_layout, rateFragment, RateFragment.TAG)
                    .commit();
        }

        removeHamburgerIcon(true);

    }










    @Override
    public void showEditProfileFragment() {

        EditUserFragment editUserFragment = (EditUserFragment) EditUserFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(EditUserFragment.TAG)
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.main_content_frame_layout, editUserFragment, EditUserFragment.TAG)
                .commit();

    }

    @Override
    public void showSearchMapFragment(DepartmentResponse.Data departmentItemResponse) {
        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout) instanceof SearchMapFragment)) {
            SearchMapFragment searchMapFragment = (SearchMapFragment) SearchMapFragment.newInstance();
            searchMapFragment.setData(departmentItemResponse);

            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(SearchMapFragment.TAG)
                    .add(R.id.main_content_frame_layout, searchMapFragment, SearchMapFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void showSubscribesListFragment(DepartmentResponse.Data departmentItemResponse) {
        if (!(getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout) instanceof SubscribesListFragment)) {
            SubscribesListFragment subscribesListFragment = (SubscribesListFragment) SubscribesListFragment.newInstance();
            subscribesListFragment.setData(departmentItemResponse);

            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(SearchMapFragment.TAG)
                    .add(R.id.main_content_frame_layout, subscribesListFragment, SubscribesListFragment.TAG)
                    .commit();
        }
        removeHamburgerIcon(true);
    }

    @Override
    public void fillUserDataFromPrefs(String name, String email) {
        mainLoginContainer.setVisibility(View.VISIBLE);
        mainNameTextView.setText(name);
        mainEmailTextView.setText(email);
    }


    @OnClick({/*R.id.main_setting_button*/})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            /*case R.id.main_setting_button:
                if (!mainMenuFlag) {
                    mainPresenter.loadMainMenuItem();
                   // mainSettingButton.setImageResource(R.drawable.ic_menu_home);
                    mainMenuFlag = true;

                } else {
                    mainPresenter.loadMainCategoryMenuFromCache();
                  //  mainSettingButton.setImageResource(R.drawable.settings);
                    mainMenuFlag = false;
                }
                break;*/

        }

    }

    @Override
    public void openLoginActivity() {
        Intent i = new Intent(MainActivity.this, LoginActivity.class);

        startActivity(i);
        finish();


    }




    @Override
    protected void onDestroy() {
        mainPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        System.err.println("ONBACK " + getSupportFragmentManager().getBackStackEntryCount());

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            System.err.println(".onBackPressed : condition true.");

            super.onBackPressed();
            finish();
        } else if (getCurrentFragment() instanceof HomeFragment) {
            super.onBackPressed();
            finish();
        }else if(getCurrentFragment() instanceof CompaniesListFragment && getPreviousFragment() instanceof SearchMapFragment){
            super.onBackPressed();
            showTitleOfPreviousFragment();
        }else if(getCurrentFragment() instanceof ServicesListFragment && getPreviousFragment() instanceof CompaniesListFragment){
            super.onBackPressed();
            showTitleOfPreviousFragment();
        }else if(getCurrentFragment() instanceof ServiceDetailsFragment && getPreviousFragment() instanceof ServicesListFragment){
            super.onBackPressed();
            showTitleOfPreviousFragment();
        }else if(getCurrentFragment() instanceof MakeOrderDetailsFragment && getPreviousFragment() instanceof ServiceDetailsFragment){
            super.onBackPressed();
            showTitleOfPreviousFragment();
        }else if(getCurrentFragment() instanceof SearchMapFragment){
            super.onBackPressed();
            showHomeFragment();
            getSupportActionBar().setTitle(R.string.menu_home_title);
        }
        else if(getCurrentFragment() instanceof MyOrderDetailsFragment && getPreviousFragment() instanceof MyOrdersFragment){
            super.onBackPressed();
        }else if(getCurrentFragment() instanceof RateFragment && getPreviousFragment() instanceof MyOrderDetailsFragment){
            super.onBackPressed();
        }
        else if(getCurrentFragment() instanceof SubscribeDetailsFragment && getPreviousFragment() instanceof SubscribesListFragment){
            super.onBackPressed();
        }
        /*else if (getCurrentFragment() instanceof EditUserFragment && getPreviousFragment() instanceof  SupportFragment) {
            super.onBackPressed();
        }*/
        else {
            /*Fragment curr = getCurrentFragment();
            // Show hamburger
            if (curr instanceof EntityOperationFragment)
                ((EntityOperationFragment) curr).onBackPress();
            else {

                if(curr instanceof BackUpSettingsFragment) {
                    super.onBackPressed();

                }else {
                    showHamburgerIcon();
                    super.onBackPressed();
                }
            }*/
            Fragment curr = getCurrentFragment();
            /*if (curr instanceof NewsDetailsFragment || curr instanceof EventDetailsFragment) {
                super.onBackPressed();
            }*/

            /*else {*/
           showHamburgerIcon();
            super.onBackPressed();
           // }
        }
    }

    public void showTitleOfPreviousFragment(){
        if(getCurrentFragment() instanceof HomeFragment){
            getSupportActionBar().setTitle(R.string.menu_home_title);
        }else if(getCurrentFragment() instanceof SearchMapFragment){
            getSupportActionBar().setTitle(getString(R.string.choose_place));
        }else if(getCurrentFragment() instanceof CompaniesListFragment){
            getSupportActionBar().setTitle(((CompaniesListFragment)getCurrentFragment()).getData().getName());
        }else if(getCurrentFragment() instanceof ServicesListFragment){
            getSupportActionBar().setTitle(((ServicesListFragment)getCurrentFragment()).getData().getName());
        }
    }
    public void removeHamburgerIcon(boolean flag) {
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        // Show back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(flag);
        // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
        // clicks are disabled i.e. the UP button will not work.
        // We need to add a listener, as in below, so DrawerToggle will forward
        // click events to this listener.
        if (!mToolBarNavigationListenerIsRegistered) {
            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Doesn't have to be onBackPressed
                    onBackPressed();
                }
            });

            mToolBarNavigationListenerIsRegistered = true;

        }
    }

    public void showHamburgerIcon() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        /*getSupportActionBar().setDisplayShowTitleEnabled(false);*/

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        // getSupportActionBar().setTitle(R.string.app_name);


        // Remove the/any drawer toggle listener
        mDrawerToggle.setToolbarNavigationClickListener(null);
        mToolBarNavigationListenerIsRegistered = false;
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.main_content_frame_layout);
    }

    public Fragment getPreviousFragment() {
        List<Fragment> fragments=getSupportFragmentManager().getFragments();
        return getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size()-2);

    }
    @OnClick(R.id.main_account_settings_button_container)
    public void onViewClicked() {
        showEditProfileFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
