package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.MyOrderDetailsModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.MyOrderDetailsFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {MyOrderDetailsModule.class})
public interface MyOrderDetailsComponent {

    void inject(MyOrderDetailsFragment myOrderDetailsFragment);
}
