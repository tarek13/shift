package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.data.network.model.SearchBodyRequest;
import com.sattar.cars.shift.di.component.DaggerUnCompletedOrdersComponent;
import com.sattar.cars.shift.di.component.UnCompletedOrdersComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.di.module.UnCompletedOrdersModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.MyOrdersListAdapter;
import com.sattar.cars.shift.ui.main.interactor.IUnCompletedOrdersInteractor;
import com.sattar.cars.shift.ui.main.presenter.IUnCompletedOrdersPresenter;
import com.sattar.cars.shift.ui.main.view.UnCompletedOrdersView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UnCompletedOrdersFragment extends BaseFragment implements UnCompletedOrdersView, MyOrdersListAdapter.Callback {
    public static final String TAG = "CompaniesListFragment";

    @BindView(R.id.my_orders_list_recycler_view)
    RecyclerView uncompletedOrdersRecyclerView;

    @BindView(R.id.my_orders_list_error)
    CustomTextView uncompletedOrdersError;

    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;

    @BindView(R.id.my_orders_list_continer)
    CardView uncompletedOrdersContiner;



    @Inject
    IUnCompletedOrdersPresenter<UnCompletedOrdersView, IUnCompletedOrdersInteractor> uncompletedOrdersPresenter;

    @Inject
    MyOrdersListAdapter uncompletedOrdersAdapter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    UnCompletedOrdersComponent uncompletedOrdersComponent;

    int comeFrom;


    private SearchBodyRequest searchRequestBody;
    private DepartmentResponse.Data data;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        UnCompletedOrdersFragment fragment = new UnCompletedOrdersFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setComeFrom(int comeFrom, DepartmentResponse.Data data) {
        this.comeFrom = comeFrom;
        this.data=data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_orders_list, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        uncompletedOrdersComponent = DaggerUnCompletedOrdersComponent.builder()
                .unCompletedOrdersModule(new UnCompletedOrdersModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        uncompletedOrdersComponent.inject(this);

        uncompletedOrdersPresenter.onAttach(this);
        //   setShowMenu(false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        ((MainActivity)getBaseActivity()).getSupportActionBar().setTitle(R.string.menu_my_orders_title);

        uncompletedOrdersRecyclerView.setLayoutManager(linearLayoutManager);
        uncompletedOrdersRecyclerView.setAdapter(uncompletedOrdersAdapter);
        uncompletedOrdersAdapter.setCallback(this);
        uncompletedOrdersRecyclerView.setItemAnimator(new DefaultItemAnimator());
        uncompletedOrdersPresenter.loadUnCompletedOrders(getString(R.string.language),
                uncompletedOrdersPresenter.getInteractor().getPreferencesHelper().getCurrentUser().getId());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        uncompletedOrdersPresenter.onDetach();
    }


    @Override
    public void showUnCompletedOrdersList(ArrayList<MyOrderItemResponse> myOrderItemResponseArrayList) {
        if (myOrderItemResponseArrayList != null && myOrderItemResponseArrayList.size() > 0) {
            uncompletedOrdersRecyclerView.setVisibility(View.VISIBLE);
            uncompletedOrdersError.setVisibility(View.GONE);
            uncompletedOrdersAdapter.setData(myOrderItemResponseArrayList);
        } else {
            showErrorMessage(R.string.no_orders_found, R.drawable.not_found);
        }


    }


    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            uncompletedOrdersContiner.setVisibility(View.GONE);

        }

    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            uncompletedOrdersContiner.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
        uncompletedOrdersContiner.setVisibility(View.GONE);
        uncompletedOrdersError.setVisibility(View.VISIBLE);
        uncompletedOrdersError.setText(messageID);
        uncompletedOrdersError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void  onDataItemClick(int position, MyOrderItemResponse myOrderItemResponse) {
        ((MainActivity)getBaseActivity()).showMyOrderDetailsFragment(myOrderItemResponse);
    }

    public void setSearchRequestBody(SearchBodyRequest searchRequestBody) {
        this.searchRequestBody = searchRequestBody;
    }

    public SearchBodyRequest getSearchRequestBody() {
        return searchRequestBody;
    }

    public DepartmentResponse.Data getData() {
        return data;
    }
}
