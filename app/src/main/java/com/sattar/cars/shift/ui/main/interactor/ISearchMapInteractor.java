package com.sattar.cars.shift.ui.main.interactor;

import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.Map;

import io.reactivex.Observable;

public interface ISearchMapInteractor extends IBaseInteractor {
    Observable<CompaniesResponse> searchByCampanies(Map<String, String> options);

    boolean checkNetworkConnection();
}
