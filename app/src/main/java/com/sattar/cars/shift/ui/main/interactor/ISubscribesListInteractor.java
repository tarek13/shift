package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.SubscribesResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;

import io.reactivex.Observable;

public interface ISubscribesListInteractor extends IBaseInteractor {
    Observable<SubscribesResponse> getAllSubscribes(HashMap<String, String> lang);

}
