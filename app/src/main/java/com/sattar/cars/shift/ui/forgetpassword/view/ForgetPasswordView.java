package com.sattar.cars.shift.ui.forgetpassword.view;

import com.sattar.cars.shift.ui.base.view.IBaseView;

public interface ForgetPasswordView extends IBaseView {

    void  returnToLoginPage(String message);

    void onEmailError(int emailError);
}
