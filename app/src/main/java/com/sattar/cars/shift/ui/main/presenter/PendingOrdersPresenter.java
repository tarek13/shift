package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.data.network.model.MyOrdersResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IPendingOrdersInteractor;
import com.sattar.cars.shift.ui.main.view.PendingOrdersView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class PendingOrdersPresenter<V extends PendingOrdersView,I extends IPendingOrdersInteractor>
        extends BasePresenter<V,I> implements IPendingOrdersPresenter<V,I> {
    private static final String TAG = "Companieslist";

    @Inject
    public PendingOrdersPresenter(I mvpInteractor,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void loadPendingOrders(String lang, String clintId) {
        getMvpView().showLoadingProgressbar();
        HashMap<String,String> options=new HashMap<>();
        options.put("client_id", clintId);
        options.put("lang", lang);

        getCompositeDisposable().add(getInteractor().getPendingOrders(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<MyOrdersResponse>() {
                    @Override
                    public void accept(MyOrdersResponse eventsResponse) {
                        getMvpView().hideLoadingProgressbar();

                        if(eventsResponse.getStatus()) {
                            getMvpView().showPendingOrdersList(eventsResponse.getData().getPending());
                        }else {
                            getMvpView().showErrorMessage(R.string.no_orders_found, R.drawable.not_found);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoadingProgressbar();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                            }
                        }else {
                            getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);

                        }
                    }
                }));
    }

}
