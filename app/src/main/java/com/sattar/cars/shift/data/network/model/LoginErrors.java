package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginErrors {

    @SerializedName("email")
    @Expose
    private List<String> email = null;

    @SerializedName("not_verified")
    @Expose
    private List<String> notVerified = null;

    @SerializedName("error")
    @Expose
    private List<String> error = null;

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }


    public List<String> getNotVerified() {
        return notVerified;
    }

    public void setNotVerified(List<String> notVerified) {
        this.notVerified = notVerified;
    }



    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }
}
