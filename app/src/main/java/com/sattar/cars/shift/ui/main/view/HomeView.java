package com.sattar.cars.shift.ui.main.view;



import com.sattar.cars.shift.data.network.model.BannerItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface HomeView extends IBaseView {

    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);

    void showHomeData(ArrayList<DepartmentResponse.Data> homeData);


    void showBannerData(ArrayList<BannerItemResponse> data);

    void showSelectedBanner(int position);
}
