package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.CompaniesResponse;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.data.network.model.MyOrdersResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;

import io.reactivex.Observable;

public interface IPendingOrdersInteractor extends IBaseInteractor {
    Observable<MyOrdersResponse> getPendingOrders(HashMap<String, String> lang);

}
