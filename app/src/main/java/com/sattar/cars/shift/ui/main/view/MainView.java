package com.sattar.cars.shift.ui.main.view;

import com.sattar.cars.shift.data.db.model.CategoryInfo;
import com.sattar.cars.shift.data.network.model.CompaniesItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.data.network.model.MakeOrderRequestBody;
import com.sattar.cars.shift.data.network.model.MyOrderItemResponse;
import com.sattar.cars.shift.data.network.model.ServiceDetailsResponse;
import com.sattar.cars.shift.data.network.model.SubscribesItemResponse;
import com.sattar.cars.shift.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface MainView extends IBaseView {

    void showMainMenuItem(ArrayList<CategoryInfo> mainMenuList);
    void closeNavigationDrawer();
    void lockDrawer();
    void unlockDrawer();

    void showHomeFragment();






    void showAboutUsFragment();


    void showMyOrdersFragment();

    void showCompaniesListFragment(int comeFrom, DepartmentResponse.Data data);

    void showServicesListFragment(int comeFrom, CompaniesItemResponse companiesItemResponse, DepartmentResponse.Data data);

    void showSubscribeDetailsFragment(DepartmentResponse.Data departmentItemResponse, SubscribesItemResponse subscribesItemResponse);

    void showServiceDetailsFragment(String serviceId, String companyId, String departmentId, String companyName);

    void showMyOrderDetailsFragment(MyOrderItemResponse myOrderItemResponse);

    void showMakeOrderDetailsFragment(ServiceDetailsResponse.Data serviceDetailsResponse, MakeOrderRequestBody makeOrderRequestBody);

    void showRateFragment(MyOrderItemResponse myOrderItemResponse);

    void showEditProfileFragment();

    void showSearchMapFragment(DepartmentResponse.Data homeData);

    void showSubscribesListFragment(DepartmentResponse.Data departmentItemResponse);

    void fillUserDataFromPrefs(String name, String email);



    void openLoginActivity();





}
