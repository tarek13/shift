package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.login.interactor.ILoginInteractor;
import com.sattar.cars.shift.ui.login.interactor.LoginInteractor;
import com.sattar.cars.shift.ui.login.presenter.ILoginPresenter;
import com.sattar.cars.shift.ui.login.presenter.LoginPresenter;
import com.sattar.cars.shift.ui.login.view.LoginView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class LoginModule {

    @Provides
    @PerActivity
    ILoginPresenter<LoginView,ILoginInteractor> provideLoginPresenter (LoginPresenter<LoginView , ILoginInteractor> loginPresenter){
        return  loginPresenter;
    }

    @Provides
    @PerActivity
    ILoginInteractor provideLoginInteractor (LoginInteractor loginInteractor) {
        return  loginInteractor;
    }

}
