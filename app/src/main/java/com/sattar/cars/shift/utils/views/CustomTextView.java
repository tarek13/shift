package com.sattar.cars.shift.utils.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.utils.AppConstants;

public class CustomTextView extends android.support.v7.widget.AppCompatTextView {
    private Context context;

    public CustomTextView(Context context) {
        super(context);

        this.context = context;
        if (getResources().getString(R.string.language).equals(AppConstants.ARABIC)) {

            System.err.println("text1: " + getTag());
            if (getTag() != null && getTag().equals("sans-serif-medium")) {
                applyCustomFont(context, "tajawal_medium.ttf");
            } else if (getTag() != null && getTag().equals("sans-serif-light")) {
                applyCustomFont(context, "tajawal_light.ttf");
            } else {
                applyCustomFont(context, "tajawal_regular.ttf");
            }
        }
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        if (getResources().getString(R.string.language).equals(AppConstants.ARABIC)) {
            System.err.println("text2: " + getTag());
            if (getTag() != null && getTag().equals("sans-serif-medium")) {
                applyCustomFont(context, "tajawal_medium.ttf");
            } else if (getTag() != null && getTag().equals("sans-serif-light")) {
                applyCustomFont(context, "tajawal_light.ttf");
            } else {
                applyCustomFont(context, "tajawal_regular.ttf");
            }
        }
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;
        if (getResources().getString(R.string.language).equals(AppConstants.ARABIC)) {

            System.err.println("text3: " + getTag());
            if (getTag() != null && getTag().equals("sans-serif-medium")) {
                applyCustomFont(context, "tajawal_medium.ttf");
            } else if (getTag() != null && getTag().equals("sans-serif-light")) {
                applyCustomFont(context, "tajawal_light.ttf");
            } else {
                applyCustomFont(context, "tajawal_regular.ttf");
            }
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (getResources().getString(R.string.language).equals(AppConstants.ARABIC) && !type.equals(BufferType.SPANNABLE)) {
            super.setText(replaceArabicNumbers(text), type);
        } else {
            super.setText(text, type);
        }

    }

    @Override
    public Object getTag() {
        return super.getTag();
    }

    private void applyCustomFont(Context context, String font) {
        Typeface customFont = FontCache.getTypeface(font, context);
        setTypeface(customFont);
    }

    private String replaceArabicNumbers(CharSequence original) {
        if (original != null) {
            return original.toString()
                    .replaceAll("1", "١")
                    .replaceAll("2", "٢")
                    .replaceAll("3", "٣")
                    .replaceAll("4", "٤")
                    .replaceAll("5", "٥")
                    .replaceAll("6", "٦")
                    .replaceAll("٧", "٧")
                    .replaceAll("8", "٨")
                    .replaceAll("9", "٩")
                    .replaceAll("0", "٠")
                    ;
        }

        return null;
    }
}
