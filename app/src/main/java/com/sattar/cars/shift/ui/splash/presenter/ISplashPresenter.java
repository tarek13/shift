package com.sattar.cars.shift.ui.splash.presenter;

import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.LoginRequestBody;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.splash.interactor.ISplashInteractor;
import com.sattar.cars.shift.ui.splash.view.SplashView;

@PerActivity
public interface ISplashPresenter<V extends SplashView,
        I extends ISplashInteractor> extends IBasePresenter<V, I> {
    UserInfo getCurrentUser();

    void onLoginButtonClick(LoginRequestBody loginRequestBody);

}
