package com.sattar.cars.shift.di.module;

import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.interactor.EditUserInfoInteractor;
import com.sattar.cars.shift.ui.main.interactor.IEditUserInfoInteractor;
import com.sattar.cars.shift.ui.main.presenter.IEditUserInfoPresenter;
import com.sattar.cars.shift.ui.main.presenter.EditUserInfoPresenter;
import com.sattar.cars.shift.ui.main.view.EditUserInfoView;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class EditUserInfoModule {

    @Provides
    @PerActivity
    IEditUserInfoPresenter<EditUserInfoView,IEditUserInfoInteractor> provideEditProfilePresenter(EditUserInfoPresenter<EditUserInfoView,IEditUserInfoInteractor> editProfilePresenter){
        return  editProfilePresenter;
    }

    @Provides
    @PerActivity
    IEditUserInfoInteractor provideEditProfileInteractor (EditUserInfoInteractor editProfileInteractor){
        return editProfileInteractor;
    }


}
