package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.RegisterRequestBody;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IEditUserInfoInteractor;
import com.sattar.cars.shift.ui.main.view.EditUserInfoView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;
import retrofit2.Retrofit;

public class EditUserInfoPresenter<V extends EditUserInfoView, I extends IEditUserInfoInteractor>
        extends BasePresenter<V, I> implements IEditUserInfoPresenter<V, I> {

    @Inject
    public EditUserInfoPresenter(I mvpInteractor,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable, Retrofit retrofit) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onSaveButtonClick(final RegisterRequestBody registerRequestBody) {
        getMvpView().showLoading();
        HashMap<String, String> options = new HashMap<>();
        options.put("client_id", getInteractor().getPreferencesHelper().getCurrentUser().getId());
        options.put("lang", registerRequestBody.getLang());
        options.put("nameAR", registerRequestBody.getFirstName());
        options.put("email", registerRequestBody.getMail());
        options.put("mobile", registerRequestBody.getMobile());
        options.put("age", registerRequestBody.getAge());
        options.put("gender", registerRequestBody.getGender());


        getCompositeDisposable().add(getInteractor().doUserEditProfile(options)
                .subscribeOn(getSchedulerProvider().io())

                .observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<RegisterResponse>() {
                    @Override
                    public void accept(RegisterResponse registerResponse) throws Exception {
                        getMvpView().hideLoading();
                        if (registerResponse.getStatus()) {
                            UserInfo userInfo = new UserInfo();

                            userInfo.setId(registerResponse.getData().getId());
                            userInfo.setName(registerResponse.getData().getName());
                            userInfo.setEmail(registerResponse.getData().getEmail());
                            userInfo.setPassword(getInteractor().getPreferencesHelper().getCurrentUser().getPassword());
                            userInfo.setMobile(registerResponse.getData().getMobile());
                            userInfo.setGender(registerResponse.getData().getGender());
                            userInfo.setTypeOfLogin(getInteractor().getPreferencesHelper().getCurrentUser().getTypeOfLogin());
                            String email = getInteractor().getPreferencesHelper().getCurrentUser().getEmail();
                            getInteractor().saveUserToSharedPreferences(userInfo);

                            if (email.equals(userInfo.getEmail())) {
                                getMvpView().showMessage(registerResponse.getMessage(), 1);
                                getMvpView().changeHeaderData(userInfo);
                            } else {
                                getInteractor().getPreferencesHelper().removeCurrentUser();
                                getMvpView().exitTheApplication();
                            }

                        } else {
                            getMvpView().onError(registerResponse.getMessage());
                        }
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().hideLoading();
                        throwable.printStackTrace();
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().onError(R.string.some_error);

                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        } else {
                            getMvpView().onError(R.string.some_error);
                        }

                    }
                }));

    }


    @Override
    public boolean validateUserData(String firstName, String email, String mobile, String age) {
        if (firstName == null || firstName.trim().isEmpty() || firstName.trim().length() <= 0) {
            getMvpView().onFirstNameError(R.string.first_name_error);
            return false;
        } else if (email == null || email.trim().isEmpty() || email.trim().length() <= 0) {
            getMvpView().onEmailError(R.string.email_required_error);
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            getMvpView().onEmailError(R.string.invalid_email_error);
            return false;
        } else if (mobile == null || mobile.trim().isEmpty() || mobile.trim().length() <= 0) {
            getMvpView().onMobileError(R.string.invalid_phone_number);
            return false;
        } else if (age == null || age.trim().isEmpty() || age.trim().length() <= 0) {
            getMvpView().onAgeError(R.string.age_error);
            return false;
        }

        return true;
    }

    @Override
    public boolean checkIfEmailChange(String email) {
        return email.equals(getInteractor().getPreferencesHelper().getCurrentUser().getEmail());
    }
}
