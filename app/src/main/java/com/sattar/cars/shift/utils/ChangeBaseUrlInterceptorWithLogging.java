package com.sattar.cars.shift.utils;

import java.io.IOException;

import javax.inject.Singleton;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@Singleton
public class ChangeBaseUrlInterceptorWithLogging implements Interceptor {
    private static ChangeBaseUrlInterceptorWithLogging sInterceptor;
    private String mScheme;
    private String mHost;
    private String mEncoddedPath;

    public static ChangeBaseUrlInterceptorWithLogging get() {
        if (sInterceptor == null) {
            sInterceptor = new ChangeBaseUrlInterceptorWithLogging();
        }
        return sInterceptor;
    }

    private ChangeBaseUrlInterceptorWithLogging() {
        // Intentionally blank
    }

    public void setInterceptor(String url) {
        HttpUrl httpUrl = HttpUrl.parse(url);
        mScheme = httpUrl.scheme();
        mHost = httpUrl.host();
        mEncoddedPath=httpUrl.encodedPath();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String originalEncodedPath= original.url().encodedPath();
       int indexOfApi= originalEncodedPath.indexOf("Api");
       String subPath=originalEncodedPath.substring(indexOfApi+4);

        // If new Base URL is properly formatted than replace with old one
        if (mScheme != null && mHost != null) {
            HttpUrl newUrl = original.url().newBuilder()
                    .scheme(mScheme)
                    .host(mHost)
                    .encodedPath(mEncoddedPath+subPath)
                    .build();
            original = original.newBuilder()
                    .url(newUrl)
                    .build();
        }
        return chain.proceed(original);
    }
}
