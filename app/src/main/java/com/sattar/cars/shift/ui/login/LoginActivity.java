package com.sattar.cars.shift.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.LoginRequestBody;
import com.sattar.cars.shift.di.component.DaggerLoginComponent;
import com.sattar.cars.shift.di.component.LoginComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.LoginModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.BaseActivity;
import com.sattar.cars.shift.ui.forgetpassword.ForgetPasswordActivity;
import com.sattar.cars.shift.ui.login.interactor.ILoginInteractor;
import com.sattar.cars.shift.ui.login.presenter.ILoginPresenter;
import com.sattar.cars.shift.ui.login.view.LoginView;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.register.RegistrationActivity;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.rx.RxBus;
import com.sattar.cars.shift.utils.views.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {

    private static final String TAG = "LoginActivity";
    @BindView(R.id.login_email_edit_text)
    EditText loginEmailEditText;
    @BindView(R.id.login_error_email_text_view)
    CustomTextView loginErrorEmailTextView;
    @BindView(R.id.login_password_edit_text)
    EditText loginPasswordEditText;
    @BindView(R.id.login_error_password_text_view)
    CustomTextView loginErrorPasswordTextView;
    @BindView(R.id.login_signup_button)
    CustomTextView loginSignupButton;
    @BindView(R.id.login_forget_password_button)
    CustomTextView loginForgetPasswordButton;
    @BindView(R.id.login_login_button_container)
    LinearLayout loginLoginButtonContainer;
    @BindView(R.id.login_signup_button_container)
    LinearLayout loginSignupButtonContainer;

    @Inject
    ILoginPresenter<LoginView, ILoginInteractor> mPresenter;


    @Inject
    RxBus rxBus;

    LoginComponent loginComponent;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    private String email;
    private String password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setUnBinder(ButterKnife.bind(this));


        loginComponent = DaggerLoginComponent.builder()
                .loginModule(new LoginModule())
                .activityModule(new ActivityModule(this))
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .applicationComponent(((ShiftApp) getApplication()).getComponent()).build();

        loginComponent.inject(this);

        mPresenter.onAttach(this);

        setUp();

    }

    @OnClick({R.id.login_login_button_container, R.id.login_forget_password_button, R.id.login_signup_button_container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_login_button_container:
                fillUserDataFromUI();
                loginErrorEmailTextView.setVisibility(View.GONE);
                loginErrorPasswordTextView.setVisibility(View.GONE);
                if (mPresenter.validateUserData(email, password)) {
                    changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.WEBSITE_API_URL_1);
                    mPresenter.onLoginButtonClick(buildLoginRequestBody());
                }
                break;
            case R.id.login_forget_password_button:
                mPresenter.onForgetPasswordButtonClick();
                break;
            case R.id.login_signup_button_container:
                mPresenter.onRegisterButtonClick();
                break;
        }
    }

    //function to setup view before start


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

            super.onBackPressed();

    }

    @Override
    protected void setUp() {

        loginErrorEmailTextView.setVisibility(View.GONE);
        loginErrorPasswordTextView.setVisibility(View.GONE);


        mPresenter.loadUserEmailPasswordFromPrefs();
        hideKeyboard();


    }
    //start registration activity
    @Override
    public void openRegisterActivity() {
        hideKeyboard();
        Intent intentRegistrationActivity = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(intentRegistrationActivity);

    }

    //start Forget Password activity
    @Override
    public void openForgetPasswordActivity() {
        hideKeyboard();
        Intent intentForgetPassword = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
        startActivity(intentForgetPassword);
    }

    @Override
    public void openMainActivity() {

            hideKeyboard();
            Intent intentMainActivity = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intentMainActivity);
            finish();

    }

    @Override
    public void fillEmailAndPasswordFromPrefs(String email, String password) {
        loginEmailEditText.setText(email);
        loginPasswordEditText.setText(password);
    }

    @Override
    public void onErrorEmail(int emailError) {
        loginErrorEmailTextView.setVisibility(View.VISIBLE);
        loginErrorEmailTextView.setText(emailError);
    }

    @Override
    public void onErrorPassword(int passwordError) {
        loginErrorPasswordTextView.setVisibility(View.VISIBLE);
        loginErrorPasswordTextView.setText(passwordError);
    }

    private void fillUserDataFromUI() {
        email = loginEmailEditText.getText().toString();
        password = loginPasswordEditText.getText().toString();
    }

    private LoginRequestBody buildLoginRequestBody() {
        LoginRequestBody loginRequestBody = new LoginRequestBody();

        loginRequestBody.setLang(getString(R.string.language));
        loginRequestBody.setEmail(email);
        loginRequestBody.setPassword(password);


        return loginRequestBody;
    }

}
