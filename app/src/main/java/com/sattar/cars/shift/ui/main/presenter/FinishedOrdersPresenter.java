package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.MyOrdersResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IFinishedOrdersInteractor;
import com.sattar.cars.shift.ui.main.view.FinishedOrdersView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class FinishedOrdersPresenter<V extends FinishedOrdersView,I extends IFinishedOrdersInteractor>
        extends BasePresenter<V,I> implements IFinishedOrdersPresenter<V,I> {
    private static final String TAG = "Companieslist";

    @Inject
    public FinishedOrdersPresenter(I mvpInteractor,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void loadFinishedOrders(String lang, String departmentId) {
        getMvpView().showLoadingProgressbar();
        HashMap<String,String> options=new HashMap<>();
        options.put("latitude", "24.626683");
        options.put("longitude", "46.709545");
        options.put("department_id", departmentId);
        options.put("lang", lang);

        getCompositeDisposable().add(getInteractor().getFinishedOrders(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<MyOrdersResponse>() {
                    @Override
                    public void accept(MyOrdersResponse eventsResponse) {
                        getMvpView().hideLoadingProgressbar();

                        if(eventsResponse.getStatus()) {
                            getMvpView().showFinishedOrdersList(eventsResponse.getData().getFinshied());
                        }else {
                            getMvpView().showErrorMessage(R.string.no_orders_found, R.drawable.not_found);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoadingProgressbar();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                            }
                        }else {
                            getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);

                        }
                    }
                }));
    }

}
