package com.sattar.cars.shift.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.ui.main.holder.HomeViewHolder;

import java.util.ArrayList;

import javax.inject.Inject;

public class HomeAdapter extends RecyclerView.Adapter<HomeViewHolder> {



    // View Types
    private ArrayList<DepartmentResponse.Data> homeData = new ArrayList<>();

    private Callback callback;

    private Context context;


    @Inject
    public HomeAdapter(Context context) {
        this.context = context;
    }

    public interface Callback {
        void onHomeClick(int position, DepartmentResponse.Data homeData);
    }

    public void setData(ArrayList<DepartmentResponse.Data> homeData) {
        this.homeData = homeData;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HomeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_home_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        holder.setData(homeData.get(position), position);
    }


    @Override
    public int getItemCount() {
        return homeData.size();
    }
}
