package com.sattar.cars.shift.ui.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.rd.PageIndicatorView;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.data.network.model.BannerItemResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.di.component.DaggerHomeComponent;
import com.sattar.cars.shift.di.component.HomeComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.HomeModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.MainActivity;
import com.sattar.cars.shift.ui.main.adapter.BannerViewPagerAdapter;
import com.sattar.cars.shift.ui.main.adapter.HomeAdapter;
import com.sattar.cars.shift.ui.main.interactor.IHomeInteractor;
import com.sattar.cars.shift.ui.main.presenter.IHomePresenter;
import com.sattar.cars.shift.ui.main.view.HomeView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.ScreenUtils;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements HomeView, HomeAdapter.Callback {
    public static final String TAG = "HomeFragment";

    @BindView(R.id.home_recycler_view)
    RecyclerView homeRecyclerView;

    @BindView(R.id.events_list_error)
    CustomTextView eventsListError;

    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;


    @Inject
    IHomePresenter<HomeView, IHomeInteractor> homePresenter;

    @Inject
    HomeAdapter homeAdapter;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;
    
    @Inject
    BannerViewPagerAdapter bannerViewPagerAdapter;       


    HomeComponent homeComponent;
    @BindView(R.id.home_banner_view_pager)
    ViewPager homeBannerViewPager;
    @BindView(R.id.home_banner_pager_indicator)
    PageIndicatorView homeBannerPagerIndicator;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        homeComponent = DaggerHomeComponent.builder()
                .homeModule(new HomeModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        homeComponent.inject(this);

        homePresenter.onAttach(this);
          setShowMenu(false);

        return view;
    }

    @Override
    protected void setUp(View view) {
        ((MainActivity) getBaseActivity()).getSupportActionBar().setTitle(R.string.menu_home_title);

        homeRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        homeRecyclerView.setAdapter(homeAdapter);
        homeAdapter.setCallback(this);
        homeRecyclerView.setItemAnimator(new DefaultItemAnimator());

        homePresenter.loadDepartmentsData(getString(R.string.language));
        homePresenter.loadBannerData(getString(R.string.language));


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        homePresenter.onDetach();
    }


    @Override
    public void onHomeClick(int position, DepartmentResponse.Data homeData) {
        switch (Integer.valueOf(homeData.getId())) {
            case 1:
                 ((MainActivity)getBaseActivity()).showSearchMapFragment(homeData);
                break;
            case 2:
                ((MainActivity)getBaseActivity()).showSearchMapFragment(homeData);
                break;
            case 3:
               ((MainActivity) getBaseActivity()).showSubscribesListFragment(homeData);
                break;
            case 4:
                ((MainActivity)getBaseActivity()).showSearchMapFragment(homeData);
                break;
        }


    }

    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            homeRecyclerView.setVisibility(View.GONE);

        }

    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            homeRecyclerView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
        homeRecyclerView.setVisibility(View.GONE);
        eventsListError.setVisibility(View.VISIBLE);
        eventsListError.setText(messageID);
        eventsListError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void showHomeData(ArrayList<DepartmentResponse.Data> homeData) {
        homeAdapter.setData(homeData);
    }

    @Override
    public void showBannerData(ArrayList<BannerItemResponse> bannerItemResponses) {
        homeBannerViewPager.setAdapter(bannerViewPagerAdapter);
        homeBannerViewPager.setVisibility(View.GONE);
        homeBannerPagerIndicator.setVisibility(View.GONE);
        if(bannerItemResponses!=null && !bannerItemResponses.isEmpty()) {
            homeBannerViewPager.setVisibility(View.VISIBLE);
            homeBannerPagerIndicator.setVisibility(View.VISIBLE);
            if (ScreenUtils.isRTL()) {
                Collections.reverse(bannerItemResponses);
                if (bannerItemResponses.size() > 1) {
                    bannerViewPagerAdapter.setDataList(bannerItemResponses);

                } else {
                    homeBannerPagerIndicator.setVisibility(View.GONE);
                }
                homeBannerViewPager.setCurrentItem(bannerViewPagerAdapter.getCount() - 1);
                homeBannerPagerIndicator.setCount(bannerViewPagerAdapter.getCount());

                homePresenter.onBannerPageSelected(bannerViewPagerAdapter.getCount() - 1);
            } else {
                if (bannerItemResponses.size() > 1) {
                    bannerViewPagerAdapter.setDataList(bannerItemResponses);

                } else {
                    homeBannerPagerIndicator.setVisibility(View.GONE);
                }
            }

            homeBannerViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    homePresenter.onBannerPageSelected(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            bannerViewPagerAdapter.setCallback(new BannerViewPagerAdapter.Callback() {
                @Override
                public void onItemClick(int position) {
                    //((MainActivity) getBaseActivity()).showVoucherOfferDetailsFragment();
                }
            });
            homeBannerPagerIndicator.setCount(bannerViewPagerAdapter.getCount());
        }else {
            homeBannerViewPager.setVisibility(View.GONE);
            homeBannerPagerIndicator.setVisibility(View.GONE);
        }

    }

    @Override
    public void showSelectedBanner(int position) {
        homeBannerPagerIndicator.setSelected(position);

    }
}
