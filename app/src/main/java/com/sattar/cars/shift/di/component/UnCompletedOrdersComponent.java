package com.sattar.cars.shift.di.component;



import com.sattar.cars.shift.di.module.UnCompletedOrdersModule;
import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.main.fragment.UnCompletedOrdersFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {UnCompletedOrdersModule.class})
public interface UnCompletedOrdersComponent {

    void inject(UnCompletedOrdersFragment unCompletedOrdersFragment);
}
