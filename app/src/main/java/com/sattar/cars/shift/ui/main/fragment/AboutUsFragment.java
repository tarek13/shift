package com.sattar.cars.shift.ui.main.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sattar.cars.shift.GlideApp;
import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.AboutUsResponse;
import com.sattar.cars.shift.di.component.AboutUsComponent;
import com.sattar.cars.shift.di.component.DaggerAboutUsComponent;
import com.sattar.cars.shift.di.module.AboutUsModule;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.fragment.BaseFragment;
import com.sattar.cars.shift.ui.main.interactor.IAboutUsInteractor;
import com.sattar.cars.shift.ui.main.presenter.IAboutUsPresenter;
import com.sattar.cars.shift.ui.main.view.AboutUsView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutUsFragment extends BaseFragment implements AboutUsView {
    public static final String TAG = "AboutUsFragment";

    @Inject
    IAboutUsPresenter<AboutUsView, IAboutUsInteractor> eventsListPresenter;


    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    AboutUsComponent eventsListComponent;

    int comeFrom;

    private AboutUsResponse.Result result;


    public static Fragment newInstance() {
        Bundle args = new Bundle();
        AboutUsFragment fragment = new AboutUsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public void setComeFrom(int comeFrom) {
        this.comeFrom = comeFrom;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        eventsListComponent = DaggerAboutUsComponent.builder()
                .aboutUsModule(new AboutUsModule())
                .activityModule(new ActivityModule(getBaseActivity()))
                .applicationComponent(((ShiftApp) getBaseActivity().getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .build();

        eventsListComponent.inject(this);

        eventsListPresenter.onAttach(this);
        setShowMenu(false);
        getBaseActivity().getSupportActionBar().setTitle(R.string.menu_about_us_title);

        return view;
    }

    @Override
    protected void setUp(View view) {
        getBaseActivity().getSupportActionBar().setTitle(R.string.menu_about_us_title);
      //  aboutUsUrl.setText(Html.fromHtml(getString(R.string.url)), TextView.BufferType.SPANNABLE);

       /* HashMap<String, String> options = new HashMap<>();
        options.put("lang", "EN");
        options.put("user_id", eventsListPresenter.getInteractor().getPreferencesHelper().getCurrentUser().getId());
        //changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.WEBSITE_API_URL_1);
       // eventsListPresenter.loadAboutUs(getString(R.string.language));
*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        eventsListPresenter.onDetach();
    }


    @Override
    public void showAboutUs(AboutUsResponse.Result result) {
        this.result = result;
      //  aboutUsTitleTextView.setText(result.getEnSubTitle());
        //aboutUsDescription.setText(result.getEnContent());
        //GlideApp.with(getBaseActivity()).load("http://gdata.youtube.com/feeds/api/videos/gxRhQWNuMxc").into(aboutUsImageView);


    }


    @Override
    public void showLoadingProgressbar() {
        /*if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            aboutUsContainer.setVisibility(View.GONE);

        }*/

    }

    @Override
    public void hideLoadingProgressbar() {
        /*if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            aboutUsContainer.setVisibility(View.VISIBLE);
        }*/

    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
      /*  aboutUsContainer.setVisibility(View.GONE);
        aboutUsError.setVisibility(View.VISIBLE);
        aboutUsError.setText(messageID);
        aboutUsError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);*/
    }


   /* @OnClick(R.id.about_us_image_view)
    public void onViewClicked() {
        ArrayList<String> fileUrls = new ArrayList<>();
        fileUrls.add(result.getImg());
        *//*new ImageViewer.Builder(getBaseActivity(), fileUrls)
                .setStartPosition(0)
                .show();*//*
    }

    @OnClick(R.id.about_us_url)
    public void onUrlViewClicked() {
        String url = "http://www.orchidiapharma.com";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }*/
}
