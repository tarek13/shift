package com.sattar.cars.shift.ui.main.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.BannerResponse;
import com.sattar.cars.shift.data.network.model.DepartmentResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IHomeInteractor;
import com.sattar.cars.shift.ui.main.view.HomeView;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class HomePresenter<V extends HomeView,I extends IHomeInteractor>
        extends BasePresenter<V,I> implements IHomePresenter<V,I>{
    private static final String TAG = "Eventslist";

    @Inject
    public HomePresenter(I mvpInteractor,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void loadDepartmentsData(String lang) {
        getMvpView().showLoadingProgressbar();

        HashMap<String,String> options= new HashMap<>();
        options.put("lang",lang);

        getCompositeDisposable().add(getInteractor().getDepartmentsData(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<DepartmentResponse>() {
                    @Override
                    public void accept(DepartmentResponse departmentResponse) {
                        getMvpView().hideLoadingProgressbar();

                        if(departmentResponse.getStatus()) {
                            getMvpView().showHomeData(departmentResponse.getData());
                        }else {
                            getMvpView().showErrorMessage(R.string.no_events_list_found, R.drawable.not_found);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoadingProgressbar();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                            }
                        }else {
                            getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);

                        }
                    }
                }));
    }

    @Override
    public void loadBannerData(String lang) {
       // getMvpView().showLoadingProgressbar();

        HashMap<String,String> options= new HashMap<>();
        options.put("lang",lang);

        getCompositeDisposable().add(getInteractor().getBannersData(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<BannerResponse>() {
                    @Override
                    public void accept(BannerResponse bannerResponse) {
                      //  getMvpView().hideLoadingProgressbar();

                        if(bannerResponse.getStatus()) {
                            getMvpView().showBannerData(bannerResponse.getData());
                        }else {
                            getMvpView().showErrorMessage(R.string.no_events_list_found, R.drawable.not_found);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                    //    getMvpView().hideLoadingProgressbar();
                        if(throwable instanceof HttpException) {
                            getMvpView().onError(((HttpException) throwable).message());

                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);
                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                            }
                        }else {
                            getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);

                        }
                    }
                }));
    }

    @Override
    public void onBannerPageSelected(int position) {
        getMvpView().showSelectedBanner(position);

    }
}
