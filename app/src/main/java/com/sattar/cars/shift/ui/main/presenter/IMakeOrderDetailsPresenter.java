package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IMakeOrderDetailsInteractor;
import com.sattar.cars.shift.ui.main.view.MakeOrderDetailsView;

import java.util.HashMap;

@PerActivity
public interface IMakeOrderDetailsPresenter<V extends MakeOrderDetailsView,I extends IMakeOrderDetailsInteractor> extends IBasePresenter<V,I> {


    void getMakeOrder(HashMap<String, String> options);
}

