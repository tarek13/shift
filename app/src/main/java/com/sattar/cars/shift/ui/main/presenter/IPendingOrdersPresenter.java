package com.sattar.cars.shift.ui.main.presenter;


import com.sattar.cars.shift.di.scope.PerActivity;
import com.sattar.cars.shift.ui.base.presenter.IBasePresenter;
import com.sattar.cars.shift.ui.main.interactor.IPendingOrdersInteractor;
import com.sattar.cars.shift.ui.main.view.PendingOrdersView;

@PerActivity
public interface IPendingOrdersPresenter<V extends PendingOrdersView,I extends IPendingOrdersInteractor> extends IBasePresenter<V,I> {
    void loadPendingOrders(String lang, String clintId);
}

