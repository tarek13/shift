/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.sattar.cars.shift.ui.splash.interactor;

import android.content.Context;

import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.UserApis;
import com.sattar.cars.shift.data.network.model.LoginErrorResponse;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.data.prefs.PreferencesHelper;
import com.sattar.cars.shift.di.qualifier.ApplicationContext;
import com.sattar.cars.shift.ui.base.interactor.BaseInteractor;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by janisharali on 20/07/17.
 */

public class SplashInteractor extends BaseInteractor
        implements ISplashInteractor {

    private Context mContext;
    private UserApis userApis;
    private Retrofit retrofit;

    @Inject
    public SplashInteractor(@ApplicationContext Context context, PreferencesHelper preferencesHelper, UserApis userApis , Retrofit retrofit) {

        super(preferencesHelper);
        mContext = context;
        this.userApis=userApis;
        this.retrofit=retrofit;
    }

    @Override
    public Observable<RegisterResponse> doUserLogin(Map<String,String> loginRequestBody) {
        return userApis.userLogin(loginRequestBody);
    }

    @Override
    public void saveUserToSharedPreferences(UserInfo userInfo) {
        getPreferencesHelper().saveCurrentUserInfo(userInfo);
    }

    @Override
    public LoginErrorResponse handleErrorResponse(Response<?> response) {
        Converter<ResponseBody, LoginErrorResponse> converter =
                retrofit.responseBodyConverter(LoginErrorResponse.class, new Annotation[0]);

        LoginErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new LoginErrorResponse();
        }

        return error;
    }

    @Override
    public Observable<RegisterResponse> doSocialLogin(HashMap<String, String> options) {
        return userApis.socialLogin(options);

    }
}
