package com.sattar.cars.shift.ui.login.presenter;

import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.db.model.UserInfo;
import com.sattar.cars.shift.data.network.model.LoginRequestBody;
import com.sattar.cars.shift.data.network.model.RegisterResponse;
import com.sattar.cars.shift.ui.base.presenter.BasePresenter;
import com.sattar.cars.shift.ui.login.interactor.ILoginInteractor;
import com.sattar.cars.shift.ui.login.view.LoginView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.rx.SchedulerProvider;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class LoginPresenter <V extends LoginView, I extends ILoginInteractor>
        extends BasePresenter<V, I> implements ILoginPresenter<V, I>{


    @Inject
    public LoginPresenter(I mvpInteractor,
                                        SchedulerProvider schedulerProvider,
                                        CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Override
    public void onForgetPasswordButtonClick() {
        getMvpView().openForgetPasswordActivity();
    }

    @Override
    public void onRegisterButtonClick() {
        getMvpView().openRegisterActivity();
    }

    @Override
    public void onLoginButtonClick(final LoginRequestBody loginRequestBody) {
        getMvpView().showLoading();
        HashMap<String,String> options=new HashMap<>();
        options.put("lang",loginRequestBody.getLang());
        options.put("email",loginRequestBody.getEmail());
        options.put("password",loginRequestBody.getPassword());
        getCompositeDisposable().add(getInteractor().doUserLogin(options)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui())

                .subscribe(new Consumer<RegisterResponse>() {
                    @Override
                    public void accept(RegisterResponse registerResponse) throws Exception {
                        UserInfo userInfo=new UserInfo();
                        getMvpView().hideLoading();
                        if(registerResponse.getStatus()) {
                            userInfo.setId(registerResponse.getData().getId());
                            userInfo.setName(registerResponse.getData().getName());
                            userInfo.setEmail(registerResponse.getData().getEmail());
                            userInfo.setPassword(loginRequestBody.getPassword());
                            userInfo.setMobile(registerResponse.getData().getMobile());
                            userInfo.setGender(registerResponse.getData().getGender());
                            userInfo.setAge(registerResponse.getData().getAge());
                            userInfo.setAddress(registerResponse.getData().getAddress());
                            userInfo.setPlatform(registerResponse.getData().getPlatform());
                            userInfo.setPlatformToken(registerResponse.getData().getPlatformToken());
                            userInfo.setActiveCode(registerResponse.getData().getActiveCode());
                            userInfo.setBlocked(registerResponse.getData().getBlocked());
                            userInfo.setBlockReason(registerResponse.getData().getBlockReason());
                            userInfo.setTypeOfLogin(AppConstants.CUSTOM_LOGIN);
                            getMvpView().hideLoading();
                            getInteractor().saveUserToSharedPreferences(userInfo);
                            getMvpView().openMainActivity();
                        }else {
                            getMvpView().onError(registerResponse.getMessage());

                        }

                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        getMvpView().hideLoading();
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).response().code() == 400) {
                                // toDO handle error
                                getMvpView().onError(R.string.some_error);

                            }
                            if (((HttpException) throwable).response().code() == 500) {
                                getMvpView().onError(R.string.connection_error);
                            }
                        }else {
                            getMvpView().onError(R.string.some_error);
                        }

                    }
                }));

    }

    @Override
    public boolean validateUserData(String email, String password){
        if(email==null || email.trim().isEmpty() || email.trim().length()<=0){
            getMvpView().onErrorEmail(R.string.email_required_error);
            return false;
        }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            getMvpView().onErrorEmail(R.string.invalid_email_error);
            return false;
        }else if(password==null || password.trim().isEmpty() || password.trim().length()<=0) {
            getMvpView().onErrorPassword(R.string.password_required_error);
            return false;
        }
        return true;
    }

    @Override
    public void loadUserEmailPasswordFromPrefs() {
     UserInfo userInfo= getInteractor().getPreferencesHelper().getCurrentUser();
     if(userInfo!=null) {
         getMvpView().fillEmailAndPasswordFromPrefs(userInfo.getEmail(), userInfo.getPassword());
     }
    }
}
