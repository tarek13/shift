package com.sattar.cars.shift.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutUsResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
    public class Result {

        @SerializedName("en_title")
        @Expose
        private String enTitle;
        @SerializedName("en_sub_title")
        @Expose
        private String enSubTitle;
        @SerializedName("en_brief")
        @Expose
        private String enBrief;
        @SerializedName("en_content")
        @Expose
        private String enContent;
        @SerializedName("img")
        @Expose
        private String img;
        @SerializedName("banner")
        @Expose
        private String banner;

        public String getEnTitle() {
            return enTitle;
        }

        public void setEnTitle(String enTitle) {
            this.enTitle = enTitle;
        }

        public String getEnSubTitle() {
            return enSubTitle;
        }

        public void setEnSubTitle(String enSubTitle) {
            this.enSubTitle = enSubTitle;
        }

        public String getEnBrief() {
            return enBrief;
        }

        public void setEnBrief(String enBrief) {
            this.enBrief = enBrief;
        }

        public String getEnContent() {
            return enContent;
        }

        public void setEnContent(String enContent) {
            this.enContent = enContent;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getBanner() {
            return banner;
        }

        public void setBanner(String banner) {
            this.banner = banner;
        }

    }
}
