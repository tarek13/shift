package com.sattar.cars.shift.ui.forgetpassword;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sattar.cars.shift.ShiftApp;
import com.sattar.cars.shift.R;
import com.sattar.cars.shift.data.network.model.ForgetPasswordRequestBody;
import com.sattar.cars.shift.di.component.DaggerForgetPasswordComponent;
import com.sattar.cars.shift.di.component.ForgetPasswordComponent;
import com.sattar.cars.shift.di.module.ActivityModule;
import com.sattar.cars.shift.di.module.ForgetPasswordModule;
import com.sattar.cars.shift.di.module.NetworkModule;
import com.sattar.cars.shift.ui.base.BaseActivity;
import com.sattar.cars.shift.ui.forgetpassword.interactor.IForgetPasswordInteractor;
import com.sattar.cars.shift.ui.forgetpassword.presenter.IForgetPasswordPresenter;
import com.sattar.cars.shift.ui.forgetpassword.view.ForgetPasswordView;
import com.sattar.cars.shift.utils.AppConstants;
import com.sattar.cars.shift.utils.ChangeBaseUrlInterceptorWithLogging;
import com.sattar.cars.shift.utils.views.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordActivity extends BaseActivity implements ForgetPasswordView {

    @BindView(R.id.forget_password_email_edit_text)
    EditText forgetPasswordEmailEditText;
    @BindView(R.id.forget_password_error_email_text_view)
    CustomTextView forgetPasswordErrorEmailTextView;

    @Inject
    IForgetPasswordPresenter<ForgetPasswordView, IForgetPasswordInteractor> forgetPasswordPresenter;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    ForgetPasswordComponent forgetPasswordComponent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        setUnBinder(ButterKnife.bind(this));

        forgetPasswordComponent = DaggerForgetPasswordComponent.builder()
                .forgetPasswordModule(new ForgetPasswordModule())
                .activityModule(new ActivityModule(this))
                .networkModule(new NetworkModule(AppConstants.WEBSITE_API_URL_1))
                .applicationComponent(((ShiftApp) getApplication()).getComponent())
                .build();
        forgetPasswordComponent.inject(this);

        forgetPasswordPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void setUp() {
        forgetPasswordErrorEmailTextView.setVisibility(View.GONE);
    }

    @OnClick(R.id.forget_password_send_now_button_container)
    public void onViewClicked() {
        forgetPasswordErrorEmailTextView.setVisibility(View.GONE);
        if (forgetPasswordPresenter.validateUserData(forgetPasswordEmailEditText.getText().toString())) {
            ForgetPasswordRequestBody forgetPasswordRequestBody = new ForgetPasswordRequestBody();
            forgetPasswordRequestBody.setEmail(forgetPasswordEmailEditText.getText().toString());
            forgetPasswordRequestBody.setLang(getString(R.string.language));
            changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.WEBSITE_API_URL_1);
            forgetPasswordPresenter.onSendNowButtonClick(forgetPasswordRequestBody);
        }
    }

    @Override
    protected void onDestroy() {
        forgetPasswordPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void returnToLoginPage(String message) {
        showMessage(message, Toast.LENGTH_LONG);
        onBackPressed();
    }

    @Override
    public void onEmailError(int emailError) {
        forgetPasswordErrorEmailTextView.setVisibility(View.VISIBLE);
        forgetPasswordErrorEmailTextView.setText(emailError);
    }
}
