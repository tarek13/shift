package com.sattar.cars.shift.ui.main.interactor;



import com.sattar.cars.shift.data.network.model.MyOrdersResponse;
import com.sattar.cars.shift.ui.base.interactor.IBaseInteractor;

import java.util.HashMap;

import io.reactivex.Observable;

public interface IFinishedOrdersInteractor extends IBaseInteractor {
    Observable<MyOrdersResponse> getFinishedOrders(HashMap<String, String> lang);

}
